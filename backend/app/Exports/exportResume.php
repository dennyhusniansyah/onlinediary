<?php

namespace App\Exports;

use App\Activity;
use App\QnA;
use App\Question;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromView;

class exportResume implements FromView{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($resp = '', $from = '', $to = ''){
        $this->resp = $resp;
        $this->from = $from;
        $this->to   = $to;
    }

    public function view(): View {
        if($this->resp == 0){
            $activity   = Activity::leftJoin('users', 'users.id', 'activities.responder')
                        ->leftJoin('timings', 'timings.id', 'activities.timings_id')->where('users.status', 'active')
                        ->whereBetween(DB::raw('DATE(activities.created_at)'), [$this->from, $this->to])
                        ->select('users.*', 'activities.name as activity', 'activities.id as activities_id', 'activities.status', 'timings.name as timing')->get();
        }else{
            $activity   = Activity::leftJoin('users', 'users.id', 'activities.responder')
                        ->leftJoin('timings', 'timings.id', 'activities.timings_id')
                        ->where(['users.status' => 'active', 'users.id' => $this->resp])
                        ->whereBetween(DB::raw('DATE(activities.created_at)'), [$this->from, $this->to])
                        ->select('users.*', 'activities.name as activity', 'activities.id as activities_id', 'activities.status', 'timings.name as timing')->get();
        }
        $resume     = $activity->map(function($a){
            return [
                'name'          => $a->name,
                'email'         => $a->email,
                'phone'         => $a->phone,
                'birthday'      => $a->birthday,
                'gender'        => $a->gender,
                'marital'       => $a->marital,
                'status'        => $a->status,
                'activity'      => $a->activity,
                'timing'        => $a->timing,
                'activities_id' => $a->activities_id,
                'qna'           => QnA::where('activities_id', $a->activities_id)->select('questions_id', 'answer', 'photo')->get()
            ];
        });
        $quest  = Question::orderBy('sort')->select('id', 'question')->get();
        return view('exports.resume', [
            'resume'    => $resume,
            'quest'     => $quest
        ]);
    }
}
