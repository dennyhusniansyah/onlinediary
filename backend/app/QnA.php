<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QnA extends Model{
    
    protected $fillable = ['activities_id', 'questions_id', 'question', 'answers_id', 'answer', 'photo', 'users_id'];
}
