<?php

namespace App\Console;

use App\Schedule as AppSchedule;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        $schedule->call(function(){
            $users  = checkTime();
            if($users != null){
                if(count($users) > 0){
                    $sch            = new AppSchedule();
                    $sch->respondent= count($users);
                    $sch->timing    = $users[0]->timing;
                    $sch->save();
                    foreach($users as $u){
                        $notif  = [
                            'title' => 'Activity',
                            'body'  => 'Hai '.$u->name.', jangan lupa untuk mengisi aktivitas '.strtolower($u->timing).' pada project '.strtolower($u->project).' kamu di bagian ini atau tap langsung untuk isi diary'
                        ];
                        $data   = [
                            'project_id'    => $u->projects_id
                        ];
                        sendNotif($u->id, $notif, $data);
                    }
                }
            }
        });
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
