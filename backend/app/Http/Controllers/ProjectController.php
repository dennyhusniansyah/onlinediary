<?php

namespace App\Http\Controllers;

use App\Product;
use App\Project;
use App\ProjectTiming;
use App\Task;
use App\Timing;
use Auth;
use Illuminate\Http\Request;

class ProjectController extends Controller{
    
    public function __invoke(){
        $data['pageName']   = 'project';
        $data['projects']   = Project::leftJoin('project_timings', 'project_timings.projects_id', 'projects.id')
                            ->leftJoin('timings', 'timings.id', 'project_timings.timings_id')
                            ->leftJoin('products', 'products.id', 'projects.products_id')
                            ->selectRaw('projects.*, GROUP_CONCAT(timings.name SEPARATOR"#") as timing, products.name as product')
                            ->groupBy('projects.id')->get();
        return view('project.index', $data)->with('no', 1);
    }

    public function addData(){
        $data['timing']     = Timing::all();
        $data['products']   = Product::all();
        return view('project.addData', $data);
    }

    public function saveData(Request $r){
        $cek    = Project::where('code', $r->code)->count();
        if($cek > 0){
            return redirect()->back()->with(['error' => 'Kode project sudah ada !']);
        }else{
            $p              = new Project();
            $p->code        = $r->code;
            $p->name        = $r->name;
            $p->task        = $r->task;
            $p->days        = $r->days;
            $p->quota       = $r->quota;
            $p->capaian     = $r->capaian;
            $p->instruction = $r->instruction;
            $p->description = $r->description;
            $p->products_id = $r->products_id;
            $p->users_id    = Auth::user()->id;
            $p->save();

            foreach($r->timing as $k => $v){
                $timing         = Timing::find($k);
                $pt             = new ProjectTiming();
                $pt->projects_id= $p->id;
                $pt->timings_id = $k;
                $pt->start_time = $timing->start;
                $pt->end_time   = $timing->end;
                $pt->users_id   = Auth::user()->id;
                $pt->save();
            }
        }
        return redirect()->back()->with(['success' => 'Data berhasil disimpan !']);
    }

    public function editData($id = ''){
        $data['project']    = Project::leftJoin('project_timings', 'project_timings.projects_id', 'projects.id')
                        ->leftJoin('timings', 'timings.id', 'project_timings.timings_id')
                        ->where('projects.id', $id)
                        ->selectRaw('projects.*, GROUP_CONCAT(timings.id SEPARATOR"#") as timing')
                        ->groupBy('projects.id')->first();
        $data['timings']= Timing::all();
        $data['products']= Product::all();
        return view('project.editData', $data);
    }

    public function updateData(Request $r){
        $p              = Project::find($r->id);
        $p->name        = $r->name;
        $p->task        = $r->task;
        $p->days        = $r->days;
        $p->quota       = $r->quota;
        $p->capaian     = $r->capaian;
        $p->instruction = $r->instruction;
        $p->description = $r->description;
        $p->products_id = $r->products_id;
        $p->users_id    = Auth::user()->id;
        $p->update();

        ProjectTiming::where('projects_id', $r->id)->delete();

        foreach($r->timing as $k => $v){
            $timing         = Timing::find($k);
            $pt             = new ProjectTiming();
            $pt->projects_id= $p->id;
            $pt->timings_id = $k;
            $pt->start_time = $timing->start;
            $pt->end_time   = $timing->end;
            $pt->users_id   = Auth::user()->id;
            $pt->save();
        }
        return redirect()->back()->with(['success' => 'Data berhasil diupdate !']);
    }

    public function hapusData($id = ''){
        $cek    = Task::where('projects_id', $id)->count();
        if($cek > 0){
            return redirect()->back()->with(['error' => 'Data sudah diimplementasikan!']);
        }else{
            Project::find($id)->delete();
        }
        return redirect()->back()->with(['success' => 'Data berhasil dihapus !']);
    }
}
