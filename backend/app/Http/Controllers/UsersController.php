<?php

namespace App\Http\Controllers;

use App\User;
use Hash;
use Auth;
use Illuminate\Http\Request;

class UsersController extends Controller{
    public function invite($id = ''){
        $data   = msgInvite($id);
        return redirect('https://wa.me/'.$data['phone'].'/?text='.$data['pesan']);
    }

    //================================================= ADMINISTRATOR ======================
    public function administrator(){
        $data['pageName']   = 'users';
        $data['users']      = User::where('role', 'admin')->get();
        return view('users.admin', $data)->with('no', 1);
    }

    public function saveAdmin(Request $r){
        $cek    = User::where('email', $r->email)->first();
        if($cek){
            return redirect()->back()->with(['error' => 'Email sudah ada !']);
        }else{
            User::insert([
                'name'      => $r->name,
                'email'     => $r->email,
                'phone'     => $r->phone,
                'birthday'  => $r->birthday,
                'gender'    => $r->gender,
                'role'      => 'admin',
                'password'  => Hash::make($r->password),
                'users_id'  => Auth::user()->id
            ]);
        }
        return redirect()->back()->with(['success' => 'Data berhasil disimpan !']);
    }

    public function editAdmin($id = ''){
        $data['u']  = User::find($id);
        return view('users.editAdmin', $data);
    }

    public function updateAdmin(Request $r){
        $u              = User::find($r->id);
        $u->name        = $r->name;
        $u->phone       = $r->phone;
        $u->gender      = $r->gender;
        $u->birthday    = $r->birthday;
        if($r->password){
            $u->password= Hash::make($r->password);
        }
        $u->users_id    = Auth::user()->id;
        $u->update();
        return redirect()->back()->with(['success' => 'Data berhasil di update!']);
    }

    //================================================= PROJECT MANAGER ======================
    public function projectManager(){
        $data['pageName']   = 'users';
        $data['users']      = User::where('role', 'pm')->get();
        return view('users.projectManager', $data)->with('no', 1);
    }

    public function saveProjectManager(Request $r){
        $cek    = User::where('email', $r->email)->first();
        if($cek){
            return redirect()->back()->with(['error' => 'Email sudah ada !']);
        }else{
            User::insert([
                'name'      => $r->name,
                'email'     => $r->email,
                'phone'     => $r->phone,
                'birthday'  => $r->birthday,
                'gender'    => $r->gender,
                'role'      => 'pm',
                'password'  => Hash::make($r->password),
                'users_id'  => Auth::user()->id
            ]);
        }
        return redirect()->back()->with(['success' => 'Data berhasil disimpan !']);
    }

    public function editProjectManager($id = ''){
        $data['u']  = User::find($id);
        return view('users.editProjectManager', $data);
    }

    public function updateProjectManager(Request $r){
        $u              = User::find($r->id);
        $u->name        = $r->name;
        $u->phone       = $r->phone;
        $u->gender      = $r->gender;
        $u->birthday    = $r->birthday;
        if($r->password){
            $u->password= Hash::make($r->password);
        }
        $u->users_id    = Auth::user()->id;
        $u->update();
        return redirect()->back()->with(['success' => 'Data berhasil di update!']);
    }

    //================================================= RECRUITER ======================
    public function recruiter(){
        $data['pageName']   = 'users';
        $data['users']      = User::where('role', 'recruiter')->get();
        return view('users.recruiter', $data)->with('no', 1);
    }

    public function saveRecruiter(Request $r){
        $cek    = User::where('email', $r->email)->first();
        if($cek){
            return redirect()->back()->with(['error' => 'Email sudah ada !']);
        }else{
            User::insert([
                'name'      => $r->name,
                'email'     => $r->email,
                'phone'     => $r->phone,
                'birthday'  => $r->birthday,
                'gender'    => $r->gender,
                'role'      => 'recruiter',
                'status'    => 'active',
                'password'  => Hash::make($r->password),
                'users_id'  => Auth::user()->id
            ]);
        }
        return redirect()->back()->with(['success' => 'Data berhasil disimpan !']);
    }

    public function editRecruiter($id = ''){
        $data['u']  = User::find($id);
        return view('users.editRecruiter', $data);
    }

    public function updateRecruiter(Request $r){
        $u              = User::find($r->id);
        $u->name        = $r->name;
        $u->phone       = $r->phone;
        $u->gender      = $r->gender;
        $u->birthday    = $r->birthday;
        if($r->password){
            $u->password= Hash::make($r->password);
        }
        $u->users_id    = Auth::user()->id;
        $u->update();
        return redirect()->back()->with(['success' => 'Data berhasil di update!']);
    }

    //================================================= TESPONDER ======================
    public function responder(){
        $data['pageName']   = 'users';
        $data['usersRec']   = User::where('role', 'recruiter')->get();
        $data['users']      = User::leftJoin('users as u', 'u.id', 'users.users_id')->where('users.role', 'responder')
                            ->select('users.*', 'u.name as uname')->get();
        return view('users.responder', $data)->with('no', 1);
    }

    public function editResponder($id = ''){
        $data['u']  = User::find($id);
        return view('users.editResponder', $data);
    }

    public function saveResponder(Request $r){
        $cek    = User::where('email', $r->email)->first();
        if($cek){
            return redirect()->back()->with(['error' => 'Email sudah ada !']);
        }else{
            User::insert([
                'name'      => $r->name,
                'email'     => $r->email,
                'phone'     => $r->phone,
                'birthday'  => $r->birthday,
                'gender'    => $r->gender,
                'role'      => 'responder',
                'status'    => 'active',
                'password'  => Hash::make($r->password)
            ]);
        }
        return redirect()->back()->with(['success' => 'Data berhasil disimpan !']);
    }

    public function updateResponder(Request $r){
        $u              = User::find($r->id);
        $u->name        = $r->name;
        $u->phone       = $r->phone;
        $u->gender      = $r->gender;
        $u->birthday    = $r->birthday;
        if($r->password){
            $u->password= Hash::make($r->password);
        }
        $u->users_id    = Auth::user()->id;
        $u->update();
        return redirect()->back()->with(['success' => 'Data berhasil di update!']);
    }

    public function inactiveResponder($user = ''){
        $cek    = User::find($user)->count();
        if($cek > 0){
            $t          = User::where(['id' => $user])->first();
            $t->status  = 'inactive';
            $t->update();

            return redirect()->back()->with(['success' => 'User di-non-aktifkan!']);
        }
    }

    public function deleteResponder($user = ''){
        $cek    = User::where(['id' => $user])->count();
        if($cek > 0){
            User::where(['id' => $user])->delete();

            return redirect()->back()->with(['success' => 'Data berhasil dihapus !']);
        }
    }
}
