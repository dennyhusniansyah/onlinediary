<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Project;
use App\ProjectTiming;
use App\QnA;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller{

    public function test($users_id = '', $projects_id = ''){
        $now    = now()->format('H:i:s');
        // $now    = '03:15:20';
        if($now < '05:00:00'){
            $timings = ProjectTiming::leftJoin('timings', 'timings.id', 'project_timings.timings_id')
                ->leftJoin('tasks', 'tasks.projects_id', 'project_timings.projects_id')
                ->where(['project_timings.projects_id' => $projects_id, 'tasks.users_id' => $users_id, 'timings.end' => '05:00:00'])
                ->select('timings.*')->first();
            return $timings->id;
        }else{
            $timings = ProjectTiming::leftJoin('timings', 'timings.id', 'project_timings.timings_id')
                    ->leftJoin('tasks', 'tasks.projects_id', 'project_timings.projects_id')
                    ->where(['project_timings.projects_id' => $projects_id, 'tasks.users_id' => $users_id])
                    ->select('timings.*')->orderBy('timings.start', 'desc')->get();
            foreach($timings as $t){
                if($now >= $t->start){
                    return $t->id;
                }
            }
        }
        return 0;
        $timing_id  = nowTimingsId($users_id, $projects_id);
        return $timing_id;
        $users  = checkTime();
        return $users;
    }

    public function __invoke(){
        $id = Auth::user()->id;
        $user_role = Auth::user()->role;
        $data['pageName']   = 'dashboard';
        User::find(Auth::user()->id)->update(['status' => 'active']);
        $data['responder']  = User::where(['role' => 'responder', 'status' => 'active'])->get();
        $data['resp_id']    = '';
        $min                = Activity::first() ?? '';
        $max                = Activity::latest()->first() ?? '';
        $data['from_date']  = ($min != '') ? $min->created_at->format('Y-m-d') : '';
        $data['to_date']    = ($max != '') ? $max->created_at->format('Y-m-d') : '';
        $data['projects']   = Project::all();
        $data['project_id'] = 0;
        
        if($user_role == 'recruiter')
        {
            $data['activity']   = Activity::leftJoin('users', 'users.id', 'activities.responder')
                ->leftJoin('timings', 'timings.id', 'activities.timings_id')
                ->leftJoin('comments', 'comments.activities_id', 'activities.id')
                ->whereBetween(DB::raw('DATE(activities.created_at)'), [$data['from_date'], $data['to_date']])
                ->where('recruiter',$id)
                ->selectRaw('activities.*, users.name as username, users.photo as user_photo, timings.name as timing, COUNT(comments.id) as comment')
                ->groupBy('activities.id')->orderBy('activities.id', 'desc')->paginate(24);     
        } else {
            $data['activity']   = Activity::leftJoin('users', 'users.id', 'activities.responder')
                            ->leftJoin('timings', 'timings.id', 'activities.timings_id')
                            ->leftJoin('comments', 'comments.activities_id', 'activities.id')
                            ->whereBetween(DB::raw('DATE(activities.created_at)'), [$data['from_date'], $data['to_date']])
                            ->selectRaw('activities.*, users.name as username, users.photo as user_photo, timings.name as timing, COUNT(comments.id) as comment')
                            ->groupBy('activities.id')->orderBy('activities.id', 'desc')->paginate(24);
        }
        return view('dashboard.index', $data);
    }

    public function filter(Request $r){
        $data['pageName']   = 'dashboard';
        $data['responder']  = User::where(['role' => 'responder', 'status' => 'active'])->get();
        $data['resp_id']    = $r->resp_id;
        $data['projects']   = Project::all();
        $data['project_id'] = $r->project_id;
        $data['from_date']  = $r->from_date;
        $data['to_date']    = $r->to_date;
        if($data['project_id'] == 0){
            $data['activity']   = Activity::leftJoin('users', 'users.id', 'activities.responder')
                                ->leftJoin('timings', 'timings.id', 'activities.timings_id')
                                ->leftJoin('comments', 'comments.activities_id', 'activities.id')
                                ->whereBetween(DB::raw('DATE(activities.created_at)'), [$data['from_date'], $data['to_date']])
                                ->where('activities.responder', ($r->resp_id == 0) ? '>' : '=', $r->resp_id)
                                ->selectRaw('activities.*, users.name as username, users.photo as user_photo, timings.name as timing, COUNT(comments.id) as comment')
                                ->groupBy('activities.id')->orderBy('activities.id', 'desc')->paginate(24);
        }else{
            $data['activity']   = Activity::leftJoin('users', 'users.id', 'activities.responder')
                                ->leftJoin('timings', 'timings.id', 'activities.timings_id')
                                ->leftJoin('comments', 'comments.activities_id', 'activities.id')
                                ->whereBetween(DB::raw('DATE(activities.created_at)'), [$data['from_date'], $data['to_date']])
                                ->where('activities.projects_id', $r->project_id)
                                ->where('activities.responder', ($r->resp_id == 0) ? '>' : '=', $r->resp_id)
                                ->selectRaw('activities.*, users.name as username, users.photo as user_photo, timings.name as timing, COUNT(comments.id) as comment')
                                ->groupBy('activities.id')->orderBy('activities.id', 'desc')->paginate(24);
        }
        $data['activity']->appends(request()->except($data['resp_id'], $data['project_id'], $data['from_date'], $data['to_date']))->links();
        return view('dashboard.index', $data);
    }

    public function qna($act_id = ''){
        $data['activity']   = Activity::find($act_id);
        $data['qna']        = QnA::leftJoin('questions', 'questions.id', 'qn_a_s.questions_id')
                        ->select('qn_a_s.*', 'questions.type')
                        ->where('qn_a_s.activities_id', $act_id)->get();
        return view('dashboard.qna', $data);
    }

    public function updateActivity($status = '', $id = ''){
        Activity::find($id)->update(['status' => $status]);
        return redirect()->back()->with(['success' => 'Activity berhasil diupdate!']);
    }
}
