<?php

namespace App\Http\Controllers;

use App\Config;
use Auth;
use Illuminate\Http\Request;

class ConfigController extends Controller{
    
    public function invitation(){
        $data['pageName']   = 'configs';
        $data['configs']    = Config::where('keyword', 'invitation')->first();
        return view('configs.invitation', $data);
    }

    public function saveInvitation(Request $r){
        Config::updateOrCreate(
            [
                'keyword'   => 'invitation'
            ],[
                'keyword'   => 'invitation',
                'post'      => $r->post,
                'users_id'  => Auth::user()->id
        ]);

        return redirect()->back()->with(['success' => 'Data berhasil disimpan !']);
    }
}
