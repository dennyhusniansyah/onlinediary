<?php

namespace App\Http\Controllers;

use App\ProjectTiming;
use App\Timing;
use Auth;
use Illuminate\Http\Request;

class TimingController extends Controller{

    public function __invoke(){
        $data['pageName']   = 'timing';
        $data['timing']     = Timing::all();
        return view('timing.index', $data)->with('no', 1);
    }

    public function addData(){
        $data['pageName']   = 'timing';
        return view('timing.addTiming', $data);
    }

    public function saveData(Request $r){
        $t          = new Timing();
        $t->name    = $r->name;
        $t->start   = $r->start;
        $t->end     = $r->end;
        $t->users_id= Auth::user()->id;
        $t->save();

        return redirect()->back()->with(['success' => 'Data berhasil disimpan !']);
    }

    public function editData($id = ''){
        $data['t']  = Timing::find($id);
        return view('timing.editData', $data);
    }

    public function updateData(Request $r){
        $t          = Timing::find($r->id);
        $t->name    = $r->name;
        $t->start   = $r->start;
        $t->end     = $r->end;
        $t->users_id= Auth::user()->id;
        $t->update();

        return redirect()->back()->with(['success' => 'Data berhasil diupdate !']);
    }

    public function hapusData($id){
        $cek    = ProjectTiming::where('timings_id', $id)->count();
        if($cek > 0){
            return redirect()->back()->with(['error' => 'Data sudah diimplementasikan!']);
        }else{
            Timing::find($id)->delete();
        }
        return redirect()->back()->with(['success' => 'Data berhasil dihapus!']);
    }
}
