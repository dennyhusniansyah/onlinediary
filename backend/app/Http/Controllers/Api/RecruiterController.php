<?php

namespace App\Http\Controllers\Api;

use App\Activity;
use App\Comment;
use App\Http\Controllers\Controller;
use App\ProjectTiming;
use App\Task;
use App\QnA;
use Illuminate\Http\Request;
use Auth;
use Hash;
use App\User;
use Image;
use Illuminate\Support\Carbon;

class RecruiterController extends Controller{
    
    public $successStatus = 200;

    public function login(Request $req){
        if(Auth::attempt(['email' => $req->email, 'password' => $req->password, 'role' => 'recruiter'])){ 
            $r              = User::find(Auth::user()->id);
            $r->fcm_token   = $req->fcm_token;
            $r->status      = 'active';
            $r->update();

            $user   = Auth::user(); 
            $token  = $user->createToken('MyApp');
            return response()->json([
                'success'       => true,
                'token'         => $token->accessToken,
                'token_type'    => 'Bearer',
                'expires_at'    => Carbon::parse($token->token->expires_at)->toDateTimeString()
            ], $this->successStatus); 
        } 
        else{ 
            return response()->json([
                'success'   => false,
                'message'   => 'Email & Password Salah!'
            ], 401); 
        } 
    }

    public function daftarResponder(Request $r){
        try {
            $cek    = User::where('email', $r->email)->count();
            if($cek == 0){
                $u          = new User();
                if($r->photo){
                    $name   = uniqid().'.'.$r->photo->extension();
        
                    $img    = Image::make($r->photo->path());
                    $move   = $img->resize(350, 350, function($const){
                        $const->aspectRatio();
                    })->save('upload/profile/'.$name);
    
                    if($move){
                        if(file_exists('upload/profile/'.$u->photo) && $u->photo != ''){
                            unlink('upload/profile/'.$u->photo);
                        }
                        $u->photo = $name;
                    }
                }
                $u->password= Hash::make(($r->password != '') ? $r->password : uniqid());
                $u->name    = $r->name;
                $u->email   = $r->email;
                $u->phone   = $r->phone;
                $u->birthday= $r->birthday;
                $u->gender  = $r->gender;
                $u->marital = $r->marital;
                $u->role    = 'responder';
                $u->users_id= Auth::user()->id;
                $u->save();
            }else{
                return response()->json([
                    'success'   => false,
                    'message'   => 'Email sudah terdaftar !'
                ], 401); 
            }
        } catch (\Throwable $th) {
            return $this->res($th->getMessage(), false);
        }
        return $this->res([], true);
    }

    public function updateResponder($id = '', Request $r){
        try {
            $u          = User::find($id);
            // if($r->photo){
            //     $name   = uniqid().'.'.$r->photo->extension();
    
            //     $img    = Image::make($r->photo->path());
            //     $move   = $img->resize(350, 350, function($const){
            //         $const->aspectRatio();
            //     })->save('upload/profile/'.$name);

            //     if($move){
            //         if(file_exists('upload/profile/'.$u->photo) && $u->photo != ''){
            //             unlink('upload/profile/'.$u->photo);
            //         }
            //         $u->photo = $name;
            //     }
            // }
            // $u->password= Hash::make(($r->password != '') ? $r->password : uniqid());
            // $u->name    = $r->name;
            // $u->email   = $r->email;
            $u->phone   = $r->phone;
            // $u->birthday= $r->birthday;
            // $u->gender  = $r->gender;
            // $u->marital = $r->marital;
            // $u->role    = 'responder';
            // $u->users_id= Auth::user()->id;
            $u->update();
        } catch (\Throwable $th) {
            return $this->res($th->getMessage(), false);
        }
        return $this->res([], true);
    }

    public function listResponder($id = ''){
        if($id == ''){
            $responder  = User::where(['role' => 'responder', 'users_id' => Auth::user()->id])->get();
        }else{
            $responder  = User::leftJoin('tasks', 'tasks.users_id', 'users.id')
                        ->where(['users.role' => 'responder', 'users.users_id' => Auth::user()->id, 'tasks.projects_id' => $id])
                        ->select('users.*')->get();
        }
        return $responder;
    }

    public function unlistProject($id = ''){
        $task       = Task::where(['projects_id' => $id, 'role' => 'responder'])->pluck('users_id');
        $responder  = User::where(['role' => 'responder', 'users_id' => Auth::user()->id])->whereNotIn('id', $task)->get();
        return $responder;
    }

    public function messageInvitation($id = ''){
        $data   = msgInvite($id);
        return $data;
    }

    public function taskList(){
        $task   = Task::leftJoin('projects', 'projects.id', 'tasks.projects_id')
                ->where(['tasks.users_id' => Auth::user()->id, 'tasks.role' => 'recruiter', 'tasks.status' => 'active'])
                ->select('projects.*')->get();
        
        $list   = $task->map(function($t){
            return [
                'id'            => $t->id,
                'code'          => $t->code,
                'name'          => $t->name,
                'start'         => $t->start,
                'finish'        => $t->finish,
                'days'          => $t->days,
                'quota'         => $t->quota,
                'task'          => $t->task,
                'instruction'   => $t->instruction,
                'description'   => $t->description,
                'timing'        => ProjectTiming::leftJoin('timings', 'timings.id', 'project_timings.timings_id')
                                ->select('timings.name', 'timings.start as start_time', 'timings.end as end_time')
                                ->where('project_timings.projects_id', $t->id)->get()
            ];
        });
        return $list;
    }

    public function addTaskResponder($id = '', $project = ''){
        try {
            $cek    = Task::where(['projects_id' => $project, 'users_id' => $id])->count();
            if($cek == 0){
                $t              = new Task();
                $t->projects_id = $project;
                $t->users_id    = $id;
                $t->role        = 'responder';
                $t->save();
                $notif  = [
                    'title' => 'Task',
                    'body'  => 'Anda memiliki tugas baru!'
                ];
                $data   = [
                    'id_task'   => $t->id
                ];
                sendNotif($r, $notif, $data);
            }else{
                return response()->json([
                    'success'   => false,
                    'message'   => 'Responder sudah terdaftar di project tersebut !'
                ], 401); 
            }
        } catch (\Throwable $th) {
            return $this->res($th->getMessage(), false);
        }
        return $this->res([], true);
    }

    public function profile(){
        $profile    = User::find(Auth::user()->id);
        return $profile;
    }

    public function updateProfile(Request $r){
        try {
            $u          = User::find(Auth::user()->id);
            if($r->photo){
                $name   = uniqid().'.'.$r->photo->extension();
    
                $img    = Image::make($r->photo->path());
                $move   = $img->resize(350, 350, function($const){
                    $const->aspectRatio();
                })->save('upload/profile/'.$name);

                if($move){
                    if(file_exists('upload/profile/'.$u->photo) && $u->photo != ''){
                        unlink('upload/profile/'.$u->photo);
                    }
                    $u->photo = $name;
                }
            }
            if($r->password != ''){
                $u->password= Hash::make($r->password);
            }
            $u->name    = $r->name;
            $u->email   = $r->email;
            $u->phone   = $r->phone;
            $u->birthday= $r->birthday;
            $u->gender  = $r->gender;
            $u->marital = $r->marital;
            $u->update();
        } catch (\Throwable $th) {
            return $this->res($th->getMessage(), false);
        }
        return $this->res([], true);
    }

    public function comments($id = ''){
        $c  = Comment::leftJoin('users', 'users.id', 'comments.users_id')->where('comments.activities_id', $id)
            ->select('comments.*', 'users.name', 'users.photo')->get();
        return $c;
    }

    public function activityResponder($project = '', $date = '', $status = ''){
        if($date == ''){
            $act    = Activity::leftJoin('users', 'users.id', 'activities.responder')
                    ->leftJoin('projects', 'projects.id', 'activities.projects_id')
                    ->selectRaw('activities.*, users.photo as user_photo, users.name as user, projects.name as project')
                    ->where(['activities.recruiter' => Auth::user()->id, 'projects.id' => $project])
                    ->orderBy('activities.id', 'desc')->get();
        }else{
            if($status == ''){
                $act    = Activity::leftJoin('users', 'users.id', 'activities.responder')
                        ->leftJoin('projects', 'projects.id', 'activities.projects_id')
                        ->selectRaw('activities.*, users.photo as user_photo, users.name as user, projects.name as project')
                        ->where(['activities.recruiter' => Auth::user()->id, 'projects.id' => $project])
                        ->where('activities.created_at', 'like', '%'.$date.'%')
                        ->orderBy('activities.id', 'desc')->get();
            }else{
                $act    = Activity::leftJoin('users', 'users.id', 'activities.responder')
                        ->leftJoin('projects', 'projects.id', 'activities.projects_id')
                        ->selectRaw('activities.*, users.photo as user_photo, users.name as user, projects.name as project')
                        ->where(['activities.recruiter' => Auth::user()->id, 'projects.id' => $project, 'activities.status' => $status])
                        ->where('activities.created_at', 'like', '%'.$date.'%')
                        ->orderBy('activities.id', 'desc')->get();
            }
        }
        $activity= $act->map(function($a){
            return [
                'id'            => $a->id,
                'photo'         => $a->photo,
                'name'          => $a->name,
                'status'        => $a->status,
                'created_at'    => $a->created_at,
                'user_photo'    => $a->user_photo,
                'user'          => $a->user,
                'project'       => $a->project,
                'projects_id'   => $a->projects_id,
                'comment'       => Comment::where('activities_id', $a->id)->count() ?? 0
            ];
        });
        return $activity;
    }

    public function activityApproval($id = '', $status = ''){
        try {
            $a          = Activity::find($id);
            $a->status  = ($status == '0') ? 'rejected' : 'approved';
            $a->update();
        } catch (\Throwable $th) {
            return $this->res($th->getMessage(), false);
        }
        return $this->res([], true);
    }

    public function detailActivity($id = ''){
        $act = Activity::leftJoin('users', 'users.id', 'activities.responder')
                ->leftJoin('projects', 'projects.id', 'activities.projects_id')
                ->selectRaw('activities.*, users.photo as user_photo, users.name as user, projects.name as project')
                ->where('activities.id', $id)->first();
        return $act;
    }

    public function detailQna($id = ''){
        $data['activity']   = Activity::find($id);
        $data['qna']        = QnA::leftJoin('questions', 'questions.id', 'qn_a_s.questions_id')
                            ->select('qn_a_s.*', 'questions.type')->where('qn_a_s.activities_id', $id)
                            ->orderBy('questions.sort')->get();
        // return $data;
        return view('recruiter.qna', $data);
        // if($data['activity']->used == 'yes'){
        //     return view('recruiter.used', $data);
        // }else{
        //     return view('recruiter.unused', $data);
        // }
    }
}
