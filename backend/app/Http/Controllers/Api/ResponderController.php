<?php

namespace App\Http\Controllers\Api;

use App\Activity;
use App\Answer;
use App\Bot;
use App\Comment;
use App\Http\Controllers\Controller;
use App\Product;
use App\Project;
use Illuminate\Http\Request;
use Auth;
use App\Timing;
use Hash;
use Image;
use App\Task;
use App\ProjectTiming;
use App\QnA;
use App\Question;
use App\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class ResponderController extends Controller{
    
    public $successStatus = 200;

    public function login(Request $req){
        if(Auth::attempt(['email' => $req->email, 'password' => $req->password, 'role' => 'responder'])){ 
            $r              = User::find(Auth::user()->id);
            $r->fcm_token   = $req->fcm_token;
            $r->status      = 'active';
            $r->update();

            $user   = Auth::user(); 
            $token  = $user->createToken('MyApp');
            return response()->json([
                'success'       => true,
                'token'         => $token->accessToken,
                'token_type'    => 'Bearer',
                'user'          => $user,
                'expires_at'    => Carbon::parse($token->token->expires_at)->toDateTimeString()
            ], $this->successStatus); 
        } 
        else{ 
            return response()->json([
                'success'   => false,
                'message'   => 'Email & Password Salah!'
            ], 401); 
        } 
    }

    public function taskList(){
        $task   = Task::leftJoin('projects', 'projects.id', 'tasks.projects_id')
                ->leftJoin('products', 'products.id', 'projects.products_id')
                ->where(['tasks.users_id' => Auth::user()->id, 'tasks.role' => 'responder', 'tasks.status' => 'active'])
                ->select('projects.*', 'tasks.realtime', 'products.name as product', 'products.id as products_id')->get();
        
        $list   = $task->map(function($t){
            return [
                'id'            => $t->id,
                'code'          => $t->code,
                'name'          => $t->name,
                'start'         => $t->start,
                'finish'        => $t->finish,
                'days'          => $t->days,
                'quota'         => $t->quota,
                'task'          => $t->task,
                'instruction'   => $t->instruction,
                'description'   => $t->description,
                'realtime'      => $t->realtime,
                'product'       => $t->product,
                'products_id'   => $t->products_id,
                'timing'        => ProjectTiming::leftJoin('timings', 'timings.id', 'project_timings.timings_id')
                                ->select('timings.name', 'timings.start as start_time', 'timings.end as end_time')
                                ->where('project_timings.projects_id', $t->id)->get()
            ];
        });
        return $list;
    }

    public function listProduct(){
        $p  = Product::all();
        return $p;
    }

    public function postActivity(Request $r){
        try {
            $timings_id = nowTimingsId(Auth::user()->id, $r->projects_id);
            if($timings_id > 0){
                $a              = new Activity();
                if($r->photo){
                    $name       = 'CLSR_'.Auth::user()->id.'_'.strtotime(now()).'.'.$r->photo->extension();
                    $img        = Image::make($r->photo->path());
                    $move       = $img->resize(350, 350, function($const){
                        $const->aspectRatio();
                    })->save('upload/activity/'.$name);

                    if($move){
                        $a->photo = $name;
                    }
                }
                $a->name        = strtoupper($r->name);
                $a->responder   = Auth::user()->id;
                $a->recruiter   = Auth::user()->users_id;
                $a->projects_id = $r->projects_id;
                // $a->used        = $r->used;
                $a->timings_id  = $timings_id;
                $a->save();
                $notif  = [
                    'title' => 'Questionnaire',
                    'body'  => 'Isi kuesioner sesuai aktifitas yang baru saja anda post untuk melengkapi diary anda, isian tidak boleh kosong & tambahkan gambar.'
                ];
                $data   = [
                    'id_activity'   => $a->id
                ];
                sendNotif(Auth::user()->id, $notif, $data);
            }else{
                return response()->json([
                    'success'   => false,
                    'message'   => 'Periode waktu aktifitas anda tidak pada jadwalnya!'
                ], 401); 
            }
        } catch (\Throwable $th) {
            return $this->res($th->getMessage(), false);
        }
        return $this->res([], true);
    }

    public function profile(){
        $profile    = User::find(Auth::user()->id);
        return $profile;
    }

    public function updateProfile(Request $r){
        try {
            $u          = User::find(Auth::user()->id);
            if($r->photo){
                $name   = uniqid().'.'.$r->photo->extension();

                $img    = Image::make($r->photo->path());
                $move   = $img->resize(350, 350, function($const){
                    $const->aspectRatio();
                })->save('upload/profile/'.$name);

                if($move){
                    if(file_exists('upload/profile/'.$u->photo) && $u->photo != ''){
                        unlink('upload/profile/'.$u->photo);
                    }
                    $u->photo = $name;
                }
            }
            if($r->password != ''){
                $u->password= Hash::make($r->password);
            }
            $u->name    = $r->name;
            $u->email   = $r->email;
            $u->phone   = $r->phone;
            $u->birthday= $r->birthday;
            $u->gender  = $r->gender;
            $u->marital = $r->marital;
            $u->update();
        }catch (\Throwable $th) {
            return $this->res($th->getMessage(), false);
        }
        return $this->res([], true);
    }

    public function listActivity($project = '', $date = '', $status = ''){
        if($date == ''){
            $act    = Activity::leftJoin('users', 'users.id', 'activities.responder')->leftJoin('projects', 'projects.id', 'activities.projects_id')
                    ->selectRaw('activities.*, users.photo as user_photo, users.name as user, projects.name as project')
                    ->where(['activities.responder' => Auth::user()->id, 'projects.id' => $project])
                    ->orderBy('activities.id', 'desc')->get();
        }else{
            if($status == ''){
                $act    = Activity::leftJoin('users', 'users.id', 'activities.responder')->leftJoin('projects', 'projects.id', 'activities.projects_id')
                        ->selectRaw('activities.*, users.photo as user_photo, users.name as user, projects.name as project')
                        ->where(['activities.responder' => Auth::user()->id, 'projects.id' => $project])
                        ->where('activities.created_at', 'like', '%'.$date.'%')
                        ->orderBy('activities.id', 'desc')->get();
            }else{
                $act    = Activity::leftJoin('users', 'users.id', 'activities.responder')->leftJoin('projects', 'projects.id', 'activities.projects_id')
                        ->selectRaw('activities.*, users.photo as user_photo, users.name as user, projects.name as project')
                        ->where(['activities.responder' => Auth::user()->id, 'projects.id' => $project, 'activities.status' => $status])
                        ->where('activities.created_at', 'like', '%'.$date.'%')
                        ->orderBy('activities.id', 'desc')->get();
            }
        }
        $activity= $act->map(function($a){
            return [
                'id'            => $a->id,
                'photo'         => $a->photo,
                'name'          => $a->name,
                'status'        => $a->status,
                'created_at'    => $a->created_at,
                'user_photo'    => $a->user_photo,
                'user'          => $a->user,
                'project'       => $a->project,
                'projects_id'   => $a->projects_id,
                'comment'       => Comment::where('activities_id', $a->id)->count() ?? 0
            ];
        });
        return $activity;
    }

    public function listAllActivity($project = '', $status = ''){
            if($status == ''){
                $act    = Activity::leftJoin('users', 'users.id', 'activities.responder')->leftJoin('projects', 'projects.id', 'activities.projects_id')
                        ->selectRaw('activities.*, users.photo as user_photo, users.name as user, projects.name as project')
                        ->where(['activities.responder' => Auth::user()->id, 'projects.id' => $project])
                        //->where('activities.created_at', 'like', '%'.$date.'%')
                        ->orderBy('activities.id', 'desc')->get();
            }else{
                $act    = Activity::leftJoin('users', 'users.id', 'activities.responder')->leftJoin('projects', 'projects.id', 'activities.projects_id')
                        ->selectRaw('activities.*, users.photo as user_photo, users.name as user_name, projects.name as project_name')
                        ->where(['activities.responder' => Auth::user()->id, 'projects.id' => $project, 'activities.status' => $status])
                        //->where('activities.created_at', 'like', '%'.$date.'%')
                        ->orderBy('activities.id', 'desc')->get();
            }
        $activity= $act->map(function($a){
            return [
                'id'            => $a->id,
                'photo'         => $a->photo,
                'name'          => $a->name,
                'status'        => $a->status,
                'created_at'    => $a->created_at,
                'user_photo'    => $a->user_photo,
                'user'          => $a->user_name,
                'project'       => $a->project_name,
                'projects_id'   => $a->projects_id,
                'comment'       => Comment::where('activities_id', $a->id)->count() ?? 0
            ];
        });
        return $activity;
    }

    public function detailActivity($id = ''){
        $act = Activity::leftJoin('users', 'users.id', 'activities.responder')
                ->leftJoin('projects', 'projects.id', 'activities.projects_id')
                ->selectRaw('activities.*, users.photo as user_photo, users.name as user, projects.name as project')
                ->where('activities.id', $id)->first();
        return $act;
    }

    public function comments($id = ''){
        $c  = Comment::leftJoin('users', 'users.id', 'comments.users_id')->where('comments.activities_id', $id)
            ->select('comments.*', 'users.name', 'users.photo')->get();
        return $c;
    }

    public function sendComment($id = '', Request $r){
        try {
            $c                  = new Comment();
            $c->activities_id   = $id;
            $c->post            = $r->comment;
            $c->users_id        = Auth::user()->id;
            $c->status          = 'send';
            $c->save();
        } catch (\Throwable $th) {
            return $this->res($th->getMessage(), false);
        }
        return $this->res([], true);
    }

    public function qna($act_id = '', $quest_id = ''){
        $data['activity']   = Activity::find($act_id);
        $projects_id        = $data['activity']->projects_id;
        if($quest_id == ''){
            $q_id   = QnA::where('activities_id', $act_id)->pluck('questions_id');
            if(count($q_id) > 0){
                $quest  = Question::whereNotIn('id', $q_id)->where(['status' => 'active', 'projects_id' => $projects_id])->orderBy('sort')->get();
                if(count($quest) > 0){
                    foreach($quest as $q){
                        if($q->parent != null){
                            $dtQna  = QnA::where(['activities_id' => $act_id, 'questions_id' => $q->parent])->first();
                            if($dtQna){
                                if(in_array($q->answer, explode(',', $dtQna->answers_id))){
                                    $data['qna']    = QnA::where(['activities_id' => $act_id, 'questions_id' => $q->id])->first();;
                                    $data['quest']  = Question::find($q->id);
                                    $data['answer'] = Answer::where('questions_id', $q->id)->get();
                                    $data['back']   = QnA::leftJoin('questions', 'questions.id', 'qn_a_s.questions_id')
                                            ->where(['qn_a_s.activities_id' => $act_id])->where('questions.sort', '<', $data['quest']->sort)
                                            ->orderBy('questions.sort', 'desc')->select('questions.*')->first();
                                    return view('api.qna', $data);
                                }
                            }
                        }else{
                            $data['qna']    = QnA::where(['activities_id' => $act_id, 'questions_id' => $q->id])->first();
                            $data['quest']  = Question::find($q->id);
                            $data['answer'] = Answer::where('questions_id', $q->id)->get();
                            $data['back']   = '';
                            return view('api.qna', $data);
                        }
                    }
                    $data['qna']    = '';
                    $data['quest']  = '';
                    $data['answer'] = '';
                    $data['back']   = QnA::leftJoin('questions', 'questions.id', 'qn_a_s.questions_id')
                            ->where('qn_a_s.activities_id', $act_id)->orderBy('questions.sort', 'desc')
                            ->select('questions.*')->first();
                    return view('api.qna', $data);
                }else{
                    $data['qna']    = '';
                    $data['quest']  = '';
                    $data['answer'] = '';
                    $data['back']   = QnA::leftJoin('questions', 'questions.id', 'qn_a_s.questions_id')
                            ->where('qn_a_s.activities_id', $act_id)->orderBy('questions.sort', 'desc')
                            ->select('questions.*')->first();
                    return view('api.qna', $data);
                }
            }else{
                $data['qna']    = '';
                $data['quest']  = Question::where(['status' => 'active', 'projects_id' => $projects_id])->orderBy('sort')->first();
                $data['answer'] = Answer::where('questions_id', $data['quest']->id)->get();
                $data['back']   = '';
                return view('api.qna', $data);
            }
        }else{
            $data['qna']    = QnA::where(['activities_id' => $act_id, 'questions_id' => $quest_id])->first();
            $data['quest']  = Question::where(['status' => 'active', 'id' => $quest_id])->first();
            $data['answer'] = Answer::where('questions_id', $quest_id)->get();
            $data['back']   = QnA::leftJoin('questions', 'questions.id', 'qn_a_s.questions_id')
                    ->where(['qn_a_s.activities_id' => $act_id])->where('questions.sort', '<', $data['quest']->sort)
                    ->orderBy('questions.sort', 'desc')->select('questions.*')->first();
            return view('api.qna', $data);
        }
    }

    public function saveQna(Request $r){
        // return $r;
        $a      = Activity::find($r->id);
        $check_k= 0;
        $check  = [];
        if(isset($r->qna)){
            foreach($r->qna as $k => $v){
                // return $r->qna[$k];
                $key    = explode(',', $k);
                $q      = Question::find($key[0]);
                // return $key;
                if($key[1] == 'optional'){
                    $answer = Answer::find($v);
                    QnA::updateOrCreate(
                        [
                            'activities_id' => $r->id,
                            'questions_id'  => $key[0],
                            'users_id'      => $a->responder
                        ],[
                            'activities_id' => $r->id,
                            'questions_id'  => $key[0],
                            'question'      => $q->question,
                            'answers_id'    => $v,
                            'answer'        => $answer->jawaban,
                            'users_id'      => $a->responder
                        ]
                    );
                }
                if($key[1] == 'answer'){
                    QnA::updateOrCreate(
                        [
                            'activities_id' => $r->id,
                            'questions_id'  => $key[0],
                            'users_id'      => $a->responder
                        ],[
                            'activities_id' => $r->id,
                            'questions_id'  => $key[0],
                            'question'      => $q->question,
                            'answer'        => $v,
                            'users_id'      => $a->responder
                        ]
                    );
                }
                if($key[1] == 'check'){
                    if($check_k != $key[0]){
                        $check  = [];
                    }
                    array_push($check, $key[2]);
                    $check_k = $key[0];
                    $answer = Answer::whereIn('id', $check)->pluck('jawaban')->toArray();
                    QnA::updateOrCreate(
                        [
                            'activities_id' => $r->id,
                            'questions_id'  => $key[0],
                            'users_id'      => $a->responder
                        ],[
                            'activities_id' => $r->id,
                            'questions_id'  => $key[0],
                            'question'      => $q->question,
                            'answers_id'    => implode(",", $check),
                            'answer'        => implode(",", $answer),
                            'users_id'      => $a->responder
                        ]
                    );
                }
                if($key[1] == 'photo'){
                    $photo  = QnA::where(['questions_id' => $key[0], 'activities_id' => $r->id, 'users_id' => $a->responder])->first()->photo ?? '';

                    $name   = uniqid().'.'.$v->extension();
                    $img    = Image::make($v->getRealpath());
                    $img->orientate();
                    $move   = $img->resize(600, 600, function($const){
                        $const->aspectRatio();
                    })->save('upload/activity/'.$name);

                    if($move){
                        if(file_exists('upload/activity/'.$photo) && $photo != ''){
                            unlink('upload/activity/'.$photo);
                        }
                    }
                    QnA::updateOrCreate(
                        [
                            'activities_id' => $r->id,
                            'questions_id'  => $key[0],
                            'users_id'      => $a->responder
                        ],[
                            'activities_id' => $r->id,
                            'questions_id'  => $key[0],
                            'question'      => $q->question,
                            'photo'         => $name,
                            'users_id'      => $a->responder
                        ]
                    );
                }
            }
        }
        $projects_id        = Task::where(['users_id' => $a->responder, 'status' => 'active'])->latest()->first()->projects_id;
        $quest  = Question::whereNotNull('parent')->where('projects_id', $projects_id)->orderBy('sort')->get();
        foreach($quest as $q){
            $qnaQId     = QnA::where(['questions_id' => $q->parent, 'activities_id' => $r->id])->first();
            if($qnaQId){
                $qnaQuestId = Question::where(['parent' => $q->parent, 'projects_id' => $projects_id])->whereNotIn('answer', explode(',', $qnaQId->answers_id))->pluck('id');
                QnA::whereIn('questions_id', $qnaQuestId)->where('activities_id', $r->id)->delete();
            }
        }
        return redirect(url('api/responder/qna/'.$r->id));
    }
/*
    public function qna($act_id = '', $quest_id = ''){
        $a                  = Activity::find($act_id);
        $data['activity']   = $a;
        if($a->used == 'yes'){
            if($quest_id == ''){
                $qna    = QnA::leftJoin('questions', 'questions.id', 'qn_a_s.questions_id')
                            ->where(['qn_a_s.activities_id' => $act_id, 'questions.status' => 'active', 'questions.used' => 'yes'])
                            ->orderBy('questions.sort', 'desc')->first();
                if($qna != ''){
                    $data['qna']    = QnA::leftJoin('questions', 'questions.id', 'qn_a_s.questions_id')
                                    ->where('qn_a_s.activities_id', $act_id)->where('questions.sort', '>', $qna->sort)
                                    ->orderBy('questions.sort', 'asc')->first();
                    $parent         = Question::where('sort', '>', $qna->sort)->whereIn('answer', explode(',', $qna->answers_id))
                                    ->where(['parent' => 80, 'used' => 'yes', 'status' => 'active'])
                                    ->get();
                    if(count($parent) > 0){
                        return 'oke';
                        if($parent->answer == $qna->answers_id){
                            $data['question']   = $parent;
                        }else{
                            $data['question']   = Question::where('sort', '>', $qna->sort)->whereNull(['parent', 'answer'])
                                                ->where(['used' => 'yes', 'status' => 'active'])->orderBy('sort', 'asc')->first();
                        }
                    }else{
                        $data['question']   = Question::where('sort', '>', $qna->sort)->whereNull(['parent', 'answer'])
                                            ->where(['used' => 'yes', 'status' => 'active'])->orderBy('sort', 'asc')->first();
                    }
                    if($data['question']){
                        $data['answer']     = Answer::where('questions_id', $data['question']->id)->get();
                    }
                    $data['back']       = $qna;
                }else{
                    $data['qna']        = '';
                    $data['question']   = Question::where(['used' => 'yes', 'status' => 'active'])->orderBy('sort', 'asc')->first();
                    $data['back']       = '';
                    $data['answer']     = Answer::where('questions_id', $data['question']->id)->get();
                }
            }else{
                $quest              = Question::find($quest_id);
                $data['qna']        = QnA::where(['activities_id' => $act_id, 'questions_id' => $quest_id])->first();
                $data['back']       = Question::where('sort', '<', $quest->sort)
                                    ->select('questions.*', 'questions.id as questions_id')->orderBy('sort', 'desc')->first();
                $data['question']   = Question::where(['id' => $quest_id, 'used' => 'yes', 'status' => 'active'])->orderBy('sort', 'asc')->first();
                $data['answer']     = Answer::where('questions_id', $data['question']->id)->get();
            }
            return view('api.used', $data);
        }else{
            $cek    = QnA::where('activities_id', $act_id)->first();
            if($cek){
                $data['check']      = true;
                $data['question']   = QnA::leftJoin('questions', 'questions.id', 'qn_a_s.questions_id')
                        ->select('qn_a_s.*', 'questions.topic', 'questions.pertanyaan', 'questions.type', 'questions.required', 'questions.id')
                        ->where(['qn_a_s.activities_id' => $act_id, 'questions.status' => 'active'])->get();
            }else{
                $data['check']      = false;
                $data['question']   = Question::where(['used' => 'no', 'status' => 'active'])
                        ->where('timings_id', 'like', '%'.$a->timings_id.'%')
                        ->select('topic')->groupBy('topic')->orderBy('id', 'asc')->get();
            }
            return view('api.unused', $data);
        }
    }
    public function saveQna(Request $r){
        $a      = Activity::find($r->id);
        $a->used= $r->used;
        $a->update();
        $check_k= 0;
        $check  = [];
        $next   = [];
        if($r->used == 'no'){
            $quest_id   = Question::where('used', 'yes')->pluck('id');
            QnA::whereIn('questions_id', $quest_id)->where('activities_id', $r->id)->delete();
        }
        foreach($r->qna as $k => $v){
            $key    = explode(',', $k);
            $q      = Question::find($key[0]);
            if($key[1] == 'optional'){
                $answer = Answer::find($v);
                QnA::updateOrCreate(
                    [
                        'activities_id' => $r->id,
                        'questions_id'  => $key[0],
                        'users_id'      => $a->responder
                    ],[
                        'activities_id' => $r->id,
                        'questions_id'  => $key[0],
                        'question'      => $q->pertanyaan,
                        'answers_id'    => $v,
                        'answer'        => $answer->jawaban,
                        'users_id'      => $a->responder
                    ]
                );
            }
            if($key[1] == 'answer'){
                QnA::updateOrCreate(
                    [
                        'activities_id' => $r->id,
                        'questions_id'  => $key[0],
                        'users_id'      => $a->responder
                    ],[
                        'activities_id' => $r->id,
                        'questions_id'  => $key[0],
                        'question'      => $q->pertanyaan,
                        'answer'        => $v,
                        'users_id'      => $a->responder
                    ]
                );
            }
            if($key[1] == 'check'){
                if($check_k != $key[0]){
                    $check  = [];
                }
                array_push($check, $key[2]);
                $check_k = $key[0];
                $answer = Answer::whereIn('id', $check)->pluck('jawaban')->toArray();
                QnA::updateOrCreate(
                    [
                        'activities_id' => $r->id,
                        'questions_id'  => $key[0],
                        'users_id'      => $a->responder
                    ],[
                        'activities_id' => $r->id,
                        'questions_id'  => $key[0],
                        'question'      => $q->pertanyaan,
                        'answers_id'    => implode(",", $check),
                        'answer'        => implode(",", $answer),
                        'users_id'      => $a->responder
                    ]
                );
            }
            if($key[1] == 'photo'){
                $photo  = QnA::where(['questions_id' => $key[0], 'activities_id' => $r->id, 'users_id' => $a->responder])->first()->photo ?? '';

                $name   = uniqid().'.'.$v->extension();
                $img    = Image::make($v->path());
                $move   = $img->resize(350, 350, function($const){$const->aspectRatio();})->save('upload/activity/'.$name);
                if($move){
                    if(file_exists('upload/activity/'.$photo) && $photo != ''){
                        unlink('upload/activity/'.$photo);
                    }
                }
                QnA::updateOrCreate(
                    [
                        'activities_id' => $r->id,
                        'questions_id'  => $key[0],
                        'users_id'      => $a->responder
                    ],[
                        'activities_id' => $r->id,
                        'questions_id'  => $key[0],
                        'question'      => $q->pertanyaan,
                        'photo'         => $name,
                        'users_id'      => $a->responder
                    ]
                );
            }
        }
        // if($q->used == 'yes'){
        //     $qna    = QnA::leftJoin('questions', 'questions.id', 'qn_a_s.questions_id')->where('qn_a_s.activities_id', $r->id)->select('qn_a_s.*', 'questions.sort')->latest()->first();
        //     $parent = Question::where('sort', '>', $q->sort)->whereIn('parent', explode(',', $qna->answers_id))
        //             ->where(['used' => $q->used, 'status' => 'active'])->orderBy('sort', 'asc')->first();
        //     // return $qna;
        //     if($parent){
        //         if($parent->answer == $v){
        //             $next   = $parent;
        //         }else{
        //             $sub_parent = Question::where('sort', '>', $q->sort)->where(['parent' => $parent->id, 'used' => $a->used, 'status' => 'active'])->orderBy('sort', 'asc')->first();
        //             QnA::where(['activities_id' => $r->id, 'questions_id' => $parent->id])->delete();
        //             if($sub_parent){
        //                 QnA::where(['activities_id' => $r->id, 'questions_id' => $sub_parent->id])->delete();
        //             }
        //             $next   = Question::where('sort', '>', $q->sort)->whereNull(['parent', 'answer'])->where(['used' => $a->used, 'status' => 'active'])->orderBy('sort', 'asc')->first();
        //         }
        //     }else{
        //         $next   = Question::where('sort', '>', $q->sort)->whereNull(['parent', 'answer'])->where(['used' => $a->used, 'status' => 'active'])->orderBy('sort', 'asc')->first();
        //     }

        //     if($next){
        //         return redirect(url('api/responder/qna/'.$r->id.'/'.$next->id));
        //     }
        // }
        return redirect(url('api/responder/qna/'.$r->id));
    }
    public function qna($id = ''){
        $a                  = Activity::find($id);
        $data['activity']   = $a;
        $qna                = QnA::where('activities_id', $id)->get();
        if($a->used == 'yes'){
            $question           = Question::where('used', 'yes')->get();
            $data['question']   = $question->map(function($q) use($qna){
                return [
                    'id'            => $q->id,
                    'question'      => $q->pertanyaan,
                    'answer_type'   => $q->answer_type,
                    'parent'        => $q->parent,
                    'ans_parent'    => $q->ans_parent,
                    'required'      => $q->required,
                    'type'          => $q->type,
                    'answer'        => Answer::where('questions_id', $q->id)->get(),
                    'qna'           => $qna->where('questions_id', $q->id)->first() ?? ''
                ];
            });
            return view('api.used', $data);
        }else{
            $cek    = QnA::where('activities_id', $id)->first();
            if($cek){
                $data['check']      = true;
                $data['question']   = QnA::leftJoin('questions', 'questions.id', 'qn_a_s.questions_id')
                        ->select('qn_a_s.*', 'questions.topic', 'questions.pertanyaan', 'questions.type', 'questions.required', 'questions.id')
                        ->where('qn_a_s.activities_id', $id)->get();
            }else{
                $data['check']      = false;
                $data['question']   = Question::where('used', 'no')->where('timings_id', 'like', '%'.$a->timings_id.'%')
                        ->select('topic')->groupBy('topic')->orderBy('id', 'asc')->get();
            }
            // return $data['question'];
            return view('api.unused', $data);
        }
    }
    public function saveQna(Request $r){
        $a      = Activity::find($r->id);
        $check_k= 0;
        $check  = [];
        foreach($r->qna as $k => $v){
            $key    = explode(',', $k);
            $q      = Question::find($key[0]);
            if($key[1] == 'optional'){
                $answer = Answer::find($v);
                QnA::updateOrCreate(
                    [
                        'activities_id' => $r->id,
                        'questions_id'  => $key[0],
                        'users_id'      => $a->responder
                    ],[
                        'activities_id' => $r->id,
                        'questions_id'  => $key[0],
                        'question'      => $q->pertanyaan,
                        'answers_id'    => $v,
                        'answer'        => $answer->jawaban,
                        'users_id'      => $a->responder
                    ]
                );
            }
            if($key[1] == 'answer'){
                QnA::updateOrCreate(
                    [
                        'activities_id' => $r->id,
                        'questions_id'  => $key[0],
                        'users_id'      => $a->responder
                    ],[
                        'activities_id' => $r->id,
                        'questions_id'  => $key[0],
                        'question'      => $q->pertanyaan,
                        'answer'        => $v,
                        'users_id'      => $a->responder
                    ]
                );
            }
            if($key[1] == 'check'){
                if($check_k != $key[0]){
                    $check  = [];
                }
                array_push($check, $key[2]);
                $check_k = $key[0];
                $answer = Answer::whereIn('id', $check)->pluck('jawaban')->toArray();
                QnA::updateOrCreate(
                    [
                        'activities_id' => $r->id,
                        'questions_id'  => $key[0],
                        'users_id'      => $a->responder
                    ],[
                        'activities_id' => $r->id,
                        'questions_id'  => $key[0],
                        'question'      => $q->pertanyaan,
                        'answers_id'    => implode(",", $check),
                        'answer'        => implode(",", $answer),
                        'users_id'      => $a->responder
                    ]
                );
            }
            if($key[1] == 'photo'){
                $photo  = QnA::where(['questions_id' => $key[0], 'activities_id' => $r->id, 'users_id' => $a->responder])->first()->photo ?? '';

                $name   = uniqid().'.'.$v->extension();
                $img    = Image::make($v->path());
                $move   = $img->resize(350, 350, function($const){$const->aspectRatio();})->save('upload/activity/'.$name);
                if($move){
                    if(file_exists('upload/activity/'.$photo) && $photo != ''){
                        unlink('upload/activity/'.$photo);
                    }
                }
                QnA::updateOrCreate(
                    [
                        'activities_id' => $r->id,
                        'questions_id'  => $key[0],
                        'users_id'      => $a->responder
                    ],[
                        'activities_id' => $r->id,
                        'questions_id'  => $key[0],
                        'question'      => $q->pertanyaan,
                        'photo'         => $name,
                        'users_id'      => $a->responder
                    ]
                );
            }
        }
        return redirect()->back()->with(['success' => 'Terima kasih atas partisipasinya']);
    }*/

    public function jenisAktivitas($jenis = ''){
        $q  = Question::where(['topic' => $jenis, 'status' => 'active'])->get();
        return $q;
    }
}
