<?php

namespace App\Http\Controllers;

use App\Activity;
use App\User;
use Auth;
use Illuminate\Http\Request;

class ProgresController extends Controller{
    
    public function __invoke(){
        $id = Auth::user()->id;
        $user_role = Auth::user()->role;
        $data['pageName']   = 'progres';
        if($user_role == 'recruiter')
        {
            $act        = Activity::where('status', 'approved')
            ->selectRaw('DATE(created_at) as date, COUNT(timings_id) as timing, responder')
            ->groupBy('responder', 'date')->get();
$resp       = User::leftJoin('tasks', 'tasks.users_id', 'users.id')
            ->leftJoin('projects', 'projects.id', 'tasks.projects_id')
            ->select('users.*', 'projects.name as project', 'projects.days', 'projects.id as project_id', 'projects.capaian')
            ->where(['users.role' => 'responder', 'users.status' => 'active'])->groupBy('users.id')->get();
        } else {
            $act        = Activity::where('status', 'approved')
                        ->selectRaw('DATE(created_at) as date, COUNT(timings_id) as timing, responder')
                        ->groupBy('responder', 'date')->get();
            $resp       = User::leftJoin('tasks', 'tasks.users_id', 'users.id')
                        ->leftJoin('projects', 'projects.id', 'tasks.projects_id')
                        ->select('users.*', 'projects.name as project', 'projects.days', 'projects.id as project_id', 'projects.capaian')
                        ->where(['users.role' => 'responder', 'users.status' => 'active'])->groupBy('users.id')->get();
        }

        $data['progres']    = $resp->map(function($r) use($act){
            $pt     = countProjectTiming($r->project_id);
            $cp     = $pt * $r->capaian / 100;
            return [
                'id'        => $r->id,
                'name'      => $r->name,
                'email'     => $r->email,
                'project_id'=> $r->project_id,
                'project'   => $r->project,
                'days'      => $r->days,
                'capaian'   => $r->capaian,
                'progres'   => $act->where('responder', $r->id)->where('timing', '>=', $cp)->count()
            ];
        });
        return view('progres.index', $data)->with('no', 1);
    }
}
