<?php

namespace App\Http\Controllers;

use App\Product;
use App\Project;
use Illuminate\Http\Request;

class ProductController extends Controller{
    
    public function __invoke(){
        $data['pageName']   = 'product';
        $data['product']    = Product::all();
        return view('product.index', $data)->with('no', 1);
    }

    public function saveData(Request $r){
        $cek = Product::where('name', $r->name)->count();
        if($cek == 0){
            $p          = new Product();
            $p->name    = $r->name;
            $p->save();
        }else{
            return redirect()->back()->with(['error' => 'Nama produk sudah ada !']);
        }
        return redirect()->back()->with(['success' => 'Data berhasil disimpan !']);
    }

    public function editData($id = ''){
        $data['p']  = Product::find($id);
        return view('product.editData', $data);
    }

    public function updateData(Request $r){
        $p          = Product::find($r->id);
        $p->name    = $r->name;
        $p->update();
        return redirect()->back()->with(['success' => 'Data berhasil diupdate !']);
    }

    public function hapusData($id = ''){
        $cek = Project::where('products_id', $id)->count();
        if($cek > 0){
            return redirect()->back()->with(['error' => 'Produk sudah diimplementasikan pada project !']);
        }else{
            Product::find($id)->delete();
            return redirect()->back()->with(['success' => 'Data berhasil dihapus !']);
        }
    }
}
