<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Project;
use App\QnA;
use App\Question;
use App\Timing;
use Illuminate\Http\Request;

class QnAController extends Controller{
    
    public function __invoke(){
        $data['pageName']   = 'qna';
        // $data['quest']      = Question::leftJoin('answers', 'answers.questions_id', 'questions.id')
        //                     ->selectRaw('questions.*, GROUP_CONCAT(answers.jawaban SEPARATOR"#") as answers, GROUP_CONCAT(answers.id SEPARATOR"#") as answer_id')
        //                     ->whereNull('questions.parent')->groupBy('questions.id')->orderBy('questions.sort')->get();
        $data['quest']      = [];
        $data['projects']   = Project::all();
        $data['code']       = '';
        $data['parent']     = Question::where('status', 'active')->orderBy('sort')->get();
        $data['status']     = 'semua';
        $data['form']       = 'new';
        $data['question']   = '';
        return view('qna.index', $data)->with('no',1);
    }

    public function filter(Request $r){
        $data['pageName']   = 'qna';
        $projects_id        = Project::where('code', $r->projects_code)->first()->id;
        if($r->status == 'semua'){
            $data['quest']      = Question::leftJoin('answers', 'answers.questions_id', 'questions.id')
                                ->selectRaw('questions.*, GROUP_CONCAT(answers.jawaban SEPARATOR"#") as answers, GROUP_CONCAT(answers.id SEPARATOR"#") as answer_id')
                                ->whereNull('questions.parent')->where(['questions.projects_id' => $projects_id])
                                ->groupBy('questions.id')->orderBy('questions.sort')->get();
        }else{
            $data['quest']      = Question::leftJoin('answers', 'answers.questions_id', 'questions.id')
                                ->selectRaw('questions.*, GROUP_CONCAT(answers.jawaban SEPARATOR"#") as answers, GROUP_CONCAT(answers.id SEPARATOR"#") as answer_id')
                                ->whereNull('questions.parent')->where(['questions.status' => $r->status, 'questions.projects_id' => $projects_id])
                                ->groupBy('questions.id')->orderBy('questions.sort')->get();
        }
        $data['projects']   = Project::all();
        $data['code']       = $r->projects_code;
        $data['parent']     = Question::where('status', 'active')->orderBy('sort')->get();
        $data['status']     = $r->status;
        $data['form']       = 'new';
        $data['question']   = '';
        return view('qna.index', $data)->with('no',1);
    }

    public function edit($id = ''){
        $data['pageName']   = 'qna';
        $data['quest']      = Question::leftJoin('answers', 'answers.questions_id', 'questions.id')
                            ->selectRaw('questions.*, GROUP_CONCAT(answers.jawaban SEPARATOR"#") as answers, GROUP_CONCAT(answers.id SEPARATOR"#") as answer_id')
                            ->whereNull('questions.parent')->groupBy('questions.id')->orderBy('questions.sort')->get();
        $data['projects']   = Project::all();
        $data['question']   = Question::find($id);
        $data['code']       = Project::find($data['question']->projects_id)->code;
        $question           = Question::where('projects_id', $data['question']->projects_id)->where('id', '!=', $id)->orderBy('sort')->get();
        $data['parent']     = $question;
        $data['status']     = 'semua';
        $data['form']       = 'edit';
        return view('qna.index', $data)->with('no',1);
    }

    public function updateQuestion(Request $r){
        $q              = Question::find($r->id);
        $q->question    = $r->question;
        $q->parent      = $r->parent;
        $q->answer      = $r->answer_parent;
        $q->projects_id = $r->projects_id;
        $q->answer_type = $r->answer_type;
        $q->type        = $r->type;
        $q->required    = $r->required;
        if(isset($r->timing)){
            $q->timings_id  = implode(',', array_keys($r->timing));
        }else{
            $q->timings_id  = NULL;
        }
        $q->update();
        $project        = Project::find($r->projects_id);
        return redirect(url('qna/filter?projects_code='.$project->code.'&status=semua'))->with(['success' => 'Data berhasil disimpan!']);
    }

    public function saveQuestion(Request $r){
        $quest    = explode('#', $r->question);
        foreach($quest as $qu){
            $que            = Question::latest()->first();
            $q              = new Question();
            $q->sort        = ($que) ? $que->id + 1 : 1;
            $q->question    = $qu;
            $q->parent      = $r->parent;
            $q->answer      = $r->answer_parent;
            $q->projects_id = $r->projects_id;
            $q->answer_type = $r->answer_type;
            $q->type        = $r->type;
            $q->required    = $r->required;
            if(isset($r->timing)){
                $q->timings_id  = implode(',', array_keys($r->timing));
            }
            $q->save();
        }
        return redirect()->back()->with(['success' => 'Data berhasil disimpan!']);
    }

    public function saveAnswer(Request $r){
        $ans    = explode('#', $r->answer);
        foreach($ans as $an){
            $a              = new Answer();
            $a->questions_id= $r->question;
            $a->jawaban     = $an;
            $a->save();
        }
        return redirect()->back()->with(['success' => 'Data berhasil disimpan!']);
    }

    public function getAnswer($id = ''){
        return Answer::where('questions_id', $id)->get();
    }

    public function getQuestion($proj_id = ''){
        $quest  = Question::where('projects_id', $proj_id)->get();
        return $quest;
    }

    public function deleteQuestion($id = ''){
        $cek    = QnA::where('questions_id', $id)->count();
        $q      = Question::find($id);
        if($cek > 0){
            $q->status  = 'inactive';
            $q->update();
        }else{
            $q->delete();
            Answer::where('questions_id', $id)->delete();
        }
        return redirect()->back()->with(['success' => 'Data berhasil dihapus !']);
    }

    public function activatedQuestion($id = ''){
        $q          = Question::find($id);
        $q->status  = 'active';
        $q->update();
        return redirect()->back()->with(['success' => 'Data berhasil diupdate !']);
    }

    public function deleteAnswer($id = ''){
        $cek    = QnA::where('answers_id', 'like', '%'.$id.'%')->count();
        if($cek > 0){
            return redirect()->back()->with(['error' => 'Data sudah diimplementasikan!']);
        }else{
            Answer::find($id)->delete();
        }
        return redirect()->back()->with(['success' => 'Data berhasil dihapus !']);
    }

    public function schema(){
        $data['quest']  = Question::whereNull('parent')->get();
        return view('qna.schema', $data);
    }

    public function sort($proj_code = ''){
        $proj_id            = Project::where('code', $proj_code)->first()->id;
        $quest              = Question::where('projects_id', $proj_id)->orderBy('sort')->get();
        $data['quest']      = $quest->map(function($q){
            return [
                'id'        => $q->id,
                'question'  => $q->question,
                'status'    => $q->status,
                'parent'    => Question::where('id', $q->parent)->first()->question ?? '',
                'answer'    => Answer::where('id', $q->answer)->first()->jawaban ?? ''
            ];
        });
        // $data['quest']  = Question::orderBy('sort')->get();
        return view('qna.sort', $data);
    }

    public function sorted(Request $r){
        foreach($r->quest as $k => $v){
            $q      = Question::find($v);
            $q->sort= $k+1;
            $q->update();
        }
        return redirect()->bacK()->with(['success' => 'Data berhasil diupdate !']);
    }
}
