<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	protected function res($data = [], $success = false){
		if($success == false){
			$res = [
				'message' => $data,
				'success' => $success,
			];
		}else{
			$res = [
				'message' => '',
				'success' => $success,
			];
		}

		return response()->json($res);
	}
}
