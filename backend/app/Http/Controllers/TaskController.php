<?php

namespace App\Http\Controllers;

use App\Project;
use App\Task;
use App\User;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class TaskController extends Controller{

    public function recruiter(){
        $data['pageName']   = 'tasks';
        $data['project']    = Project::all();
        $data['project_id'] = 0;
        $data['task']       = Task::leftJoin('projects', 'projects.id', 'tasks.projects_id')
                            ->leftJoin('users', 'users.id', 'tasks.users_id')
                            ->selectRaw('users.*, GROUP_CONCAT(CONCAT(projects.name,"||",projects.id) SEPARATOR "#") as project, GROUP_CONCAT(tasks.status SEPARATOR "#") as taskStatus')
                            ->where('users.role', 'recruiter')->groupBy('tasks.users_id')->get();
        // return $data['task'];
        return view('task.recruiter', $data)->with('no', 1);
    }

    public function recruiterFilter(Request $r){
        $data['pageName']   = 'tasks';
        $data['project']    = Project::all();
        $data['project_id'] = $r->project;
        $data['task']       = Task::leftJoin('projects', 'projects.id', 'tasks.projects_id')
                            ->leftJoin('users', 'users.id', 'tasks.users_id')
                            ->selectRaw('users.*, GROUP_CONCAT(CONCAT(projects.name,"||",projects.id) SEPARATOR "#") as project')
                            ->where('projects.id', ($r->project == 0) ? '>' : '=', $r->project)
                            ->where('users.role', 'recruiter')->groupBy('tasks.users_id')->get();
        return view('task.recruiter', $data)->with('no', 1);
    }

    public function editTaskRecruiter(){
        $data['projects']   = Project::all();
        $data['recruiter']  = User::where('role', 'recruiter')->get();
        return view('task.editTaskRecruiter', $data);
    }

    public function taskRecruiter($id = ''){
        $task   = Task::leftJoin('projects', 'projects.id', 'tasks.projects_id')
                ->where(['tasks.users_id' => $id, 'tasks.role' => 'recruiter'])
                ->select('tasks.id', 'tasks.projects_id', 'projects.name', 'projects.code', 'tasks.users_id as recruiter')->get();

        return DataTables::of($task)
            ->addColumn('aksi', function($task){
                $cek    = Task::leftJoin('users', 'users.id', 'tasks.users_id')
                        ->where(['users.users_id' => $task->recruiter, 'tasks.role' => 'responder', 'tasks.projects_id' => $task->projects_id])->count();
                if($cek > 0){
                    return '<button class="btn btn-labeled btn-sm btn-warning"span class="btn-label"><i class="fa fa-spinner"></i></span></button>';
                    // return '<span class="label label-warning">On Progress</span>';
                }else{
                    return '<button class="btn btn-labeled btn-sm btn-danger" onclick="deleteTask('.$task->recruiter.','.$task->id.')"><span class="btn-label"><i class="fa fa-trash"></i></span> Del</button>';
                }
            })
            ->rawColumns(['aksi'])
            ->make();
    }

    public function addTaskRecruiter($t = '', $r = ''){
        Task::updateOrCreate(
            [
                'projects_id'   => $t,
                'role'          => 'recruiter',
                'users_id'      => $r
            ],[
                'projects_id'   => $t,
                'role'          => 'recruiter',
                'users_id'      => $r
            ]
        );
        $p  = Project::find($t);
        $notif  = [
            'title' => 'Task',
            'body'  => 'Anda telah asign di project '.$p->code.' '.$p->name.', silahkan login ulang. terima kasih'
        ];
        $data   = [
            'id_activity'   => $t
        ];
        sendNotif($r, $notif, $data);
    }

    public function deleteTaskRecruiter($user = '', $id = ''){
        $cek    = User::leftJoin('tasks', 'tasks.users_id', 'users.id')
                ->where(['users.users_id' => $user, 'tasks.projects_id' => $id, 'tasks.role' => 'responder'])->count();
        if($cek > 0){
            $t          = Task::where(['users_id' => $user, 'projects_id' => $id, 'role' => 'recruiter'])->first();
            $t->status  = 'inactive';
            $t->update();

            $tasks      = User::leftJoin('tasks', 'tasks.users_id', 'users.id')
                        ->where(['users.users_id' => $user, 'tasks.projects_id' => $id, 'tasks.role' => 'responder'])
                        ->select('tasks.*')->get();
            foreach($tasks as $t){
                $ts         = Task::find($t->id);
                $ts->status = 'inactive';
                $ts->update();
            }
            return redirect()->back()->with(['success' => 'Project dinonaktifkan!']);
        }else{
            Task::where(['users_id' => $user, 'projects_id' => $id])->delete();
        }
        return redirect()->back()->with(['success' => 'Data berhasil dihapus !']);
    }

    public function activatedTaskRecruiter($user = '', $id = ''){
        $t          = Task::where(['users_id' => $user, 'projects_id' => $id, 'role' => 'recruiter'])->first();
        $t->status  = 'active';
        $t->update();

        $tasks      = User::leftJoin('tasks', 'tasks.users_id', 'users.id')
                    ->where(['users.users_id' => $user, 'tasks.projects_id' => $id, 'tasks.role' => 'responder'])
                    ->select('tasks.*')->get();
        foreach($tasks as $t){
            $ts         = Task::find($t->id);
            $ts->status = 'active';
            $ts->update();
        }
        return redirect()->back()->with(['success' => 'Data berhasil dihapus !']);
    }

    public function finishedTaskRecruiter($user = '', $id = ''){
        $t          = Task::where(['users_id' => $user, 'projects_id' => $id, 'role' => 'recruiter'])->first();
        $t->status  = 'finish';
        $t->update();

        $tasks      = User::leftJoin('tasks', 'tasks.users_id', 'users.id')
                    ->where(['users.users_id' => $user, 'tasks.projects_id' => $id, 'tasks.role' => 'responder'])
                    ->select('tasks.*')->get();
        foreach($tasks as $t){
            $ts         = Task::find($t->id);
            $ts->status = 'finish';
            $ts->update();
        }
        return redirect()->back()->with(['success' => 'Data berhasil dihapus !']);
    }

    public function responder(){
        $data['pageName']   = 'tasks';
        $data['project']    = Project::all();
        $data['project_id'] = 0;
        $data['task']       = Task::leftJoin('projects', 'projects.id', 'tasks.projects_id')->leftJoin('users', 'users.id', 'tasks.users_id')
                            ->selectRaw('users.*, GROUP_CONCAT(CONCAT(projects.name,"||",projects.id) SEPARATOR "#") as project')
                            ->where('users.role', 'responder')->groupBy('tasks.users_id')->get();
        return view('task.responder', $data)->with('no', 1);
    }

    public function responderFilter(Request $r){
        $data['pageName']   = 'tasks';
        $data['project']    = Project::all();
        $data['project_id'] = $r->project;
        $data['task']       = Task::leftJoin('projects', 'projects.id', 'tasks.projects_id')
                            ->leftJoin('users', 'users.id', 'tasks.users_id')
                            ->selectRaw('users.*, GROUP_CONCAT(CONCAT(projects.name,"||",projects.id) SEPARATOR "#") as project')
                            ->where('projects.id', ($r->project == 0) ? '>' : '=', $r->project)
                            ->where('users.role', 'responder')->groupBy('tasks.users_id')->get();
        return view('task.responder', $data)->with('no', 1);
    }

    public function editTaskResponder(){

        $data['responder']   =  User::where('role', 'responder')->get();
        $data['recruiter']   =  User::leftJoin('tasks', 'tasks.users_id', 'users.id')->leftJoin('projects', 'projects.id', 'tasks.projects_id')
                                ->where('users.role', 'recruiter')
                                ->selectRaw('users.*,projects.id as pid, projects.name as pname')->get();
        return view('task.editTaskResponder', $data);
    }

    public function taskResponder($id = ''){
        $task   = Task::leftJoin('projects', 'projects.id', 'tasks.projects_id')->leftJoin('users', 'tasks.users_id', 'users.id')
                ->where(['tasks.users_id' => $id, 'tasks.role' => 'responder'])
                ->select('tasks.id', 'tasks.projects_id', 'projects.name', 'projects.code', 'tasks.users_id as recruiter','users.name as recname')->get();

        return DataTables::of($task)
            ->addColumn('aksi', function($task){
                $cek    = Task::leftJoin('users', 'users.id', 'tasks.users_id')
                        ->where(['users.users_id' => $task->recruiter, 'tasks.role' => 'responder', 'tasks.projects_id' => $task->projects_id])->count();
                if($cek > 0){
                    return '<button class="btn btn-labeled btn-sm btn-warning"span class="btn-label"><i class="fa fa-spinner"></i></span></button>';
                    // return '<span class="label label-warning">On Progress</span>';
                }else{
                    return '<button class="btn btn-labeled btn-sm btn-danger" onclick="deleteTask('.$task->recruiter.','.$task->id.')"><span class="btn-label"><i class="fa fa-trash"></i></span> Del</button>';
                }
            })
            ->rawColumns(['aksi'])
            ->make();
    }

    public function addTaskResponder($rc = '', $rp = '',$p = ''){
        $resp           = User::find($rp);
        $resp->role     = 'responder';
        $resp->users_id  = $rc;
        $resp->update();

        Task::updateOrCreate(
            [
                'projects_id'   => $p,
                'role'          => 'responder',
                'users_id'      => $rp
            ],[
                'projects_id'   => $p,
                'role'          => 'responder',
                'users_id'      => $rp
            ]
        );
        $proj  = Project::find($p);
        $notif  = [
            'title' => 'Task',
            'body'  => 'Anda telah asign di project '.$proj->code.' '.$proj->name.', silahkan login ulang. terima kasih'
        ];
        $data   = [
            'id_activity'   => $p
        ];
        sendNotif($rp, $notif, $data);
    }

    public function deleteTaskResponder($user = '', $id = ''){
        $cek    = User::leftJoin('tasks', 'tasks.users_id', 'users.id')
                ->where(['users.users_id' => $user, 'tasks.projects_id' => $id, 'tasks.role' => 'responder'])->count();
        if($cek > 0){
            $t          = Task::where(['users_id' => $user, 'projects_id' => $id, 'role' => 'recruiter'])->first();
            $t->status  = 'inactive';
            $t->update();

            $tasks      = User::leftJoin('tasks', 'tasks.users_id', 'users.id')
                        ->where(['users.users_id' => $user, 'tasks.projects_id' => $id, 'tasks.role' => 'responder'])
                        ->select('tasks.*')->get();
            foreach($tasks as $t){
                $ts         = Task::find($t->id);
                $ts->status = 'inactive';
                $ts->update();
            }
            return redirect()->back()->with(['success' => 'Project dinonaktifkan!']);
        }else{
            Task::where(['users_id' => $user, 'projects_id' => $id])->delete();
        }
        return redirect()->back()->with(['success' => 'Data berhasil dihapus !']);
    }

    public function activatedTaskResponder($user = '', $id = ''){
        $t          = Task::where(['users_id' => $user, 'projects_id' => $id, 'role' => 'recruiter'])->first();
        $t->status  = 'active';
        $t->update();

        $tasks      = User::leftJoin('tasks', 'tasks.users_id', 'users.id')
                    ->where(['users.users_id' => $user, 'tasks.projects_id' => $id, 'tasks.role' => 'responder'])
                    ->select('tasks.*')->get();
        foreach($tasks as $t){
            $ts         = Task::find($t->id);
            $ts->status = 'active';
            $ts->update();
        }
        return redirect()->back()->with(['success' => 'Data berhasil dihapus !']);
    }

    public function finishedTaskResponder($user = '', $id = ''){
        $t          = Task::where(['users_id' => $user, 'projects_id' => $id, 'role' => 'recruiter'])->first();
        $t->status  = 'finish';
        $t->update();

        $tasks      = User::leftJoin('tasks', 'tasks.users_id', 'users.id')
                    ->where(['users.users_id' => $user, 'tasks.projects_id' => $id, 'tasks.role' => 'responder'])
                    ->select('tasks.*')->get();
        foreach($tasks as $t){
            $ts         = Task::find($t->id);
            $ts->status = 'finish';
            $ts->update();
        }
        return redirect()->back()->with(['success' => 'Data berhasil dihapus !']);
    }
}
