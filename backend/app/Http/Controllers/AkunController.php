<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Illuminate\Http\Request;

class AkunController extends Controller{
    
    public function __invoke(){
        $data['pageName']   = 'akun';
        $data['akun']       = User::find(Auth::user()->id);
        return view('akun.index', $data);
    }

    public function updateAkun(Request $r){
        $u          = User::find(Auth::user()->id);
        $u->name    = $r->name;
        if($r->userfile){
            $name = uniqid().'.'.$r->userfile->extension();
            $move = $r->userfile->move(public_path('upload/profile'), $name);
            if($move){
                if(file_exists(public_path($u->photo)) && $u->photo != ''){
                    unlink(public_path($u->photo));
                }
                $u->photo = $name;
            }
        }
        if($r->password != ''){
            $u->password = bcrypt($r->password);
        }
        $u->update();

        return redirect()->back()->with(['success' => 'Akun berhasil diupdate !']);
    }
}
