<?php

namespace App\Http\Controllers;

use App\User;
use App\Activity;
use App\Exports\exportResume;
use App\Project;
use App\QnA;
use DB;
use App\Question;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ResumeController extends Controller{
    
    public function __invoke(){
        $data['pageName']   = 'resume';
        $data['responder']  = User::where(['role' => 'responder'])->get();
        $data['resp_id']    = 0;
        $data['projects']   = Project::all();
        $data['project']    = Project::first();
        $min                = Activity::orderBy('created_at', 'asc')->first() ?? '';
        $max                = Activity::orderBy('created_at', 'desc')->first() ?? '';
        $data['from_date']  = ($min != '') ? $min->created_at->format('Y-m-d') : '';
        $data['to_date']    = ($max != '') ? $max->created_at->format('Y-m-d') : '';
        $data['quest']      = Question::where('projects_id', $data['project']->id)->orderBy('sort')->get();
        if($data['project']){
            $activity           = Activity::leftJoin('users', 'users.id', 'activities.responder')
                                ->leftJoin('timings', 'timings.id', 'activities.timings_id')
                                ->where(['users.status' => 'active', 'activities.projects_id' => $data['project']->id])
                                ->whereBetween(DB::raw('DATE(activities.created_at)'), [$data['from_date'], $data['to_date']])
                                ->select('users.*', 'activities.name as activity', 'activities.id as activities_id', 'activities.status', 'timings.name as timing', 'activities.created_at as created')->get();
            $data['resume']     = $activity->map(function($a){
                return [
                    'name'          => $a->name,
                    'email'         => $a->email,
                    'phone'         => $a->phone,
                    'birthday'      => $a->birthday,
                    'gender'        => $a->gender,
                    'marital'       => $a->marital,
                    'status'        => $a->status,
                    'activity'      => $a->activity,
                    'timing'        => $a->timing,
                    'created'       => $a->created,
                    'activities_id' => $a->activities_id,
                    'qna'           => QnA::where('activities_id', $a->activities_id)->select('questions_id', 'answer', 'photo')->get()
                ];
            });
        }else{
            $data['resume'] = [];
        }
        // return $data['resume'];
        return view('resume.index', $data)->with('no', 1);
    }

    public function filter(Request $r){
        $data['pageName']   = 'resume';
        $data['responder']  = User::where(['role' => 'responder'])->get();
        $data['resp_id']    = $r->resp_id;
        $data['from_date']  = $r->from_date;
        $data['to_date']    = $r->to_date;
        $data['projects']   = Project::all();
        $data['project']    = Project::find($r->project);
        $data['quest']      = Question::where('projects_id', $r->project)->orderBy('sort')->get();
        $activity           = Activity::leftJoin('users', 'users.id', 'activities.responder')
                            ->leftJoin('timings', 'timings.id', 'activities.timings_id')
                            ->where('users.id', ($r->resp_id == 0) ? '>' : '=', $r->resp_id)
                            ->where(['users.status' => 'active', 'activities.projects_id' => $r->project])
                            ->whereBetween(DB::raw('DATE(activities.created_at)'), [$data['from_date'], $data['to_date']])
                            ->select('users.*', 'activities.name as activity', 'activities.id as activities_id', 'activities.status', 'timings.name as timing', 'activities.created_at as created')->get();
        $data['resume']     = $activity->map(function($a){
            return [
                'name'          => $a->name,
                'email'         => $a->email,
                'phone'         => $a->phone,
                'birthday'      => $a->birthday,
                'gender'        => $a->gender,
                'marital'       => $a->marital,
                'status'        => $a->status,
                'activity'      => $a->activity,
                'timing'        => $a->timing,
                'created'       => $a->created,
                'activities_id' => $a->activities_id,
                'qna'           => QnA::where('activities_id', $a->activities_id)->select('questions_id', 'answer', 'photo')->get()
            ];
        });
        return view('resume.index', $data)->with('no', 1);
    }

    public function export($resp = '', $from = '', $to = ''){
        return Excel::download(new exportResume($resp, $from, $to), 'resume.xlsx');
    }
}
