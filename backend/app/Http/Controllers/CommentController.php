<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Bot;
use Auth;
use App\Comment;
use App\Product;
use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class CommentController extends Controller{
    
    public function all(){
        $data['pageName']   = 'comments';
        $lists              = Activity::leftJoin('users', 'users.id', 'activities.responder')
                            ->select('users.*', 'activities.id as act_id', 'activities.name as act')->get();
        $data['activity']   = '';
        $data['comments']   = [];
        $data['act_id']     = 0;
        $data['lists']      = $lists->map(function($a){
            return [
                'id'    => $a->id,
                'act_id'=> $a->act_id,
                'act'   => $a->act,
                'name'  => $a->name,
                'photo' => $a->photo,
                'unread'=> Comment::where('users_id', '!=', Auth::user()->id)->where(['activities_id' => $a->act_id, 'status' => 'send'])->count(),
                'last'  => Comment::where(['activities_id' => $a->act_id])->latest()->first()->post ?? '',
                'time'  => Comment::where(['activities_id' => $a->act_id])->latest()->first()->created_at ?? ''
            ];
        });
        $data['lists'] = $data['lists']->SortByDesc('time');
        return view('comment.all', $data)->with('no', 1);
    }

    public function activity($id = ''){
        $data['pageName']   = 'comments';
        if(Auth::user()->role == 'pm'){
            Comment::where('activities_id', $id)->where('users_id', '!=', Auth::user()->id)->update(['status' => 'read']);
        }
        $lists              = Activity::leftJoin('users', 'users.id', 'activities.responder')
                            ->select('users.*', 'activities.id as act_id', 'activities.name as act')->get();
        $data['activity']   = Activity::leftJoin('timings', 'timings.id', 'activities.timings_id')
                            ->leftJoin('users', 'users.id', 'activities.responder')
                            ->select('activities.*', 'users.photo as profile', 'timings.name as timing', 'users.name as user', 'users.phone', 'users.email')->where('activities.id', $id)->first();
        $data['comments']   = Comment::leftJoin('users', 'users.id', 'comments.users_id')
                            ->where('comments.activities_id', $id)->select('comments.*', 'users.name')->get();
        $data['act_id']     = $id;
        $data['lists']   = $lists->map(function($a){
            return [
                'id'    => $a->id,
                'act_id'=> $a->act_id,
                'act'   => $a->act,
                'name'  => $a->name,
                'photo' => $a->photo,
                'unread'=> Comment::where('users_id', '!=', Auth::user()->id)->where(['activities_id' => $a->act_id, 'status' => 'send'])->count(),
                'last'  => Comment::where(['activities_id' => $a->act_id])->latest()->first()->post ?? '',
                'time'  => Comment::where(['activities_id' => $a->act_id])->latest()->first()->created_at ?? ''
            ];
        });
        $data['lists'] = $data['lists']->SortByDesc('time');
        return view('comment.all', $data)->with('no', 1);
    }

    public function sendComment(Request $r){
        $c                  = new Comment();
        $c->activities_id   = $r->id;
        $c->post            = $r->comment;
        $c->users_id        = Auth::user()->id;
        $c->status          = 'send';
        $c->save();
        $a  = Activity::leftJoin('users', 'users.id', 'activities.responder')->where('activities.id', $r->id)->first();

        $notif  = [
            'title' => 'Comments',
            'body'  => $r->comment
        ];
        $data   = [
            'id_activity'   => $r->id
        ];
        sendNotif($a->responder, $notif, $data);
        return redirect()->back();
    }

    public function bot(){
        $data['pageName']   = 'comments';
        // $data['projects']   = Project::all();
        $data['bots']       = Bot::leftJoin('products', 'products.id', 'bots.products_id')->get();
        return view('comment.bot', $data)->with('no', 1);
    }

    public function saveData(Request $r){
        $prod           = Product::where('name', 'car')->first()->id;
        $b              = new Bot();
        $b->post        = $r->bot;
        $b->products_id = $prod;
        $b->save();

        return redirect()->back()->with(['success' => 'Data berhasil disimpan !']);
    }

    public function deleteBot($id = ''){
        $cek    = Comment::where('bots_id', $id)->count();
        if($cek > 0){
            return redirect()->back()->with(['success' => 'Bot comment sudah diterapka !']);
        }else{
            Bot::find($id)->delete();
        }

        return redirect()->back()->with(['success' => 'Data berhasil dihapus !']);
    }

    public function roomChat($act = ''){
        if(Auth::user()->role == 'pm'){
            Comment::where('users_id', '!=', Auth::user()->id)->update(['status' => 'read']);
        }
        $comment        = Comment::leftJoin('users', 'users.id', 'comments.users_id')->where('comments.activities_id', $act)
                        ->select('comments.*', 'users.photo')->get();
        $data['chat']   = $comment->map(function($c){
            return [
                'post'  => getChat($c->photo, $c->post, $c->created_at, $c->users_id),
            ];
        });
        return $data;
    }

    public function getActivity($act = ''){
        $act    = Activity::leftJoin('projects', 'projects.id', 'activities.projects_id')
                ->leftJoin('products', 'products.id', 'projects.products_id')
                ->leftJoin('users', 'users.id', 'activities.responder')
                ->select('activities.*', 'users.name as user', 'users.phone', 'products.name as product')
                ->where('activities.id', $act)->first();
        $act['photo']   = url('upload/activity/'.$act['photo']);
        $act['time']    = $act['created_at']->format('d M Y H:i');
        return $act;
    }
}
