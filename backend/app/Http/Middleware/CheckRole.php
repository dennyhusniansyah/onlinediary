<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($req, Closure $next, $roles){
        $roles  = explode('|', $roles);

        if(in_array(Auth::user()->role, $roles)){
            return $next($req);
        }

        return abort(503, 'Anda tidak memiliki hak akses');
    }
}
