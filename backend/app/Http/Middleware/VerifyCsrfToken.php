<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'http://app.vespernetwork.id/api/responder/qna',
        'http://app.vespernetwork.id/api/responder/jenis-aktivitas',
        'http://app.vespernetwork.id/api/responder/save-qna',
    ];
}
