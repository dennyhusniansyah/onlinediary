<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model{
    
    protected $fillable = ['projects_id', 'users_id', 'realtime', 'role'];
}
