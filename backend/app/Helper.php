<?php

use App\Comment;
use App\Config;
use App\Question;
use App\User;
use App\QnA;
use App\Task;
use App\ProjectTiming;
use App\Timing;
use Illuminate\Support\Facades\Hash;

function isParent($id){
    $cek    = Question::where('parent', $id)->count();
    return ($cek > 0) ? true : false;
}

function subQuestion($id){
    $q      = Question::leftJoin('answers as p_ans', 'p_ans.id', 'questions.answer')
            ->leftJoin('answers', 'answers.questions_id', 'questions.id')
            ->selectRaw('questions.*, p_ans.jawaban, GROUP_CONCAT(answers.jawaban SEPARATOR"#") as answers, GROUP_CONCAT(answers.id SEPARATOR"#") as answer_id')
            ->where('questions.parent', $id)->groupBy('questions.id')->orderBy('questions.sort')->get();
    return $q;
}

function msgInvite($id = ''){
    $pass       = uniqid();
    $u          = User::find($id);
    $u->password= Hash::make($pass);
    $u->update();

    $config        = Config::where('keyword', 'invitation')->first();
    if($config){
        $data['pesan']  = $config->post;
        $data['pesan']  = str_replace('[role]', $u->role, $data['pesan']);
        $data['pesan']  = str_replace('[email]', $u->email, $data['pesan']);
        $data['pesan']  = str_replace('[password]', $pass, $data['pesan']);
        $data['phone']  = $u->phone;
    }else{
        $data['pesan']  = '';
        $data['phone']  = '';
    }
    return $data;
}

function getChat($name = '', $chat = '', $time = '', $users_id = ''){
    // $avatar = ($photo == '') ? asset('assets/img/avatar.png') : url('upload/profile/'.$photo);
    if(Auth::user()->id == $users_id){
        return '<div class="messenger-message-container from bg-blue">
            <div class="messages">
                <ul>
                    <li>
                        <div class="time-ago">'.$time->format('H:i').'</div>
                        <div class="message">
                            <div>'.$chat.'</div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>';
    }else{
        return '<div class="messenger-message-container">
            <div class="messages">
                <ul>
                    <li>
                        <div class="message">
                            <div><span class="text-left">'.$name.'</span>'.$chat.'</div>
                        </div>
                        <div class="time-ago">'.$time->format('H:i').'</div>
                    </li>
                </ul>
            </div>
        </div>';
    }
}

function resumeQnA($act = '', $quest = '', $status = ''){
    $qna    = QnA::where(['activities_id' => $act, 'questions_id' => $quest])->first();
    if($qna){
        if($qna->photo != ''){
            if($status == 'tampil'){
                return '<button onclick="modalPict(\'Foto\', \''.url('upload/activity/'.$qna->photo).'\')" class="btn btn-closer btn-sm btn-labeled"><span class="btn-label"><i class="fa fa-image"></i></span>VIEW</button>';
            }else{
                return '<a href="'.url('upload/activity/'.$qna->photo).'">'.url('upload/activity/'.$qna->photo).'<a/>';
            }
        }else{
            return $qna->answer;
        }
    }
}

function commentUnread(){
    $c  = Comment::where('users_id', '!=', Auth::user()->id)->where('status', 'send')->count();
    if($c > 0){
        return '<span class="label label-custom label-pill label-danger pull-right">'.$c.'</span>';
    }
    return '';
}

function nowTimingsId($users_id = '', $projects_id = ''){
    $now    = now()->format('H:i:s');
    if($now < '05:00:00'){
        $timings = ProjectTiming::leftJoin('timings', 'timings.id', 'project_timings.timings_id')
            ->leftJoin('tasks', 'tasks.projects_id', 'project_timings.projects_id')
            ->where(['project_timings.projects_id' => $projects_id, 'tasks.users_id' => $users_id, 'timings.end' => '05:00:00'])
            ->select('timings.*')->first();
        return $timings->id;
    }else{
        $timings = ProjectTiming::leftJoin('timings', 'timings.id', 'project_timings.timings_id')
                ->leftJoin('tasks', 'tasks.projects_id', 'project_timings.projects_id')
                ->where(['project_timings.projects_id' => $projects_id, 'tasks.users_id' => $users_id])
                ->select('timings.*')->orderBy('timings.start', 'desc')->get();
        foreach($timings as $t){
            if($now >= $t->start){
                return $t->id;
            }
        }
    }
    return 0;
    // $task   = Task::where('users_id', $users_id)->pluck('projects_id');
    // $pt     = ProjectTiming::whereIn('projects_id', $task)->orderBy('start_time', 'desc')->first();
    // if($time > $pt->start_time){
    //     $timing = ProjectTiming::whereIn('projects_id', $task)->where('start_time', '<', $time)->first();
    // }else{
    //     $timing = ProjectTiming::whereIn('projects_id', $task)->where('end_time', '>=', $time)
    //             ->orderBy('end_time', 'asc')->first();
    // }
    // if($timing){
    //     return $timing->timings_id;
    // }else{
    //     return 0;
    // }
}

function countProjectTiming($id = ''){
    $c  = ProjectTiming::where('projects_Id', $id)->count();
    return $c;
}

function checkQnA($act_id = ''){
    $qna = QnA::where('activities_id', $act_id)->count();
    return $qna;
}

function checkMasterQuestion($id = ''){
    $cek = Question::where('parent', $id)->count();
    if($cek > 0){
        return 'onchange="ikiMaster(\'siaaaaap\')"';
    }
    return '';
}

function checkTime(){
    $times  = Timing::all();
    foreach($times as $t){
        $nowHour    = now()->format('H');
        $nowMinute  = now()->format('i');
        $H          = (int)date('H', strtotime($t->start));
        $M          = (int)date('i', strtotime($t->start));
        $diffH      = ($nowHour - $H) * 60;
        $diffM      = $nowMinute - $M;
        $diff       = $diffH + $diffM;
        if($diff >= 0 && $diff < 5){
        // if($diff < 0){
            $resp   = ProjectTiming::leftJoin('tasks', 'tasks.projects_id', 'project_timings.projects_id')
                    ->leftJoin('projects', 'projects.id', 'tasks.projects_id')
                    ->leftJoin('timings', 'timings.id', 'project_timings.timings_id')
                    ->leftJoin('users', 'users.id', 'tasks.users_id')
                    ->where(['tasks.role' => 'responder', 'project_timings.timings_id' => $t->id, 'users.status' => 'active', 'tasks.status' => 'active'])
                    ->select('users.*', 'timings.name as timing', 'projects.id as projects_id', 'projects.name as project')->get();
            return $resp;
        }
    }
    return null;
}

function sendNotif($id = '', $notif = '', $data = ''){
    define('API_ACCESS_KEY', 'AAAAhZWy4X8:APA91bFH87Jl5XimlheR26Vplgu_JvRJ896kuljKBh0yEpKbCTd54Pt9e6B14K3xCmTtjbzfmHDI7U6-i3swFh1a6Fnv7PD0T6sfZmQeHgoJ1syLqfmXu-pjD4pb_aj3MRhzMmh7MZwl');
    $u = User::find($id);
    
    // $msg = [
    //     'message' 	=> 'here is a message. message',
    //     'title'		=> 'This is a title. title',
    //     'subtitle'	=> 'This is a subtitle. subtitle',
    //     'tickerText'=> 'Ticker text here...Ticker text here...Ticker text here',
    //     'vibrate'	=> 1,
    //     'sound'		=> 1,
    //     'largeIcon'	=> 'large_icon',
    //     'smallIcon'	=> 'small_icon'
    // ];
    // $notif  = [
    //     'title' => 'Pekerjaan baru',
    //     'body'  => 'Anda memiliki list baru'
    // ];

    $fields = [
        'to'                => $u->fcm_token,
        // 'registration_ids'  => $tokens,
        'data'              => $data,
        'notification'      => $notif
    ];
    
    $headers = [
        'Authorization: key=' . API_ACCESS_KEY,
        'Content-Type: application/json'
    ];
    
    $ch = curl_init();
    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
    curl_setopt( $ch,CURLOPT_POST, true );
    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
    $result = curl_exec($ch );
    curl_close( $ch );
}