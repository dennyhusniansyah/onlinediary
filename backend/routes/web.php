<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('dashboard/send-notif', 'DashboardController@sendNotif');
// Route::get('dashboard/send-notif', 'DashboardController@sendNotif');
Route::get('dashboard/test/{usr}/{pro}', 'DashboardController@test');

// Route::get('/', function () {
//     return redirect('login');
// });

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::group(['middleware' => ['web', 'auth', 'role:admin|pm|recruiter']], function () {

    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('/', 'DashboardController');
        Route::get('/activity/{status}/{id}', 'DashboardController@updateActivity');
        Route::get('/filter', 'DashboardController@filter');
        Route::get('/qna/{act_id}', 'DashboardController@qna');
    });

    Route::group(['prefix' => 'resume'], function () {
        Route::get('/', 'ResumeController');
        Route::get('/filter', 'ResumeController@filter');
        Route::get('/export/{resp}/{from}/{to}', 'ResumeController@export');
    });

    Route::group(['prefix' => 'progres'], function () {
        Route::get('/', 'ProgresController');
    });

    Route::group(['prefix' => 'product'], function () {
        Route::get('/', 'ProductController');
        Route::view('/add-data', 'product.addData');
        Route::post('/save-data', 'ProductController@saveData');
        Route::get('/edit-data/{id}', 'ProductController@editData');
        Route::get('/hapus-data/{id}', 'ProductController@hapusData');
        Route::post('/update-data', 'ProductController@updateData');
    });

    Route::group(['prefix' => 'comments'], function () {
        Route::get('/all', 'CommentController@all');
        Route::get('/activity/{id}', 'CommentController@activity');
        Route::post('/send-comment', 'CommentController@sendComment');
        // Route::get('/bot', 'CommentController@bot');
        // Route::post('/save-data', 'CommentController@saveData');
        // Route::get('/delete-bot/{id}', 'CommentController@deleteBot');
        Route::get('/room-chat/{act}', 'CommentController@roomChat');
        Route::get('/get-activity/{act}', 'CommentController@getActivity');
    });

    Route::group(['prefix' => 'timing'], function () {
        Route::get('/', 'TimingController');
        Route::get('/add-data', 'TimingController@addData');
        Route::get('/edit-data/{id}', 'TimingController@editData');
        Route::post('/save-data', 'TimingController@saveData');
        Route::post('/update-data', 'TimingController@updateData');
        Route::get('/hapus-data/{id}', 'TimingController@hapusData');
    });

    Route::group(['prefix' => 'configs'], function () {
        Route::get('/invitation', 'ConfigController@invitation');
        Route::post('/save-invitation', 'ConfigController@saveInvitation');
    });

    Route::group(['prefix' => 'tasks'], function () {
        Route::get('/recruiter', 'TaskController@recruiter');
        Route::get('/recruiter/filter', 'TaskController@recruiterFilter');
        Route::get('/edit-task-recruiter', 'TaskController@editTaskRecruiter');
        Route::get('/task-recruiter/{id}', 'TaskController@taskRecruiter');
        Route::get('/delete-task-recruiter/{user}/{id}', 'TaskController@deleteTaskRecruiter');
        Route::get('/activated-task-recruiter/{user}/{id}', 'TaskController@activatedTaskRecruiter');
        Route::get('/finished-task-recruiter/{user}/{id}', 'TaskController@finishedTaskRecruiter');
        Route::get('/add-task-recruiter/{t}/{r}', 'TaskController@addTaskRecruiter');

        Route::get('/responder', 'TaskController@responder');
        Route::get('/responder/filter', 'TaskController@responderFilter');
        Route::get('/edit-task-responder', 'TaskController@editTaskResponder');
        Route::get('/task-responder/{id}', 'TaskController@taskResponder');
        Route::get('/delete-task-responder/{user}/{id}', 'TaskController@deleteTaskResponder');
        Route::get('/activated-task-responder/{user}/{id}', 'TaskController@activatedTaskResponder');
        Route::get('/finished-task-responder/{user}/{id}', 'TaskController@finishedTaskResponder');
        Route::get('/add-task-responder/{rc}/{rp}/{p}', 'TaskController@addTaskResponder');

    });

    Route::group(['prefix' => 'qna'], function () {
        Route::get('/', 'QnAController');
        Route::get('/filter', 'QnAController@filter');
        Route::get('/sort/{proj_code}', 'QnAController@sort');
        Route::get('/schema', 'QnAController@schema');
        Route::get('/edit/{id}', 'QnAController@edit');
        ROute::post('/sorted', 'QnAController@sorted');
        Route::post('/save-question', 'QnAController@saveQuestion');
        Route::post('/update-question', 'QnAController@updateQuestion');
        Route::post('/save-answer', 'QnAController@saveAnswer');
        Route::get('/get-answer/{question}', 'QnAController@getAnswer');
        Route::get('/delete-question/{id}', 'QnAController@deleteQuestion');
        Route::get('/activated-question/{id}', 'QnAController@activatedQuestion');
        Route::get('/delete-answer/{id}', 'QnAController@deleteAnswer');
        Route::get('/get-question/{proj_id}', 'QnAController@getQuestion');
    });

    Route::group(['prefix' => 'project'], function () {
        Route::get('/', 'ProjectController');
        Route::get('/add-data', 'ProjectController@addData');
        Route::get('/edit-data/{id}', 'ProjectController@editData');
        Route::post('/save-data', 'ProjectController@saveData');
        Route::post('/update-data', 'ProjectController@updateData');
        Route::get('/hapus-data/{id}', 'ProjectController@hapusData');
    });

    Route::group(['prefix' => 'akun'], function () {
        Route::get('/', 'AkunController');
        Route::post('/update-akun', 'AkunController@updateAkun');
    });

    Route::group(['prefix' => 'users'], function () {
        Route::get('/invite/{id}', 'UsersController@invite');

        Route::get('/administrator', 'UsersController@administrator');
        Route::view('/add-administrator', 'users.addAdmin');
        Route::post('/save-administrator', 'UsersController@saveAdmin');
        Route::get('/edit-administrator/{id}', 'UsersController@editAdmin');
        Route::post('/update-administrator', 'UsersController@updateAdmin');

        Route::get('/project-manager', 'UsersController@projectManager');
        Route::view('/add-project-manager', 'users.addProjectManager');
        Route::post('/save-project-manager', 'UsersController@saveProjectManager');
        Route::get('/edit-project-manager/{id}', 'UsersController@editProjectManager');
        Route::post('/update-project-manager', 'UsersController@updateProjectManager');

        Route::get('/recruiter', 'UsersController@recruiter');
        Route::view('/add-recruiter', 'users.addRecruiter');
        Route::post('/save-recruiter', 'UsersController@saveRecruiter');
        Route::get('/edit-recruiter/{id}', 'UsersController@editRecruiter');
        Route::post('/update-recruiter', 'UsersController@updateRecruiter');

        Route::get('/responder', 'UsersController@responder');
        Route::get('/inactive-responder/{id}', 'UsersController@inactiveResponder');
        Route::get('/delete-responder/{id}', 'UsersController@deleteResponder');
        Route::view('/add-responder', 'users.addResponder');
        Route::post('/save-responder', 'UsersController@saveResponder');
        Route::get('/edit-responder/{id}', 'UsersController@editResponder');
        Route::post('/update-responder', 'UsersController@updateResponder');
    });
});
