<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('recruiter/login', 'Api\RecruiterController@login');
Route::post('responder/login', 'Api\ResponderController@login');

Route::group(['prefix' => 'recruiter', 'middleware' => ['auth:api', 'role:recruiter']], function () {
    Route::post('/daftar-responder', 'Api\RecruiterController@daftarResponder');
    Route::post('/update-responder/{id}', 'Api\RecruiterController@updateResponder');
    Route::get('/list-responder/{id?}', 'Api\RecruiterController@listResponder');
    Route::get('/unlist-project/{id?}', 'Api\RecruiterController@unlistProject');
    Route::get('/message-invitation/{id}', 'Api\RecruiterController@messageInvitation');
    Route::get('/task-list', 'Api\RecruiterController@taskList');
    Route::get('/add-task-responder/{id}/{project}', 'Api\RecruiterController@addTaskResponder');
    Route::get('/profile', 'Api\RecruiterController@profile');
    Route::post('/update-profile', 'Api\RecruiterController@updateProfile');
    Route::get('/activity-responder/{project}/{date?}/{status?}', 'Api\RecruiterController@activityResponder');
    Route::get('/activity-approval/{id}/{status}', 'Api\RecruiterController@activityApproval');
    Route::get('/detail-activity/{id}', 'Api\RecruiterController@detailActivity');
    Route::get('/comments/{id}', 'Api\RecruiterController@comments');
});

Route::group(['prefix' => 'responder', 'middleware' => ['auth:api', 'role:responder']], function () {
    Route::get('/task-list', 'Api\ResponderController@taskList');
    // Route::get('/list-product', 'Api\ResponderController@listProduct');
    Route::post('/post-activity', 'Api\ResponderController@postActivity');
    Route::get('/profile', 'Api\ResponderController@profile');
    Route::post('/update-profile', 'Api\ResponderController@updateProfile');
    Route::get('/list-activity/{project}/{date?}/{status?}', 'Api\ResponderController@listActivity');
    Route::get('/list-all-activity/{project}/{status?}', 'Api\ResponderController@listAllActivity');
    Route::get('/comments/{id}', 'Api\ResponderController@comments');
    Route::post('/send-comment/{id}', 'Api\ResponderController@sendComment');
    Route::get('/detail-activity/{id}', 'Api\ResponderController@detailActivity');
    // Route::get('/qna/{id}', 'Api\ResponderController@qna');
    // Route::get('/jenis-aktivitas/{jenis}', 'Api\ResponderController@jenisAktivitas');
    // Route::post('/save-qna', 'Api\ResponderController@saveQna');
});
Route::get('recruiter/detail-qna/{id}', 'Api\RecruiterController@detailQna');
Route::get('responder/qna/{act_id?}/{quest_id?}', 'Api\ResponderController@qna');
Route::get('responder/jenis-aktivitas/{jenis}', 'Api\ResponderController@jenisAktivitas');
Route::post('responder/save-qna', 'Api\ResponderController@saveQna');
// Route::get('recruiter/detail-qna/{id}', 'Api\RecruiterController@detailQna');