<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        User::insert([
            [
                'name'      => 'Admin',
                'email'     => 'admin@mail.com',
                'password'  => Hash::make('superAdmin20'),
                'role'      => 'admin'
            ],[
                'name'      => 'Project Manajer',
                'email'     => 'pm@mail.com',
                'password'  => Hash::make('superPm20'),
                'role'      => 'pm'
            ],[
                'name'      => 'Recruiter',
                'email'     => 'recruiter@mail.com',
                'password'  => Hash::make('superRecruiter20'),
                'role'      => 'recruiter'
            ]
        ]);
    }
}
