<?php

use App\Timing;
use Illuminate\Database\Seeder;

class TimingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        Timing::insert([
            ['name' => 'Pagi', 'start' => '05:00', 'end' => '11:00', 'users_id' => 1],
            ['name' => 'Siang', 'start' => '11:00', 'end' => '14:00', 'users_id' => 1],
            ['name' => 'Senja', 'start' => '14:00', 'end' => '18:00', 'users_id' => 1],
            ['name' => 'Sore', 'start' => '18:00', 'end' => '21:00', 'users_id' => 1],
            ['name' => 'Malam', 'start' => '21:00', 'end' => '05:00', 'users_id' => 1]
        ]);
    }
}
