<?php

use App\Config;
use Illuminate\Database\Seeder;

class ConfigsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        Config::insert(
            [   
                'keyword'   => 'invitation',
                'post'      => 'Selamat, Anda terdaftar di app *Closers* sebagai *[role]*. Silahkan login dengan email *[email]* dan password *[password]*',
                'users_id'  => 1
            ]
        );
    }
}
