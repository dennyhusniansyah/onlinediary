<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->string('code', 10);
            $table->string('name', 25);
            $table->string('description');
            // $table->date('start');
            // $table->date('finish');
            $table->string('task');
            $table->string('instruction');
            $table->integer('days');
            $table->integer('quota');
            $table->integer('capaian');
            $table->integer('products_id');
            $table->integer('users_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
