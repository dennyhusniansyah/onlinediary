<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->id();
            $table->integer('sort');
            $table->string('question');
            $table->integer('parent')->nullable();
            $table->integer('answer')->nullable();
            $table->string('timings_id', 20)->nullable();
            $table->enum('answer_type', ['single', 'multi'])->default('single');
            $table->enum('type', ['text', 'file', 'number'])->default('text');
            $table->enum('required', ['Y', 'N'])->default('Y');
            // $table->enum('group', ['Y', 'N'])->default('N');
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
