<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectTimingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_timings', function (Blueprint $table) {
            $table->id();
            $table->integer('projects_id');
            $table->integer('timings_id');
            // $table->date('start_date');
            $table->time('start_time');
            // $table->date('end_date');
            $table->time('end_time');
            $table->integer(('users_id'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_timings');
    }
}
