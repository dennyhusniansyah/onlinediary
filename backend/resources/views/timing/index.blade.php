@extends('master')

@push('style')
    <link rel="stylesheet" href="{{ asset('assets/css/lib/clockpicker/bootstrap-clockpicker.min.css') }}">
@endpush

@section('content')
    <header class="section-header">
        <div class="tbl">
            <div class="tbl-row">
                <div class="tbl-cell">
                    <h4 class="pull-left">Timing</h4>
                    <button onclick="showAjaxModal('Tambah Waktu', '{{ url('timing/add-data') }}')" class="btn btn-closer btn-labeled btn-header pull-right"><span class="btn-label"><i class="fa fa-plus"></i></span>TAMBAH</button>
                </div>
            </div>
        </div>
    </header>
    <section class="card">
        <div class="card-block">
            <div class="row">
                <table class="display table table-striped table-bordered dataTable">
                    <thead>
                        <tr class="text-center">
                            <th width="15px">No</th>
                            <th>Aksi</th>
                            <th>Jadwal</th>
                            <th>Dari Jam</th>
                            <th>Sampai Jam</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($timing as $t)
                            <tr>
                                <td class="text-center">{{ $no++ }}</td>
                                <td class="text-center" width="80px">
                                    <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                                        <button class="btn btn-sm btn-success" onclick="showAjaxModal('Edit Jadwal', '{{ url('timing/edit-data/'.$t->id) }}')"><i class="fa fa-pencil-alt"></i></button>
                                        <button class="btn btn-sm btn-danger" onclick="confirmModal('{{ url('timing/hapus-data/'.$t->id) }}')"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                                <td>{{ $t->name }}</td>
                                <td class="text-center">{{ date('H:i', strtotime($t->start)) }}</td>
                                <td class="text-center">{{ date('H:i', strtotime($t->end)) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection

@push('script')
    <script src="{{ asset('assets/js/lib/clockpicker/bootstrap-clockpicker.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/datatables-net/datatables.min.js') }}"></script>
	<script>
        $('.dataTable').DataTable();
    </script>
@endpush