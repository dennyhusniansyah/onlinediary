<form method="POST" action="{{ url('timing/update-data') }}">
    @csrf
    <input type="hidden" name="id" value="{{ $t->id }}"/>
    <div class="form-group row">
        <label class="col-sm-3 form-control-label">Jadwal</label>
        <div class="col-sm-9">
            <div class="form-control-static">
                <input type="text" name="name" class="form-control" value="{{ $t->name }}" required autocomplete="off">
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 form-control-label">Dari Jam</label>
        <div class="col-sm-9">
            <div class="form-control-static">
                <input type="text" name="start" class="form-control clockpicker" data-autoclose="true" value="{{ date('H:i', strtotime($t->start)) }}" required autocomplete="off">
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 form-control-label">Sampai Jam</label>
        <div class="col-sm-9">
            <div class="form-control-static">
                <input type="text" name="end" class="form-control clockpicker" value="{{ date('H:i', strtotime($t->end)) }}" data-autoclose="true" required autocomplete="off">
            </div>
        </div>
    </div>
    <div class="form-group text-center">
        <button type="submit" class="btn btn-success">UPDATE</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">BATAL</button>
    </div>
</form>
<script>
    $('.clockpicker').clockpicker();
</script>