<form method="POST" action="{{ url('timing/save-data') }}">
    @csrf
    <div class="form-group row">
        <label class="col-sm-3 form-control-label">Jadwal</label>
        <div class="col-sm-9">
            <div class="form-control-static">
                <input type="text" name="name" class="form-control" placeholder="Jadwal" required autocomplete="off">
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 form-control-label">Dari Jam</label>
        <div class="col-sm-9">
            <div class="form-control-static">
                <input type="text" name="start" class="form-control clockpicker" data-autoclose="true" placeholder="Dari Jam" required autocomplete="off">
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 form-control-label">Sampai Jam</label>
        <div class="col-sm-9">
            <div class="form-control-static">
                <input type="text" name="end" class="form-control clockpicker" placeholder="Sampai Jam" data-autoclose="true" required autocomplete="off">
            </div>
        </div>
    </div>
    <div class="form-group text-center">
        <button type="submit" class="btn btn-success">SIMPAN</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">BATAL</button>
    </div>
</form>
<script>
    $('.clockpicker').clockpicker();
</script>