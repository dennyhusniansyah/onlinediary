<table>
    <thead>
        <tr align="center">
            <th align="center">No</th>
            <th align="center">Respondent</th>
            <th align="center">Email</th>
            <th align="center">Phone</th>
            <th align="center">Birthday</th>
            <th align="center">Gender</th>
            <th align="center">Marital</th>
            <th align="center">Status</th>
            <th align="center">Timing</th>
            <th align="center">Activity</th>
            @foreach ($quest as $q)
                <th>{{ $q->question }}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @php($no = 1)
        @foreach ($resume as $r)
            <tr>
                <td align="center">{{ $no++ }}</td>
                <td>{{ $r['name'] }}</td>
                <td align="center">{{ $r['email'] }}</td>
                <td align="center">{{ $r['phone'] }}</td>
                <td align="center">{{ date('d M Y', strtotime($r['birthday'])) }}</td>
                <td align="center">{{ $r['gender'] }}</td>
                <td align="center">{{ $r['marital'] }}</td>
                <td align="center">{{ $r['status'] }}</td>
                <td align="center">{{ $r['timing'] }}</td>
                <td>{{ $r['activity'] }}</td>
                @foreach ($quest as $q)
                    <td class="text-center pb_0 pt_0">
                        {!! $r['qna']->where('questions_id', $q->id)->first()->answer ?? '' !!}
                    </td>
                @endforeach
            </tr>
        @endforeach
    </tbody>
</table>