<!DOCTYPE html>
<html>
    @include('include/head')
    <body class="with-side-menu control-panel control-panel-compact chrome-browser">
        @include('include/header')
        @include('include/sidebar')
	    <div class="page-content">
            <form id="logout-form" action="{{ route('logout') }}" method="POST">
                @csrf
            </form>
            @yield('content')
        </div>
    </body>
    @include('include/scripts')
	@include('include.modal')
    @stack('script')
</html>