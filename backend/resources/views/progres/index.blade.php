@extends('master')

@push('style')
    <style>
        td{
            white-space: nowrap;
        }
    </style>
@endpush

@section('content')
    <header class="section-header">
        <div class="tbl">
            <div class="tbl-row">
                <div class="tbl-cell">
                    <h4 class="pull-left">Progres</h4>
                </div>
            </div>
        </div>
    </header>
    <section class="card">
        <div class="card-block">
            <div class="row">
                <table class="display table table-striped table-bordered dataTable" width="100%">
                    <thead>
                        <tr class="text-center">
                            <th width="15px">No</th>
                            <th>Responden</th>
                            <th>Email</th>
                            <th>Progres</th>
                            <th>Days</th>
                            <th>Project</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($progres as $p)
                            @php($progres = ($p['progres'] > 0) ? $p['progres'] / $p['days'] * 100 : 0)
                            <tr>
                                <td class="text-center">{{ $no++ }}</td>
                                <td>{{ $p['name'] }}</td>
                                <td>{{ $p['email'] }}</td>
                                <td width="250px">
                                    <div class="progress progress-lg">
                                        <div class="progress-bar progress-success" role="progressbar" style="width: {{ $progres }}%" aria-valuenow="{{ $progres }}" aria-valuemin="0" aria-valuemax="100">{{ $p['progres'] }}</div>
                                    </div>
                                </td>
                                <td class="text-center">{{ $p['days'] }}</td>
                                <td>{{ $p['project'] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection

@push('script')
    <script src="{{ asset('assets/js/lib/datatables-net/datatables.min.js') }}"></script>
	<script>
        $('.dataTable').DataTable({
            scrollX: true
        });
    </script>
@endpush