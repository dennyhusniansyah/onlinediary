@extends('master')

@section('content')
    <header class="section-header">
        <div class="tbl">
            <div class="tbl-row">
                <div class="tbl-cell">
                    <h4 class="pull-left">Produk</h4>
                        <button onclick="showAjaxModal('Tambah Produk', '{{ url('product/add-data') }}')" class="btn btn-primary btn-labeled btn-header pull-right"><span class="btn-label"><i class="fa fa-plus"></i></span>TAMBAH</button>
                </div>
            </div>
        </div>
    </header>
    <section class="card">
        <div class="card-block">
            <div class="row">
                <table class="display table table-striped table-bordered dataTable">
                    <thead>
                        <tr class="text-center">
                            <th width="15px">No</th>
                            <th>Aksi</th>
                            <th>Produk</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($product as $p)
                            <tr>
                                <td class="text-center">{{ $no++ }}</td>
                                <td class="text-center" width="80px">
                                    <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                                        <button class="btn btn-sm btn-success" onclick="showAjaxModal('Edit Produk', '{{ url('product/edit-data/'.$p->id) }}')"><i class="fa fa-pencil-alt"></i></button>
                                        <button class="btn btn-sm btn-danger" onclick="confirmModal('{{ url('product/hapus-data/'.$p->id) }}')"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                                <td>{{ $p->name }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection

@push('script')
    <script src="{{ asset('assets/js/lib/datatables-net/datatables.min.js') }}"></script>
	<script>
        $('.dataTable').DataTable();
    </script>
@endpush