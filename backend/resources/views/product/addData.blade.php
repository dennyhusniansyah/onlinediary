<form method="POST" action="{{ url('product/save-data') }}">
    @csrf
    <div class="form-group row">
        <label class="col-sm-3 form-control-label">Produk</label>
        <div class="col-sm-9">
            <div class="form-control-static">
                <input type="text" name="name" class="form-control" placeholder="Nama Produk" required autocomplete="off">
            </div>
        </div>
    </div>
    <div class="form-group text-center">
        <button type="submit" class="btn btn-primary">SIMPAN</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">BATAL</button>
    </div>
</form>