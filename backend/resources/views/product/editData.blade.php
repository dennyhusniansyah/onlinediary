<form method="POST" action="{{ url('product/update-data') }}">
    @csrf
    <input type="hidden" name="id" value="{{ $p->id }}"/>
    <div class="form-group row">
        <label class="col-sm-3 form-control-label">Produk</label>
        <div class="col-sm-9">
            <div class="form-control-static">
                <input type="text" name="name" class="form-control" value="{{ $p->name }}" required autocomplete="off">
            </div>
        </div>
    </div>
    <div class="form-group text-center">
        <button type="submit" class="btn btn-success">UPDATE</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">BATAL</button>
    </div>
</form>