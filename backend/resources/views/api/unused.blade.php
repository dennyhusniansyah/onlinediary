@extends('webview')

@push('style')
    <style>
        .checkbox-bird.purple{
            background-color: unset !important;
        }
        .mt_20{
            margin-top: 20px !important;
        }
    </style>
@endpush

@section('content')
    <section class="card mt_20">
        <div class="card-block">
            <div class="row">
                <div class="col-12">
                    @if($activity->status == 'pending')
                        <form action="{{ url('api/responder/save-qna') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id" value="{{ $activity->id }}"/>
                            <div class="form-group row">
                                <label class="col-sm-8 form-control-label">Interaksi dengan Mobil ?</label>
                                <div class="col-sm-4">
                                    <select name="used" class="form-control select2">
                                        <option value="no" {!! ($activity->used == 'no') ? 'selected' : '' !!}>Tidak</option>
                                        <option value="yes" {!! ($activity->used == 'yes') ? 'selected' : '' !!}>Ya</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-8 form-control-label">Jenis Aktivitas</label>
                                <div class="col-sm-4">
                                    @if($check == false)
                                        <select class="form-control select2" name="jenis" required onchange="jenisAktivitas()">
                                            <option value="" selected disabled>Pilih Jenis Aktivitas</option>
                                            @foreach ($question as $q)
                                                <option value="{{ $q->topic }}">{{ $q->topic }}</option>
                                            @endforeach
                                        </select>
                                    @else
                                        <input type="text" class="form-control" value="{{ $question[0]['topic'] }}" readonly/>
                                    @endif
                                </div>
                            </div>
                            @if($check == false)
                                <div id="question"></div>
                            @else
                                @foreach ($question as $q)
                                    <div class="form-group row">
                                        <label class="col-sm-8 form-control-label">{{ $q->pertanyaan }}</label>
                                        <div class="col-sm-4">
                                            <input name="qna[{{ $q->id }},{{ ($q->type == 'file') ? 'photo' : 'answer' }}]" type="{{ $q->type }}" class="form-control" value="{{ $q->answer }}" {!! ($q->required == 'yes' && $q->type == 'text') ? 'required' : '' !!}/>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-closer btn-labeled" id="btnFetch"><span class="btn-label"><i class="fa fa-save"></i></span>SIMPAN</button>
                            </div>
                        </form>
                    @else
                        <div class="form-group">
                            <label class="form-control-label">Jenis Aktivitas</label>
                            <input type="text" class="form-control" value="{{ $question[0]['topic'] }}" readonly/>
                        </div>
                        @foreach ($question as $q)
                            <div class="form-group">
                                <label class="form-control-label">{{ $q->question }}</label>
                                @if ($q->type == 'text')
                                    <input type="text" class="form-control" value="{{ $q->answer }}" readonly/>
                                @else
                                    <img src="{{ url('upload/activity/'.$q->photo) }}" width="100%"/>
                                @endif
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection

@push('script')
    <script src="{{ asset('assets/js/lib/select2/select2.full.min.js') }}"></script>
    //<script src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
    <script>
        function jenisAktivitas(){
            var j   = $('select[name="jenis"]').val();
            $.get("{{ url('api/responder/jenis-aktivitas') }}/"+j, function(resp){
                $('#question').html('');
                for(var i = 0; i < resp.length; i++){
                    var required= (resp[i].required == 'yes') ? 'required' : '',
                        name    = (resp[i].type == 'file') ? 'photo' : 'answer';
                    $('#question').append('<div class="form-group row"><label class="col-sm-8 form-control-label">'+resp[i].pertanyaan+'</label><div class="col-sm-4"><input type="'+resp[i].type+'" name="qna['+resp[i].id+','+name+']" class="form-control" '+required+'/></div></div>');
                }
            })
        }
        $('.select2').select2();
    </script>
@endpush