@extends('webview')

@push('style')
    <style>
        .checkbox-bird.purple{
            background-color: unset !important;
        }
        .mt_20{
            margin-top: 20px !important;
        }
    </style>
@endpush

@section('content')
    <section class="card mt_20">
        <div class="card-block">
            <div class="row">
                <div class="col-12">
                    @if($activity->status == 'pending')
                        @if ($question == '')
                            <h5 class="text-center">Anda telah menyelesaikan semua kuesionair</h5>
                            <div class="form-group text-center">
                                <a href="{{ url('api/responder/qna/'.$activity->id.'/'.$back->questions_id) }}" class="btn btn-default btn-labeled"><span class="btn-label"><i class="fa fa-chevron-left"></i></span>BACK</a>
                            </div>
                        @else
                            <form action="{{ url('api/responder/save-qna') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{ $activity->id }}"/>
                                <div class="form-group row">
                                    <label class="col-sm-8 form-control-label">Interaksi dengan Mobil ?</label>
                                    <div class="col-sm-4">
                                        <select name="used" class="form-control select2">
                                            <option value="no" {!! ($activity->used == 'no') ? 'selected' : '' !!}>Tidak</option>
                                            <option value="yes" {!! ($activity->used == 'yes') ? 'selected' : '' !!}>Ya</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-8 form-control-label">{{ $question->pertanyaan }}</label>
                                    <div class="col-sm-4">
                                        @if ($question->answer_type == 'single')
                                            @if (count($answer) > 0)
                                                <select class="form-control select2" name="qna[{{ $question->id }},optional]" {!! ($question->required == 'yes') ? 'required' : '' !!}>
                                                    <option value="" selected disabled>Pilihan Jawaban</option>
                                                    @foreach ($answer as $a)
                                                        <option value="{{ $a->id }}" {!! ($qna != '' && $a->id == $qna->answers_id) ? 'selected' : '' !!}>{{ $a->jawaban }}</option>
                                                    @endforeach
                                                </select>
                                            @else
                                                <input type="{{ $question->type }}" class="form-control" value="{{ ($qna != '') ? $qna->answer : '' }}" name="qna[{{ $question->id }},{{ ($question->type == 'text') ? 'answer' : 'photo' }}]"  {!! ($question->required == 'yes') ? 'required' : '' !!}/>
                                            @endif
                                        @else
                                            <div class="row">
                                                @foreach ($answer as $a)
                                                <div class="col-6">
                                                    <div class="checkbox-bird primary">
                                                        <input type="checkbox" id="{{ $a->id }}" name="qna[{{ $question->id }},check,{{ $a->id }}]" {!! ($qna != '' && in_array($a->id, explode(',', $qna->answers_id))) ? 'checked' : '' !!}>
                                                        <label for="{{ $a->id }}">{{ $a->jawaban }}</label>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group text-center">
                                    @if ($back != '')
                                        <a href="{{ url('api/responder/qna/'.$activity->id.'/'.$back->questions_id) }}" class="btn btn-default btn-labeled"><span class="btn-label"><i class="fa fa-chevron-left"></i></span>BACK</a>
                                    @endif
                                    <button type="submit" class="btn btn-closer btn-labeled" id="btnFetch">NEXT<span class="btn-label right"><i class="fa fa-chevron-right"></i></span></button>
                                </div>
                            </form>
                        @endif
                    @else
                        @foreach ($question as $k => $q)
                            <div class="form-group">
                                <label class="form-control-label">{{ $q['question'] }}</label>
                                <input type="text" class="form-control" value="{{ ($q['qna'] != '') ? $q['qna']->answer : '' }}" readonly/>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection

@push('script')
    <script src="{{ asset('assets/js/lib/select2/select2.full.min.js') }}"></script>
    <script>
        /*
        $(document).ready(function() {
            $("#btnFetch").click(function() {
                $(this).prop("disabled", true);
                $(this).html('LOADING<span class="btn-label right"><span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span></span>');
            });
        });*/
        $('.select2').select2();
    </script>
@endpush