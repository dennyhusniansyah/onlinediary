@extends('webview')

@push('style')
    <style>
        .checkbox-bird.purple{
            background-color: unset !important;
        }
        .mt_20{
            margin-top: 20px !important;
        }
    </style>
@endpush

@section('content')
    <section class="card mt_20">
        <div class="card-block">
            <div class="row">
                <div class="col-12">
                    @if($activity->status == 'pending')
                        @if ($quest == '')
                            <h5 class="text-center">Anda telah menyelesaikan semua kuesioner</h5>
                            <div class="form-group text-center">
                                <a href="{{ url('api/responder/qna/'.$activity->id.'/'.$back->id) }}" class="btn btn-default btn-labeled"><span class="btn-label"><i class="fa fa-chevron-left"></i></span>BACK</a>
                            </div>
                        @else
                            <form action="{{ url('api/responder/save-qna') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{ $activity->id }}"/>
                                <div class="form-group row">
                                    <label class="col-sm-8 form-control-label">{{ $quest->question }}</label>
                                    <div class="col-sm-4">
                                        @if ($quest->answer_type == 'single')
                                            @if (count($answer) > 0)
                                                <select class="form-control select2" name="qna[{{ $quest->id }},optional]" {!! ($quest->required == 'Y') ? 'required' : '' !!}>
                                                    <option value="" selected disabled>Pilihan Jawaban</option>
                                                    @foreach ($answer as $a)
                                                        <option value="{{ $a->id }}" {!! ($qna != '' && $a->id == $qna->answers_id) ? 'selected' : '' !!}>{{ $a->jawaban }}</option>
                                                    @endforeach
                                                </select>
                                            @else
                                                <input type="{{ $quest->type }}" class="form-control" value="{{ ($qna != '') ? $qna->answer : '' }}" name="qna[{{ $quest->id }},{{ ($quest->type == 'file') ? 'photo' : 'answer' }}]"  {!! ($quest->required == 'Y') ? 'required' : '' !!}/>
                                            @endif
                                        @else
                                            <div class="row">
                                                @foreach ($answer as $a)
                                                <div class="col-12">
                                                    <div class="checkbox-bird primary">
                                                        <input type="checkbox" id="{{ $a->id }}" name="qna[{{ $quest->id }},check,{{ $a->id }}]" {!! ($qna != '' && in_array($a->id, explode(',', $qna->answers_id))) ? 'checked' : '' !!}>
                                                        <label for="{{ $a->id }}">{{ $a->jawaban }}</label>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                            {{-- <select class="form-control select2" multiple name="qna[]" required>
                                                @foreach ($answer as $a)
                                                    <option value="{{ $quest->id }},check,{{ $a->id }}" {!! ($qna != '' && in_array($a->id, explode(',', $qna->answers_id))) ? 'selected' : '' !!}>{{ $a->jawaban }}</option>
                                                @endforeach
                                            </select> --}}
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group text-center">
                                    @if ($back != '')
                                        <a href="{{ url('api/responder/qna/'.$activity->id.'/'.$back->id) }}" class="btn btn-default btn-labeled"><span class="btn-label"><i class="fa fa-chevron-left"></i></span>BACK</a>
                                    @endif
                                    <button type="submit" class="btn btn-closer btn-labeled" id="btnFetch">NEXT<span class="btn-label right"><i class="fa fa-chevron-right"></i></span></button>
                                </div>
                            </form>
                        @endif
                    @else
                        @foreach ($quest as $k => $q)
                            <div class="form-group">
                                <label class="form-control-label">{{ $q['question'] }}</label>
                                <input type="text" class="form-control" value="{{ ($q['qna'] != '') ? $q['qna']->answer : '' }}" readonly/>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection

@push('script')
    <script src="{{ asset('assets/js/lib/select2/select2.full.min.js') }}"></script>
    <script>
        $('.select2').select2();
    </script>
@endpush