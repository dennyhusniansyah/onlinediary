@extends('webview')

@push('style')
    <style>
        .checkbox-bird.purple{
            background-color: unset !important;
        }
        .mt_20{
            margin-top: 20px !important;
        }
    </style>
@endpush

@section('content')
    <section class="card mt_20">
        <div class="card-block">
            <div class="row">
                <div class="col-12">
                    @if($activity->status == 'pending')
                        <form action="{{ url('api/responder/save-qna') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id" value="{{ $activity->id }}"/>
                            @foreach ($question as $q)
                                <div class="form-group row">
                                    <label class="col-sm-8 form-control-label">{{ $q['question'] }}</label>
                                    <div class="col-sm-4">
                                        @if($q['answer_type'] == 'single')
                                            @if (count($q['answer']) > 0)
                                                <select class="form-control select2" name="qna[{{ $q['id'] }},optional]" {!! ($q['required'] == 'yes') ? 'required' : '' !!}>
                                                    <option value="" selected disabled>Pilihan Jawaban</option>
                                                    @foreach ($q['answer'] as $a)
                                                        <option value="{{ $a->id }}" {!! ($q['qna'] != '' && $a->id == $q['qna']->answers_id) ? 'selected' : '' !!}>{{ $a->jawaban }}</option>
                                                    @endforeach
                                                </select>
                                            @else
                                                <input type="{{ $q['type'] }}" class="form-control" value="{{ ($q['qna'] != '') ? $q['qna']->answer : '' }}" name="qna[{{ $q['id'] }},{{ ($q['type'] == 'text') ? 'answer' : 'photo' }}]"  {!! ($q['required'] == 'yes') ? 'required' : '' !!}/>
                                            @endif
                                        @else
                                            <div class="row">
                                                @foreach ($q['answer'] as $a)
                                                <div class="col-6">
                                                    <div class="checkbox-bird primary">
                                                        <input type="checkbox" id="{{ $a->id }}" name="qna[{{ $q['id'] }},check,{{ $a->id }}]" {!! ($q['qna'] != '' && in_array($a->id, explode(',', $q['qna']->answers_id))) ? 'checked' : '' !!}>
                                                        <label for="{{ $a->id }}">{{ $a->jawaban }}</label>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-primary">SIMPAN</button>
                            </div>
                        </form>
                    @else
                        @foreach ($question as $k => $q)
                            <div class="form-group">
                                <label class="form-control-label">{{ $q['question'] }}</label>
                                <input type="text" class="form-control" value="{{ ($q['qna'] != '') ? $q['qna']->answer : '' }}" readonly/>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection

@push('script')
    <script src="{{ asset('assets/js/lib/select2/select2.full.min.js') }}"></script>
    <script>
        function ikiMaster(a){
            console.log(a);
        }
        $('.select2').select2();
    </script>
@endpush