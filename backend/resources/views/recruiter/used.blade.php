@extends('webview')

@push('style')
    <style>
        .checkbox-bird.purple{
            background-color: unset !important;
        }
        .mt_20{
            margin-top: 20px !important;
        }
    </style>
@endpush

@section('content')
    @if (count($qna) > 0)
        <section class="card mt_20">
            <div class="card-block">
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12">
                                @foreach ($qna as $q)
                                    <div class="form-group">
                                        <label class="form-control-label">{{ $q->question }}</label>
                                        @if ($q->type == 'text')
                                            <input type="text" class="form-control" value="{{ $q->answer }}" readonly/>
                                        @else
                                            <img src="{{ url('upload/activity/'.$q->photo) }}" width="100%"/>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif
@endsection