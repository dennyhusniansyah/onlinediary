@extends('master')

@section('content')
    <header class="section-header">
        <div class="tbl">
            <div class="tbl-row">
                <div class="tbl-cell">
                    <h4 class="pull-left">Bot Comments</h4>
                </div>
            </div>
        </div>
    </header>
    <div class="row">
        <div class="col-lg-4">
            <section class="widget widget-activity" style="width:100%">
                <header class="widget-header">Add Bot Comment</header>
                <div class="box-typical-body panel-body">
                    <div class="col-12">
                        <form method="POST" action="{{ url('comments/save-data') }}">
                            @csrf
                            {{--  <div class="form-group">
                                <label class="form-control-label">Produk</label>
                                <select name="product" class="form-control select2">
                                    @foreach ($products as $p)
                                        <option value="{{ $p->id }}">{{ $p->name }}</option>
                                    @endforeach
                                </select>
                            </div>  --}}
                            <div class="form-group">
                                <label class="form-control-label">Comment</label>
                                <textarea name="bot" class="form-control" required></textarea>
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-primary">SIMPAN</button>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-sm-8">
            <section class="widget widget-activity" style="width:100%">
                <header class="widget-header">Bot Comments</header>
                <br/>
                <div class="box-typical-body panel-body">
                    <table class="table table-striped table-bordered dataTable" width="100%">
                        <thead>
                            <tr class="text-center">
                                <th width="25px">No</th>
                                <th>Produk</th>
                                <th>Bot</th>
                                <th width="15px">Del</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($bots as $b)
                                <tr>
                                    <td class="text-center">{{ $no++ }}</td>
                                    <td>{{ ucfirst($b->name) }}</td>
                                    <td>{{ $b->post }}</td>
                                    <td class="text-center">
                                        <button class="btn btn-sm btn-danger" onclick="confirmModal('{{ url('comments/delete-bot/'.$b->id) }}')"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('assets/js/lib/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/datatables-net/datatables.min.js') }}"></script>
    <script>
        $('.select2').select2();
        $('.dataTable').DataTable();
    </script>
@endpush