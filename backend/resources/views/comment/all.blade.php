@extends('master')

@push('style')
    <link rel="stylesheet" href="{{ asset('assets/css/separate/pages/chat.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/separate/pages/messenger.min.css') }}">
    <style>
        #photo img{
            width: 100%;
        }
        .chat-settings{
            color: #606060;
        }
        .chat-list{
            max-height: calc(100vh - 130px);
            overflow: scroll;
            margin: 10px 0;
        }
        .message div span{
            width: 100%;
            display: block;
            font-weight: 600;
            color: #ff4700;
            font-size: 12px;
        }
        button.submit{
            float: right;
            position: absolute;
            right: 0;
            top: 0;
            outline: none !important;
            border: none;
            color: #DC143C;
            padding: 24px 15px;
        }
    </style>
@endpush

@section('content')
    <div class="container-fluid messenger">
        <div class="box-typical chat-container">
            <section class="chat-list">
                <div class="chat-list-in scrollable-block">
                    @foreach ($lists as $l)
                        <a href="{{ url('comments/activity/'.$l['act_id']) }}" class="chat-list-item{!! ($act_id == $l['act_id']) ? ' selected' : '' !!}">
                            <div class="chat-list-item-photo">
                                <img src="{{ asset('upload/profile/'.$l['photo']) }}">
                            </div>
                            <div class="chat-list-item-header">
                                <div class="chat-list-item-name">
                                    <span class="name">{{ $l['act'] }}</span>
                                </div>
                                <div class="chat-list-item-date">{{ ($l['time'] != '') ? $l['time']->format('H:i') : '' }}</div>
                            </div>
                            <div class="chat-list-item-cont">
                                <div class="chat-list-item-txt">{{ ($l['last'] != '') ? $l['last'] : '...' }}</div>
                                @if ($l['unread'] > 0)
                                    <div class="chat-list-item-count">{{ $l['unread'] }}</div>
                                @endif
                            </div>
                        </a>
                    @endforeach
                </div>
            </section>

            <section class="chat-list-info">
                <div class="chat-list-in">
                    <section class="chat-user-info" id="photo"></section>
                    @if($activity != '')
                        <section class="chat-settings">
                            <div class="item"><img src="{{ url('upload/profile/'.$activity->profile) }}" width="100%"></div>
                        </section>
                        <section class="chat-settings">
                            <div class="item"><a href="#" class="maroon"><span class="font-icon font-icon-quote"></span>{{ $activity->name }}</a></div>
                            <div class="item"><a href="#" class="maroon"><span class="font-icon font-icon-user"></span>{{ $activity->user }}</a></div>
                            <div class="item"><a href="#" class="maroon"><span class="font-icon font-icon-mail"></span>{{ $activity->email }}</a></div>
                            <div class="item"><a href="#" class="maroon"><span class="font-icon font-icon-phone"></span>{{ $activity->phone }}</a></div>
                            <div class="item"><a href="#" class="maroon"><span class="font-icon font-icon-calend"></span>{{ $activity->created_at->format('d M Y H:i:s') }} ({{ $activity->timing }})</a></div>
                        </section>
                    @endif
                </div>
            </section>

            <section class="chat-area">
                <div class="chat-area-in">
                    <div class="chat-dialog-area scrollable-block">
                        <div class="messenger-dialog-area" id="roomChat">
                            @foreach ($comments as $c)
                                {!! getChat($c->name, $c->post, $c->created_at, $c->users_id) !!}
                            @endforeach
                        </div>
                    </div>
                    <div class="chat-area-bottom">
                        @if($activity != '')
                            <form class="write-message" action="{{ url('comments/send-comment') }}" method="POST">
                                <div class="form-group">
                                    @csrf
                                    <input type="hidden" name="id" value="{{ $activity->id }}"/>
                                    <textarea name="comment" rows="1" class="form-control" placeholder="Type a message" {!! (Auth::user()->role != 'pm') ? 'readonly' : '' !!} required></textarea>
                                    <button type="submit" class="submit"><i class="fa fa-paper-plane"></i></button>
                                </div>
                            </form>
                        @endif
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $(window).ready(function() {
            $(".chat-dialog-area .jspPane").animate({ top: $('.chat-area .jspContainer').height() - $('#roomChat').height() - 30 }, 1000);
        });
    </script>
@endpush