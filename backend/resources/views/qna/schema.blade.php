<div class="col-12">
    <div class="dd dd-maroon" id="nestable">
        <ol class="dd-list">
            @foreach ($quest as $q)
                <li class="dd-item" data-id="{{ $q->id }}">
                    <div class="dd-handle">{{ $q->question }}</div>
                    @php($isParent = isParent($q->id))
                    @if($isParent == true)
                        @php($sub = subQuestion($q->id))
                        @foreach ($sub as $s)
                            <ol class="dd-list">
                                <li class="dd-item" data-id="{{ $s->id }}">
                                    <div class="dd-handle">
                                            <a href="{{ url('/') }}" target="_blank">Oke</a>
                                        {{ $s->question }}<span class="label label-danger pull-right">{{ $s->jawaban }}</span>
                                    </div>
                                    @php($isParent = isParent($s->id))
                                    @if($isParent == true)
                                        @php($subSub = subQuestion($s->id))
                                        @foreach ($subSub as $ss)
                                            <ol class="dd-list">
                                                <li class="dd-item" data-id="{{ $ss->id }}">
                                                    <div class="dd-handle">{{ $ss->question }}<span class="label label-danger pull-right">{{ $ss->jawaban }}</span></div>
                                                </li>
                                            </ol>
                                        @endforeach
                                    @endif
                                </li>
                            </ol>
                        @endforeach
                    @endif
                </li>
            @endforeach
        </ol>
    </div>
</div>
<script src="{{ asset('assets/js/lib/nestable/jquery.nestable.js') }}"></script>
<script>
    $('#nestable').nestable({
        group: 1,
        maxDepth: 1,2,3
    });
</script>