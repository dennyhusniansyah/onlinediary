<section class="widget widget-activity" id="sort">
    <div class="box-typical-body panel-body">
        <div class="col-12">
            <form method="POST" action="{{ url('qna/sorted') }}">
                @csrf
                <div class="row">
                    <div class="col-12">
                        <div class="dd dd-maroon" id="nestable">
                            <ol class="dd-list">
                                @foreach ($quest as $q)
                                    <li class="dd-item">
                                        <div class="dd-handle">
                                            <input type="hidden" name="quest[]" value="{{ $q['id'] }}"/>
                                            <header class="widget-header">{{ $q['question'] }}<span class="pull-right label label-{!! ($q['status'] == 'active') ? 'success' : 'warning' !!}">{{ $q['status'] }}</span></header>
                                            @if($q['parent'] != '')
                                                <div>
                                                    <div class="widget-tasks-item">
                                                        <div class="user-card-row">
                                                            <table width="100%">
                                                                <tbody>
                                                                    <tr>
                                                                        <td width="50%">Parent Question</td>
                                                                        <td width="50%">: {{ $q['parent'] }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="50%">Parent Answer</td>
                                                                        <td width="50%">: {{ $q['answer'] }}</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </li>
                                @endforeach
                            </ol>
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-success">UPDATE</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">BATAL</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<script src="{{ asset('assets/js/lib/nestable/jquery.nestable.js') }}"></script>
<script>
    $('#nestable').nestable({
        group: 1,
        maxDepth: 1
    });
</script>