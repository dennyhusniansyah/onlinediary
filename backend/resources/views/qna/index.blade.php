@extends('master')

@push('style')
    <style>
        #home .dd-handle:hover{
            background-color: #ffffff !important;
            color: #343434 !important;
        }
        .widget-tasks-item{
            color: #343434 !important;
        }
        .widget-header{
            overflow: scroll;
        }
        .chart-legend-list li{
            white-space: nowrap;
            margin-right: 10px;
            display: inline-block;
            padding: 3px 6px;
            border: 1px solid rgb(220 25 60);
            border-radius: 5px;
        }
        .pt_10{
            padding-top: 10px !important;
        }
        .pb_10{
            padding-bottom: 10px !important;
        }
        .bg_qna{
            background: #b3b3b3;
        }
    </style>
@endpush

@section('content')
    <header class="section-header">
        <div class="tbl">
            <div class="tbl-row">
                <div class="tbl-cell">
                    <h4 class="pull-left">Question & Answer</h4>
                </div>
            </div>
        </div>
    </header>
    <div class="row">
        <div class="col-12">
            <section class="widget widget-accordion mb_20" id="accordion" role="tablist" aria-multiselectable="true">
                <article class="panel">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <a class="{!! ($form == 'edit') ? '' : 'colapsed' !!}" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">New / Update Q & A <i class="font-icon font-icon-arrow-down"></i></a>
                        <hr class="m_0"/>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in {!! ($form == 'edit') ? 'show' : '' !!}" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-collapse-in row">
                            <div class="col-8 quest">
                                @if($form == 'edit')
                                    <form method="POST" action="{{ url('qna/update-question') }}">
                                @else
                                    <form method="POST" action="{{ url('qna/save-question') }}">
                                @endif
                                    @csrf
                                    @if($form == 'edit')
                                        <input type="hidden" name="id" value="{{ $question->id }}"/>
                                    @endif
                                    <div class="box-typical-body panel-body row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label class="form-control-label">Parent Question</label>
                                                <select name="parent" class="form-control select2" onchange="parentQ()">
                                                    <option value="" selected>Pilih jika menyangkut pertanyaan sebelumnya</option>
                                                    @foreach ($parent as $p)
                                                        <option value="{{ $p->id }}" {!! ($question != '' && $question->parent == $p->id) ? 'selected' : '' !!}>{{ $p->question }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label class="form-control-label">Answer Parent Question</label>
                                                <select name="answer_parent" class="form-control select2">
                                                    <option value="" selected>Pilih parent question dahulu</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label class="form-control-label">Project Code</label>
                                                @if($form == 'edit')
                                                    @foreach ($projects as $p)
                                                        @if($p->code == $code)
                                                            <input type="hidden" name="projects_id" class="form-control" value="{{ $p->id }}"/>
                                                        @endif
                                                    @endforeach
                                                    <input type="text" class="form-control" value="{{ $code }}" disabled/>
                                                @else
                                                    <select name="projects_id" class="form-control select2" required onchange="pilihProject(this)">
                                                        <option value="" selected disabled>Pilin Project</option>
                                                        @foreach ($projects as $p)
                                                            <option value="{{ $p->id }}" {!! ($p->code == $code) ? 'selected' : '' !!}>{{ $p->code }}</option>
                                                        @endforeach
                                                    </select>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label class="form-control-label">Opsional Answer</label>
                                                <select name="answer_type" class="form-control select2" required>
                                                    <option value="single" {!! ($question != '' && $question->used == 'single') ? 'selected' : '' !!}>Single</option>
                                                    <option value="multi" {!! ($question != '' && $question->used == 'multi') ? 'selected' : '' !!}>Multi</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label class="form-control-label">Answer Type</label>
                                                <select name="type" class="form-control select2" required>
                                                    <option value="text" {!! ($question != '' && $question->type == 'text') ? 'selected' : '' !!}>Text</option>
                                                    <option value="file" {!! ($question != '' && $question->type == 'file') ? 'selected' : '' !!}>File</option>
                                                    <option value="number" {!! ($question != '' && $question->type == 'number') ? 'selected' : '' !!}>Number</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label class="form-control-label">Required</label>
                                                <select name="required" class="form-control select2" required>
                                                    <option value="Y" {!! ($question != '' && $question->required == 'Y') ? 'selected' : '' !!}>Yes</option>
                                                    <option value="N" {!! ($question != '' && $question->required == 'N') ? 'selected' : '' !!}>No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label class="form-control-label">Question</label>
                                                <textarea name="question" class="form-control" required>{{ ($question != '') ? $question->question : '' }}</textarea>
                                                <small class="text-muted"><i>anda bisa memasukkan banyak pertanyaan dengan pemisah # jika opsional answer, answer type dan required sama</i></small>
                                            </div>
                                            <div class="form-group text-center">
                                                @if($form == 'edit')
                                                    <a href="{{ url('qna') }}" class="btn btn-labeled btn-success"><span class="btn-label"><i class="fa fa-plus"></i></span>NEW</a>
                                                @endif
                                                <button type="submit" class="btn btn-labeled btn-closer"><span class="btn-label"><i class="fa fa-save"></i></span>{!! ($form == 'edit') ? 'UPDATE' : 'SAVE' !!}</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-4">
                                <form method="POST" action="{{ url('qna/save-answer') }}">
                                    @csrf
                                    <div class="form-group">
                                        <label class="form-control-label">Project Code</label>
                                        <select name="projects_id" class="form-control select2" required onchange="pilihProject(this)">
                                            <option value="" selected disabled>Pilih Project</option>
                                            @foreach ($projects as $p)
                                                <option value="{{ $p->id }}" {!! ($p->code == $code) ? 'selected' : '' !!}>{{ $p->code }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">Question</label>
                                        <select name="question" class="form-control select2" id="question" required>
                                            <option value="" selected disabled>Pilih Project Terlebih Dahulu !</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">Answer</label>
                                        <textarea name="answer" class="form-control" required></textarea>
                                        <small class="text-muted"><i>anda bisa memasukkan banyak jawaban dengan pemisah #</i></small>
                                    </div>
                                    <div class="form-group text-center">
                                        <button type="submit" class="btn btn-labeled btn-closer"><span class="btn-label"><i class="fa fa-save"></i></span>SAVE</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </article>
            </section>
        </div>
        <div class="col-12">
            <section class="widget widget-activity" style="width:100%">
                <header class="widget-header">Q & A</header>
                <br/>
                <form method="GET" action="{{ url('qna/filter') }}">
                    <div class="box-typical-body panel-body">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-5">
                                    <div class="form-group row">
                                        <label class="col-3 form-control-label">Project</label>
                                        <div class="col-9">
                                            <select class="select2 form-control" name="projects_code" onchange="this.form.submit()" required>
                                                <option value="" selected disabled>Pilih Kode Project</option>
                                                @foreach ($projects as $p)
                                                    <option value="{{ $p->code }}" {!! ($p->code == $code) ? 'selected' : '' !!}>{{ $p->code }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="form-group row">
                                        <label class="col-3 form-control-label">Status</label>
                                        <div class="col-5">
                                            <select class="select2 form-control" name="status" onchange="this.form.submit()" required>
                                                <option value="semua" {!! ($status == 'semua') ? 'selected' : '' !!}>Semua</option>
                                                <option value="active" {!! ($status == 'active') ? 'selected' : '' !!}>Active</option>
                                                <option value="inactive" {!! ($status == 'inactive') ? 'selected' : '' !!}>Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                @if ($code != '')
                                    <div class="col-2">
                                        <button type="button" onclick="modalLarge('Sort Question', '{{ url('qna/sort/'.$code) }}')" class="btn btn-closer btn-labeled"><span class="btn-label"><i class="fa fa-sort-numeric-down"></i></span>SORT</button>
                                        {{--  <button type="button" onclick="modalLarge('Schema Question', '{{ url('qna/schema') }}')" class="btn btn-closer btn-labeled"><span class="btn-label"><i class="fa fa-align-right"></i></span>SCHEMA</button>  --}}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </form>
                {{-- <hr/> --}}
                <div class="box-typical-body panel-body pt_10 pb_10 bg_qna">
                    <div class="col-12">
                        <div class="dd dd-maroon" id="home">
                            <ol class="dd-list">
                                @foreach ($quest as $q)
                                    <li class="dd-item" data-id="{{ $q->id }}">
                                        <div class="dd-handle">
                                            <header class="widget-header">
                                                <div class="btn-group btn-group-sm ml_0" role="group" aria-label="Basic example">
                                                    <a href="{{ url('qna/edit/'.$q->id) }}" class="btn btn-success"><i class="fa fa-pencil-alt"></i></a>
                                                    @if($q->status == 'active')
                                                        <button class="btn btn-danger" onclick="confirmModal('{{ url('qna/delete-question/'.$q->id) }}')"><i class="fa fa-trash"></i></button>
                                                    @else
                                                        <button class="btn btn-success" onclick="optModal('Apakah anda yakin mengaktifkan pertanyaan ini?', '{{ url('qna/activated-question/'.$q->id) }}')"><i class="fa fa-check"></i></button>
                                                    @endif
                                                </div>
                                                {{ $q->question }}
                                                <div class="pull-right">
                                                    <span class="label label-{!! ($q->status == 'active') ? 'success' : 'warning' !!}">{{ $q->status }}</span>
                                                    <span class="label label-{!! ($q->answer_type == 'single') ? 'success' : 'warning' !!}">{{ $q->answer_type }}</span>
                                                </div>
                                            </header>
                                            @if($q->answers != '')
                                                <div>
                                                    <div class="widget-tasks-item">
                                                        <ul class="chart-legend-list font-16">
                                                            @php($answer_id = explode('#', $q->answer_id))
                                                            @foreach (explode('#', $q->answers) as $k => $j)
                                                                <li><span onclick="confirmModal('{{ url('qna/delete-answer/'.$answer_id[$k]) }}')"><i class="fa fa-sm fa-trash fa-danger"></i></span> {{ $j }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                        @php($isParent = isParent($q->id))
                                        @if($isParent == true)
                                            @php($sub = subQuestion($q->id))
                                            @foreach ($sub as $s)
                                                <ol class="dd-list">
                                                    <li class="dd-item" data-id="{{ $s->id }}">
                                                        <div class="dd-handle">
                                                            <header class="widget-header">
                                                                <div class="btn-group btn-group-sm ml_0" role="group" aria-label="Basic example">
                                                                    <a href="{{ url('qna/edit/'.$s->id) }}" class="btn btn-success"><i class="fa fa-pencil-alt"></i></a>
                                                                    @if($s->status == 'active')
                                                                        <button class="btn btn-danger" onclick="confirmModal('{{ url('qna/delete-question/'.$s->id) }}')"><i class="fa fa-trash"></i></button>
                                                                    @else
                                                                        <button class="btn btn-success" onclick="optModal('Apakah anda yakin mengaktifkan pertanyaan ini?', '{{ url('qna/activated-question/'.$s->id) }}')"><i class="fa fa-check"></i></button>
                                                                    @endif
                                                                </div>
                                                                {{ $s->question }}
                                                                <div class="pull-right">
                                                                    <span class="label label-{!! ($s->status == 'active') ? 'success' : 'warning' !!}">{{ $s->status }}</span>
                                                                    <span class="label label-{!! ($s->answer_type == 'single') ? 'success' : 'warning' !!}">{{ $s->answer_type }}</span>
                                                                    <span class="label label-danger pull-right">{{ $s->jawaban }}</span>
                                                                </div>
                                                            </header>
                                                            @if($s->answers != '')
                                                                <div>
                                                                    <div class="widget-tasks-item">
                                                                        <ul class="chart-legend-list font-16">
                                                                            @php($answer_id = explode('#', $s->answer_id))
                                                                            @foreach (explode('#', $s->answers) as $k => $j)
                                                                                <li><span onclick="confirmModal('{{ url('qna/delete-answer/'.$answer_id[$k]) }}')"><i class="fa fa-sm fa-trash fa-danger"></i></span> {{ $j }}</li>
                                                                            @endforeach
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        </div>
                                                        @php($isParent = isParent($s->id))
                                                        @if($isParent == true)
                                                            @php($subSub = subQuestion($s->id))
                                                            @foreach ($subSub as $ss)
                                                                <ol class="dd-list">
                                                                    <li class="dd-item" data-id="{{ $ss->id }}">
                                                                        <div class="dd-handle">
                                                                            <header class="widget-header">
                                                                                <div class="btn-group btn-group-sm ml_0" role="group" aria-label="Basic example">
                                                                                    <a href="{{ url('qna/edit/'.$ss->id) }}" class="btn btn-success"><i class="fa fa-pencil-alt"></i></a>
                                                                                    @if($ss->status == 'active')
                                                                                        <button class="btn btn-danger" onclick="confirmModal('{{ url('qna/delete-question/'.$ss->id) }}')"><i class="fa fa-trash"></i></button>
                                                                                    @else
                                                                                        <button class="btn btn-success" onclick="optModal('Apakah anda yakin mengaktifkan pertanyaan ini?', '{{ url('qna/activated-question/'.$ss->id) }}')"><i class="fa fa-check"></i></button>
                                                                                    @endif
                                                                                </div>
                                                                                {{ $ss->question }}
                                                                                <div class="pull-right">
                                                                                    <span class="label label-{!! ($ss->status == 'active') ? 'success' : 'warning' !!}">{{ $ss->status }}</span>
                                                                                    <span class="label label-{!! ($ss->answer_type == 'single') ? 'success' : 'warning' !!}">{{ $ss->answer_type }}</span>
                                                                                    <span class="label label-danger pull-right">{{ $ss->jawaban }}</span>
                                                                                </div>
                                                                            </header>
                                                                            @if($ss->answers != '')
                                                                                <div>
                                                                                    <div class="widget-tasks-item">
                                                                                        <ul class="chart-legend-list font-16">
                                                                                            @php($answer_id = explode('#', $ss->answer_id))
                                                                                            @foreach (explode('#', $ss->answers) as $k => $j)
                                                                                                <li><span onclick="confirmModal('{{ url('qna/delete-answer/'.$answer_id[$k]) }}')"><i class="fa fa-sm fa-trash fa-danger"></i></span> {{ $j }}</li>
                                                                                            @endforeach
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            @endif
                                                                        </div>
                                                                        @php($isParent = isParent($ss->id))
                                                                        @if($isParent == true)
                                                                            @php($subSubSub = subQuestion($ss->id))
                                                                            @foreach ($subSubSub as $sss)
                                                                                <ol class="dd-list">
                                                                                    <li class="dd-item" data-id="{{ $sss->id }}">
                                                                                        <div class="dd-handle">
                                                                                            <header class="widget-header">
                                                                                                <div class="btn-group btn-group-sm ml_0" role="group" aria-label="Basic example">
                                                                                                    <a href="{{ url('qna/edit/'.$sss->id) }}" class="btn btn-success"><i class="fa fa-pencil-alt"></i></a>
                                                                                                    @if($sss->status == 'active')
                                                                                                        <button class="btn btn-danger" onclick="confirmModal('{{ url('qna/delete-question/'.$sss->id) }}')"><i class="fa fa-trash"></i></button>
                                                                                                    @else
                                                                                                        <button class="btn btn-success" onclick="optModal('Apakah anda yakin mengaktifkan pertanyaan ini?', '{{ url('qna/activated-question/'.$sss->id) }}')"><i class="fa fa-check"></i></button>
                                                                                                    @endif
                                                                                                </div>
                                                                                                {{ $sss->question }}
                                                                                                <div class="pull-right">
                                                                                                    <span class="label label-{!! ($sss->status == 'active') ? 'success' : 'warning' !!}">{{ $sss->status }}</span>
                                                                                                    <span class="label label-{!! ($sss->answer_type == 'single') ? 'success' : 'warning' !!}">{{ $sss->answer_type }}</span>
                                                                                                    <span class="label label-danger pull-right">{{ $sss->jawaban }}</span>
                                                                                                </div>
                                                                                            </header>
                                                                                            @if($sss->answers != '')
                                                                                                <div>
                                                                                                    <div class="widget-tasks-item">
                                                                                                        <ul class="chart-legend-list font-16">
                                                                                                            @php($answer_id = explode('#', $sss->answer_id))
                                                                                                            @foreach (explode('#', $sss->answers) as $k => $j)
                                                                                                                <li><span onclick="confirmModal('{{ url('qna/delete-answer/'.$answer_id[$k]) }}')"><i class="fa fa-sm fa-trash fa-danger"></i></span> {{ $j }}</li>
                                                                                                            @endforeach
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                </div>
                                                                                            @endif
                                                                                        </div>
                                                                                        @php($isParent = isParent($sss->id))
                                                                                        @if($isParent == true)
                                                                                            @php($subSubSubSub = subQuestion($sss->id))
                                                                                            @foreach ($subSubSubSub as $ssss)
                                                                                                <ol class="dd-list">
                                                                                                    <li class="dd-item" data-id="{{ $ssss->id }}">
                                                                                                        <div class="dd-handle">
                                                                                                            <header class="widget-header">
                                                                                                                <div class="btn-group btn-group-sm ml_0" role="group" aria-label="Basic example">
                                                                                                                    <a href="{{ url('qna/edit/'.$ssss->id) }}" class="btn btn-success"><i class="fa fa-pencil-alt"></i></a>
                                                                                                                    @if($ssss->status == 'active')
                                                                                                                        <button class="btn btn-danger" onclick="confirmModal('{{ url('qna/delete-question/'.$ssss->id) }}')"><i class="fa fa-trash"></i></button>
                                                                                                                    @else
                                                                                                                        <button class="btn btn-success" onclick="optModal('Apakah anda yakin mengaktifkan pertanyaan ini?', '{{ url('qna/activated-question/'.$ssss->id) }}')"><i class="fa fa-check"></i></button>
                                                                                                                    @endif
                                                                                                                </div>
                                                                                                                {{ $ssss->question }}
                                                                                                                <div class="pull-right">
                                                                                                                    <span class="label label-{!! ($ssss->status == 'active') ? 'success' : 'warning' !!}">{{ $ssss->status }}</span>
                                                                                                                    <span class="label label-{!! ($ssss->answer_type == 'single') ? 'success' : 'warning' !!}">{{ $ssss->answer_type }}</span>
                                                                                                                    <span class="label label-danger pull-right">{{ $ssss->jawaban }}</span>
                                                                                                                </div>
                                                                                                            </header>
                                                                                                            @if($ssss->answers != '')
                                                                                                                <div>
                                                                                                                    <div class="widget-tasks-item">
                                                                                                                        <ul class="chart-legend-list font-16">
                                                                                                                            @php($answer_id = explode('#', $ssss->answer_id))
                                                                                                                            @foreach (explode('#', $ssss->answers) as $k => $j)
                                                                                                                                <li><span onclick="confirmModal('{{ url('qna/delete-answer/'.$answer_id[$k]) }}')"><i class="fa fa-sm fa-trash fa-danger"></i></span> {{ $j }}</li>
                                                                                                                            @endforeach
                                                                                                                        </ul>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            @endif
                                                                                                        </div>
                                                                                                    </li>
                                                                                                </ol>
                                                                                            @endforeach
                                                                                        @endif
                                                                                    </li>
                                                                                </ol>
                                                                            @endforeach
                                                                        @endif
                                                                    </li>
                                                                </ol>
                                                            @endforeach
                                                        @endif
                                                    </li>
                                                </ol>
                                            @endforeach
                                        @endif
                                    </li>
                                @endforeach
                            </ol>
                        </div>
                    </div>
                    {{--  <table class="table table-striped table-bordered dataTable" width="100%">
                        <thead>
                            <tr class="text-center">
                                <th width="25px">No</th>
                                <th width="15px">Aksi</th>
                                <th>Question</th>
                                <th>Optional Answer</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($qna as $q)
                                <tr>
                                    <td class="text-center">{{ $no++ }}</td>
                                    <td class="text-center">
                                        <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                                            <a href="{{ url('qna/edit/'.$q['id']) }}" class="btn btn-success"><i class="fa fa-pencil-alt"></i></a>
                                            @if($q['status'] == 'active')
                                                <button class="btn btn-danger" onclick="confirmModal('{{ url('qna/delete-question/'.$q['id']) }}')"><i class="fa fa-trash"></i></button>
                                            @else
                                                <button class="btn btn-success" onclick="optModal('Apakah anda yakin mengaktifkan pertanyaan ini?', '{{ url('qna/activated-question/'.$q['id']) }}')"><i class="fa fa-check"></i></button>
                                            @endif
                                        </div>
                                    </td>
                                    <td>{{ $q['question'] }}</td>
                                    <td>
                                        <ul class="chart-legend-list font-16">
                                            @foreach ($q['answer'] as $j)
                                                <li><div class="dot red"></div>{{ $j['jawaban'] }} <span onclick="confirmModal('{{ url('qna/delete-answer/'.$answer_id[$k]['id']) }}')"><i class="fa fa-sm fa-trash fa-danger"></i></span></li>
                                            @endforeach
                                        </ul>
                                    </td>
                                    <td class="text-center" width="15px">
                                        <span class="label label-{!! ($q['status'] == 'active') ? 'success' : 'warning' !!}">{{ $q['status'] }}</span>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>  --}}
                </div>
            </section>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('assets/js/lib/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/datatables-net/datatables.min.js') }}"></script>
    <script>
        var parent = {{ ($question != '' && $question->answer != null) ? $question->answer : 0 }};
        function parentQ(){
            $.get("{{ url('qna/get-answer') }}/"+$('select[name="parent"]').val(), function(resp){
                $('select[name="answer_parent"]').html('');
                for(var i = 0; i < resp.length; i++){
                    if(resp[i].id == parent){
                        $('select[name="answer_parent"]').append('<option value="'+resp[i].id+'" selected>'+resp[i].jawaban+'</option>');
                    }else{
                        $('select[name="answer_parent"]').append('<option value="'+resp[i].id+'">'+resp[i].jawaban+'</option>');
                    }
                }
            })
        }
        function pilihProject(val){
            console.log(val.value);
            $.get("{{ url('qna/get-question') }}"+"/"+val.value, function(resp){
                $('#question').html('');
                $('select[name="parent"]').html('');
                for(var i = 0; i < resp.length; i++){
                    $('#question').append('<option value="'+resp[i].id+'">'+resp[i].question+'</option>');
                    $('select[name="parent"]').append('<option value="'+resp[i].id+'">'+resp[i].question+'</option>');
                }
            })
        }
        @if($question != '' && $question->parent != null)
            parentQ();
        @endif
        $('.select2').select2();
        $('.dataTable').DataTable({
            scrollX: true,
            pageLength:25,
        });
    </script>
@endpush