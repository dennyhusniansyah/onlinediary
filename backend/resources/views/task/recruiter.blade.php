@extends('master')

@push('style')
    <link rel="stylesheet" href="{{ asset('assets/css/datepicker.min.css') }}">
    <style>
        td,
        td ul li{
            white-space: nowrap;
        }
        .fa-danger{
            color: #fa424a;
        }
        span{
            cursor: pointer;
        }
        .chart-legend-list li{
            padding: 3px 6px;
            border: 1px solid rgb(220 25 60);
            border-radius: 5px;
        }
        b.maroon{
            color: #DC143C;
        }
        b.green{
            color: #46c35f;
        }
        .p_0{
            padding: 0 !important;
        }
    </style>
@endpush

@section('content')
    <header class="section-header">
        <div class="tbl">
            <div class="tbl-row">
                <div class="tbl-cell">
                    <h4 class="pull-left">Task Recruiter</h4>
                    <button onclick="modalExtra('Edit Task Recruiter', '{{ url('tasks/edit-task-recruiter') }}')" class="btn btn-closer btn-labeled btn-header pull-right"><span class="btn-label"><i class="fa fa-pencil-alt"></i></span>EDIT</button>
                </div>
            </div>
        </div>
    </header>
    <section class="card">
        <div class="card-block">
            <div class="row">
                <div class="col-sm-12">
                    <form class="form-horizontal" method="GET" action="{{ url('tasks/recruiter/filter') }}">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label class="form-control-label col-md-4"><b>Project</b></label>
                                    <div class="col-md-8">
                                        <select class="form-control select2" name="project" onchange="this.form.submit()">
                                            <option value="0">Semua</option>
                                            @foreach ($project as $p)
                                                <option value="{{ $p->id }}" {!! ($project_id == $p->id) ? 'selected' : '' !!}>{{ $p->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <hr/>
            <div class="row">
                <table class="display table table-striped table-bordered dataTable" width="100%">
                    <thead>
                        <tr class="text-center">
                            <th width="15px">No</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Gender</th>
                            <th>Project</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($task as $t)
                            <tr>
                                <td class="text-center">{{ $no++ }}</td>
                                <td>{{ $t->name }}</td>
                                <td>{{ $t->email }}</td>
                                <td class="text-center">{{ $t->phone }}</td>
                                <td class="text-center">{{ $t->gender }}</td>
                                <td class="p_0">
                                    <table class="table table-striped table-xs">
                                        @php($status = explode('#', $t->taskStatus))
                                        @foreach (explode('#', $t->project) as $k => $p)
                                            @php($task = explode('||', $p))
                                            <tr>
                                                @if($status[$k] != 'finish')
                                                    <td width="15px">
                                                        @if($status[$k] == 'active')
                                                            <button class="btn btn-xs btn-danger" onclick="confirmModal('{{ url('tasks/delete-task-recruiter/'.$t->id.'/'.$task[1]) }}')"><i class="fa fa-trash"></i></button>
                                                        @else
                                                            <button class="btn btn-xs btn-success" onclick="optModal('Anda yakin mengatifkan kembali project ini ke rekruter ?', '{{ url('tasks/activated-task-recruiter/'.$t->id.'/'.$task[1]) }}')"><i class="fa fa-check"></i></button>
                                                        @endif
                                                        <button class="btn btn-xs btn-primary" onclick="optModal('Anda yakin project ini telah selesai ?', '{{ url('tasks/finished-task-recruiter/'.$t->id.'/'.$task[1]) }}')"><i class="fa fa-clipboard-check"></i></button>
                                                    </td>
                                                @endif
                                                <td {!! ($status[$k] == 'finish') ? 'colspan="2"' : '' !!}>{{ $task[0] }}</td>
                                                <td width="25px" class="text-center"><b class="{!! ($status[$k] == 'inactive') ? 'maroon' : 'green' !!}">{{ $status[$k] }}</b></td>
                                            </tr>
                                        @endforeach
                                    </table>
                                    {{-- <ul class="chart-legend-list font-16">
                                        @foreach (explode('#', $t->project) as $k => $p)
                                            @php($task = explode('||', $p))
                                            <li>
                                                <span onclick="confirmModal('{{ url('tasks/delete-task-recruiter/'.$t->id.'/'.$task[1]) }}')"><i class="fa fa-sm fa-trash fa-danger"></i></span> {{ $task[0] }}
                                            </li>
                                        @endforeach
                                    </ul> --}}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection

@push('script')
    <script src="{{ asset('assets/js/lib/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/datatables-net/datatables.min.js') }}"></script>
	<script>
        $(document).ready(function() {
            $('.dataTable').DataTable({
                scrollX : true,
                columnDefs: [
                    { width: '60px', targets: 1 }
                ],
            });
        });
    </script>
@endpush
