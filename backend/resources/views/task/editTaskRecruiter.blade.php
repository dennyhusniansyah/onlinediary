<div class="row">
    <div class="col-md-7">
        <div class="form-group row">
            <label class="form-control-label col-12 text-center"><b>Task</b></label>
        </div>
        <hr/>
        <div class="row">
            <table class="table table-striped table-bordered" id="task" width="100%">
                <thead>
                    <tr class="text-center">
                        <th>Kode</th>
                        <th>Project</th>
                        <th>Start</th>
                        <th>Finish</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($projects as $p)
                        <tr>
                            <td>{{ $p->code }}</td>
                            <td>{{ $p->name }}</td>
                            <td>{{ date('d M Y', strtotime($p->start)) }}</td>
                            <td>{{ date('d M Y', strtotime($p->finish)) }}</td>
                            <td width="25px" class="text-center">
                                <button class="btn btn-labeled btn-sm btn-success" onclick="addTask('{{ $p->id }}')">Add <span class="btn-label right"><i class="fa fa-arrow-right"></i></span></button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-5">
        <div class="form-group row">
            <label class="form-control-label col-md-4"><b>Recruiter</b></label>
            <div class="col-md-8">
                <select name="recruiter" class="form-control select2" onchange="getTask()">
                    <option value="0" selected>Pilih Recruiter</option>
                    @foreach ($recruiter as $r)
                        <option value="{{ $r->id }}">{{ $r->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <hr/>
        <div class="row">
            <table class="table table-striped table-bordered" id="recruiter" width="100%">
                <thead width="100%">
                    <tr class="text-center">
                        {{-- <th>Aksi</th> --}}
                        <th>Kode</th>
                        <th>Project</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $('.select2').select2();
    $('#task').DataTable({
        processing: false,
        paging: false,
        bInfo: false,
    });

    var task = $('#recruiter').DataTable({
        processing: true,
        serverSide: true,
        paging: false,
        bInfo: false,
        searching: true,
        scrollCollapse: true,
        scrollY: 300,
        iDisplayLength: 100,
        ajax: "{{ url('tasks/task-recruiter/0') }}",
        columns: [
            //{ data: 'aksi', sClass: 'text-center', sWidth: '15px'},
            { data: 'code'},
            { data: 'name'},
        ]
    });

    function getTask(){
        var r   = $('select[name="recruiter"]').val();
        task.ajax.url("{{ url('tasks/task-recruiter') }}/"+r).load();
    }
    function addTask(id){
        var r   =  $('select[name="recruiter"]').val();
        if(r > 0){
            $.get("{{ url('tasks/add-task-recruiter') }}/"+id+"/"+r);
            setTimeout(function(){
                task.ajax.url("{{ url('tasks/task-recruiter') }}/"+r).load();
            }, 300);
        }
    }
    function deleteTask(id){
        var r   =  $('select[name="recruiter"]').val();
        $.get("{{ url('tasks/delete-task-recruiter') }}/"+id);
        setTimeout(function(){
            task.ajax.url("{{ url('tasks/task-recruiter') }}/"+r).load();
        }, 300);
    }
</script>