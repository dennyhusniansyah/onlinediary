<div class="row">
    <div class="col-md-7">
        <div class="form-group row">
            <label class="form-control-label col-12 text-center"><b>Task</b></label>
        </div>
        <hr/>
        <div class="row">
            <table class="table table-striped table-bordered" id="task" width="100%">
                <thead>
                    <tr class="text-center">
                        <th>ID</th>
                        <th>Recruiter</th>
                        <th>Project</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($recruiter as $r)
                        <tr>
                            <td>{{ $r->id }}</td>
                            <td>{{ $r->name }}</td>
                            <td>{{ $r->pname }}</td>
                            
                            <td width="25px" class="text-center">
                                <button class="btn btn-labeled btn-sm btn-success" onclick="addTask('{{ $r->id }}','{{ $r->pid }}')">Add <span class="btn-label right"><i class="fa fa-arrow-right"></i></span></button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-5">
        <div class="form-group row">
            <label class="form-control-label col-md-4"><b>Responden</b></label>
            <div class="col-md-8">
                <select name="responder" class="form-control" onchange="getTask()">
                    <option value="0" selected>Pilih Responden</option>
                    @foreach ($responder as $r)
                        <option value="{{ $r->id }}">{{ $r->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <hr/>
        <div class="row">
            <table class="table table-striped table-bordered" id="responder" width="100%">
                <thead width="100%">
                    <tr class="text-center">
                        {{-- <th>Aksi</th> --}}
                        <th>Kode</th>
                        <th>Project</th>
                        
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    
    $('#task').DataTable({
        processing: false,
        paging: false,
        bInfo: false,
    });

    var task = $('#responder').DataTable({
        processing: true,
        serverSide: true,
        paging: false,
        bInfo: false,
        searching: true,
        scrollCollapse: true,
        scrollY: 300,
        iDisplayLength: 100,
        ajax: "{{ url('tasks/task-responder/0') }}",
        columns: [
            //{ data: 'aksi', sClass: 'text-center', sWidth: '15px'},
            { data: 'code'},
            { data: 'name'},
            
        ]
    });

    function getTask(){
        var r   = $('select[name="responder"]').val();
        task.ajax.url("{{ url('tasks/task-responder') }}/"+r).load();
    }
    function addTask(id,pid){
        var r   =  $('select[name="responder"]').val();
        
        if(r > 0){
            $.get("{{ url('tasks/add-task-responder') }}/"+id+"/"+r+"/"+pid);
            setTimeout(function(){
                task.ajax.url("{{ url('tasks/task-responder') }}/"+r).load();
            }, 300);
        }
    }
    function deleteTask(id){
        var r   =  $('select[name="responder"]').val();
        $.get("{{ url('tasks/delete-task-responder') }}/"+id);
        setTimeout(function(){
            task.ajax.url("{{ url('tasks/task-responder') }}/"+r).load();
        }, 300);
    }
</script>