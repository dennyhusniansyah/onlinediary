<form method="POST" action="{{ url('users/update-administrator') }}">
    @csrf
    <input type="hidden" name="id" value="{{ $u->id }}"/>
    <div class="form-group row">
        <label class="col-sm-3 form-control-label">Nama</label>
        <div class="col-sm-9">
            <div class="form-control-static">
                <input type="text" name="name" class="form-control" value="{{ $u->name }}" required autocomplete="off">
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 form-control-label">Email</label>
        <div class="col-sm-9">
            <div class="form-control-static">
                <input type="email" class="form-control" value="{{ $u->email }}" readonly>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 form-control-label">Kontak</label>
        <div class="col-sm-9">
            <div class="form-control-static">
                <input type="text" name="phone" class="form-control" value="{{ $u->phone }}" required autocomplete="off">
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 form-control-label">Gender</label>
        <div class="col-sm-9">
            <select name="gender" class="form-control select2">
                <option value="M" {!! ($u->gender == "M") ? 'selected' : '' !!}>Laki-laki</option>
                <option value="F" {!! ($u->gender == "F") ? 'selected' : '' !!}>Perempuan</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 form-control-label">Tanggal Lahir</label>
        <div class="col-sm-9">
            <input type="text" name="birthday" class="form-control datepicker" value="{{ $u->birthday }}" required autocomplete="off"/>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 form-control-label">Password</label>
        <div class="col-sm-9">
            <div class="form-control-static">
                <input type="password" name="password" class="form-control" placeholder="Kosongi jika tidak di ubah" autocomplete="off">
            </div>
        </div>
    </div>
    <div class="form-group text-center">
        <button type="submit" class="btn btn-success">UPDATE</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">BATAL</button>
    </div>
</form>
<script>
    $('.select2').select2();
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });
</script>