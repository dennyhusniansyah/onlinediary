@extends('master')

@push('style')
    <link rel="stylesheet" href="{{ asset('assets/css/datepicker.min.css') }}">
@endpush

@section('content')
    <header class="section-header">
        <div class="tbl">
            <div class="tbl-row">
                <div class="tbl-cell">
                    <h4 class="pull-left">Admin</h4>
                    @if(Auth::user()->role == 'admin')
                        <button onclick="showAjaxModal('Tambah Admin', '{{ url('users/add-administrator') }}')" class="btn btn-closer btn-labeled btn-header pull-right"><span class="btn-label"><i class="fa fa-plus"></i></span>TAMBAH</button>
                    @endif
                </div>
            </div>
        </div>
    </header>
    <section class="card">
        <div class="card-block">
            <div class="row">
                <table class="display table table-striped table-bordered dataTable">
                    <thead>
                        <tr class="text-center">
                            <th width="15px">No</th>
                        @if(Auth::user()->role == 'admin')
                            <th>Aksi</th>
                        @endif
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Gender</th>
                            <th>Role</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $u)
                            <tr>
                                <td class="text-center">{{ $no++ }}</td>
                            @if(Auth::user()->role == 'admin')
                                <td class="text-center" width="80px">
                                    <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                                        <button class="btn btn-sm btn-success" onclick="showAjaxModal('Edit Administrator', '{{ url('/users/edit-administrator/'.$u->id) }}')"><i class="fa fa-pencil-alt"></i></button>
                                        <button class="btn btn-sm btn-danger" onclick="showAjaxModal('Edit Administrator', '{{ url('/users/hapus-administrator/'.$u->id) }}')"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            @endif
                                <td>{{ $u->name }}</td>
                                <td>{{ $u->email }}</td>
                                <td class="text-center">{{ ($u->gender == 'M') ? 'Laki-laki' : 'Perempuan' }}</td>
                                <td class="text-center"><span class="label label-primary">{{ $u->role }}</span></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection

@push('script')
    <script src="{{ asset('assets/js/datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/datatables-net/datatables.min.js') }}"></script>
	<script>
        $('.dataTable').DataTable();
    </script>
@endpush