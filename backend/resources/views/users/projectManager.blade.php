@extends('master')

@push('style')
    <link rel="stylesheet" href="{{ asset('assets/css/datepicker.min.css') }}">
@endpush

@section('content')
    <header class="section-header">
        <div class="tbl">
            <div class="tbl-row">
                <div class="tbl-cell">
                    <h4 class="pull-left">Project Manager</h4>
                    @if(Auth::user()->role == 'admin')
                        <button onclick="showAjaxModal('Tambah Project Manager', '{{ url('users/add-project-manager') }}')" class="btn btn-closer btn-labeled btn-header pull-right"><span class="btn-label"><i class="fa fa-plus"></i></span>TAMBAH</button>
                    @endif
                </div>
            </div>
        </div>
    </header>
    <section class="card">
        <div class="card-block">
            <div class="row">
                <table class="display table table-striped table-bordered dataTable">
                    <thead>
                        <tr class="text-center">
                            <th width="15px">No</th>
                        @if(Auth::user()->role == 'admin')
                            <th>Aksi</th>
                        @endif
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Gender</th>
                            <th>Role</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $u)
                            <tr>
                                <td class="text-center">{{ $no++ }}</td>
                            @if(Auth::user()->role == 'admin')
                                <td class="text-center" width="80px">
                                    <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                                        <button class="btn btn-sm btn-success" onclick="showAjaxModal('Edit Project Manager', '{{ url('/users/edit-project-manager/'.$u->id) }}')"><i class="fa fa-pencil-alt"></i></button>
                                        <button class="btn btn-sm btn-danger" onclick="showAjaxModal('Edit Project Manager', '{{ url('/users/hapus-project-manager/'.$u->id) }}')"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            @endif
                                <td>{{ $u->name }}</td>
                                <td>{{ $u->email }}</td>
                                <td class="text-center">{{ ($u->gender == 'M') ? 'Laki-laki' : 'Perempuan' }}</td>
                                <td class="text-center"><span class="label label-success">{{ $u->role }}</span></td>
                                <td class="text-center">
                                    @if($u->status == 'inactive' && Auth::user()->role == 'admin')
                                        <a href="{{ url('users/invite/'.$u->id) }}" class="btn btn-labeled btn-sm btn-closer" target="_blank"><span class="btn-label"><i class="fa fa-paper-plane"></i></span>invite</a>
                                    @else
                                        <span class="label label-{!! ($u->status == 'active') ? 'success' : 'warning' !!}">{{ $u->status }}</span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection

@push('script')
    <script src="{{ asset('assets/js/datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/datatables-net/datatables.min.js') }}"></script>
	<script>
        $('.dataTable').DataTable();
    </script>
@endpush