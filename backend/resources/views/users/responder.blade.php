@extends('master')

@push('style')
    <link rel="stylesheet" href="{{ asset('assets/css/datepicker.min.css') }}">
@endpush

@section('content')
    <header class="section-header">
        <div class="tbl">
            <div class="tbl-row">
                <div class="tbl-cell">
                    <h4 class="pull-left">Respondent</h4>
                    @if(Auth::user()->role == 'admin' || Auth::user()->role == 'pm' || Auth::user()->role == 'recruiter')
                        <button onclick="showAjaxModal('Tambah Responden', '{{ url('users/add-responder') }}')" class="btn btn-closer btn-labeled btn-header pull-right"><span class="btn-label"><i class="fa fa-plus"></i></span>TAMBAH</button>
                    @endif
                </div>
            </div>
        </div>
    </header>
    <section class="card">
        <div class="card-block">
            <div class="row">
                <table class="display table table-striped table-bordered dataTable">
                    <thead>
                        <tr class="text-center">
                            <th width="15px">ID</th>
                            <th>Aksi</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Gender</th>
                            <th>Age</th>
                            <th>Role</th>
                            <th>Recruiter</th>
                            <th>Status</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $u)
                            <tr>
                                <td class="text-center">{{ $u->id }}</td>
                                <td class="text-center" width="80px">
                                    <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                                        <button class="btn btn-sm btn-success" onclick="showAjaxModal('Edit Responden', '{{ url('/users/edit-responder/'.$u->id) }}')"><i class="fa fa-pencil-alt"></i></button>
                                        <!-- <button class="btn btn-sm btn-danger" onclick="showAjaxModal('Edit Responden', '{{ url('/users/delete-responder/'.$u->id) }}')"><i class="fa fa-trash"></i></button> -->
                                    </div>
                                </td>
                                <td>{{ $u->name }}</td>
                                <td class="text-center">{{ $u->email }}</td>
                                <td class="text-center">{{ ($u->gender == 'M') ? 'Laki-laki' : 'Perempuan' }}</td>
                                <td class="text-center">{{ now()->diff($u->birthday)->y }} th</td>
                                <td class="text-center"><span class="label label-warning">respondent</span></td>
                                <td class="text-center">{{ ucfirst($u->uname) }}</td>
                                <td class="text-center">
                                    @if($u->status == 'active')
                                        <button class="btn btn-xs btn-danger" onclick="optModal('Anda yakin ingin meng-non-aktifkan responden ?', '{{ url('/users/inactive-responder/'.$u->id) }}')"><i class="fa fa-recycle"></i> Active</button>
                                    @else
                                        <span class="label label-{!! ($u->status == 'active') ? 'success' : 'warning' !!}">{{ $u->status }}</span></td>
                                    @endif
                                </td>
                                <td class="text-center">
                                    @if($u->status == 'active')
                                        <span class="label label-{!! ($u->status == 'active') ? 'success' : 'warning' !!}">Still Active</span></td>
                                    @else
                                        <button class="btn btn-xs btn-danger" onclick="optModal('Anda yakin ingin menghapus responden ?', '{{ url('/users/delete-responder/'.$u->id) }}')"><i class="fa fa-trash"></i> Hapus</button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection

@push('script')
    <script src="{{ asset('assets/js/datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/datatables-net/datatables.min.js') }}"></script>
	<script>
        $('.dataTable').DataTable();
    </script>
@endpush
