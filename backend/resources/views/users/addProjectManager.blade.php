<form method="POST" action="{{ url('users/save-project-manager') }}">
    @csrf
    <div class="form-group row">
        <label class="col-sm-3 form-control-label">Nama</label>
        <div class="col-sm-9">
            <div class="form-control-static">
                <input type="text" name="name" class="form-control" placeholder="Nama" required autocomplete="off">
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 form-control-label">Email</label>
        <div class="col-sm-9">
            <div class="form-control-static">
                <input type="email" name="email" class="form-control" placeholder="Email" required autocomplete="off">
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 form-control-label">Kontak</label>
        <div class="col-sm-9">
            <div class="form-control-static">
                <input type="text" name="phone" class="form-control" placeholder="62...." required autocomplete="off">
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 form-control-label">Gender</label>
        <div class="col-sm-9">
            <select name="gender" class="form-control select2">
                <option value="M">Laki-laki</option>
                <option value="F">Perempuan</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 form-control-label">Tanggal Lahir</label>
        <div class="col-sm-9">
            <input type="text" name="birthday" class="form-control datepicker" placeholder="Tanggal Lahir" required autocomplete="off"/>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 form-control-label">Password</label>
        <div class="col-sm-9">
            <div class="form-control-static">
                <input type="password" name="password" class="form-control" placeholder="Password" required autocomplete="off">
            </div>
        </div>
    </div>
    <div class="form-group text-center">
        <button type="submit" class="btn btn-success">SIMPAN</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">BATAL</button>
    </div>
</form>
<script>
    $('.select2').select2();
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });
</script>