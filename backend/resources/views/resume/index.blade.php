@extends('master')

@push('style')
    <link rel="stylesheet" href="{{ asset('assets/css/datepicker.min.css') }}">
    <style>
        td{
            white-space: nowrap;
        }
        .pt_0, .pb_0{
            padding-top: 0 !important;
            padding-bottom: 0 !important;
        }
        .btn.btn-secondary{
            background-color: #dc143c;
            border-color: #dc143c;
        }
        .btn.btn-secondary:active,
        .btn.btn-secondary:hover {
            background-color: #ffffff !important;
            color: #dc143c !important;
            border-color: #dc143c !important;
        }
    </style>
@endpush

@section('content')
    <header class="section-header">
        <div class="tbl">
            <div class="tbl-row">
                <div class="tbl-cell">
                    <h4 class="pull-left">Resume</h4>
                </div>
            </div>
        </div>
    </header>
    <section class="card">
        <div class="card-block">
            <form class="form-horizontal" method="GET" action="{{ url('resume/filter') }}" id="filter">
                <div class="row">
                    <div class="col-4">
                        <div class="form-group row">
                            <label class="form-control-label col-md-4"><b>Resp</b></label>
                            <div class="col-md-8">
                                <select class="form-control select2" name="resp_id" onchange="this.form.submit()">
                                    <option value="0">Semua</option>
                                    @foreach ($responder as $r)
                                        <option value="{{ $r->id }}" {!! ($r->id == $resp_id) ? 'selected' : '' !!}>{{ $r->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group row">
                            <label class="form-control-label col-md-3"><b>Proj</b></label>
                            <div class="col-md-9">
                                <select class="form-control select2" name="project" onchange="this.form.submit()">
                                    @foreach ($projects as $p)
                                        <option value="{{ $p->id }}" {!! ($p->id == $project->id) ? 'selected' : '' !!}>{{ $p->code }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="form-group row">
                            <label class="col-md-3 form-control-label"><b>Periode</b></label>
                            <div class="col-md-4">
                                <input type="text" name="from_date" class="form-control datepicker" value="{{ $from_date }}"/>
                            </div>
                            <label class="col-md-1 form-control-label"><b>:</b></label>
                            <div class="col-md-4">
                                <input type="text" name="to_date" class="form-control datepicker" value="{{ $to_date }}"/>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <hr/>
            <div class="row">
                <table class="display table table-striped table-bordered dataTable" width="100%">
                    <thead>
                        <tr class="text-center">
                            <th width="15px">No</th>
                            <th>Respondent</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Birthday</th>
                            <th>Gender</th>
                            <th>Marital</th>
                            <th>Status</th>
                            <th>Timing</th>
                            <th>Created At</th>
                            <th>Activity</th>
                            @foreach ($quest as $q)
                                <th>{{ $q->question }}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($resume as $r)
                            <tr>
                                <td class="text-center">{{ $no++ }}</td>
                                <td>{{ $r['name'] }}</td>
                                <td class="text-center">{{ $r['email'] }}</td>
                                <td class="text-center">{{ $r['phone'] }}</td>
                                <td class="text-center">{{ date('d M Y', strtotime($r['birthday'])) }}</td>
                                <td class="text-center">{{ $r['gender'] }}</td>
                                <td class="text-center">{{ $r['marital'] }}</td>
                                <td class="text-center"><span class="label label-{!! ($r['status'] == 'approved') ? 'success' : (($r['status'] == 'pending') ? 'warning' : 'danger') !!}">{{ $r['status'] }}</span></td>
                                <td align="center">{{ $r['timing'] }}</td>
                                <td align="center">{{ date('d M Y H:i', strtotime($r['created'])) }}</td>
                                <td>{{ $r['activity'] }}</td>
                                @foreach ($quest as $q)
                                    <td class="pb_0 pt_0">
                                        @if($q->type == 'file')
                                            @php($photo = $r['qna']->where('questions_id', $q->id)->first()->photo ?? '')
                                            @if($photo != '')
                                                <a href="{{ url('upload/activity/'.$photo) }}" target="_blank">{{ url('upload/activity/'.$photo) }}<a/>
                                            @endif
                                        @else
                                            {{ $r['qna']->where('questions_id', $q->id)->first()->answer ?? '' }}
                                        @endif
                                    </td>
                                @endforeach
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection

@push('script')
    <script src="{{ asset('assets/js/lib/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.table2excel.js') }}"></script>
    <script src="{{ asset('assets/js/lib/datatables-net/datatables.min.js') }}"></script>
	<script>
        $(document).ready(function() {
            $('.datepicker').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            }).change(function(){
                $('#filter').submit();
            });
            @if($project)
                $('.dataTable').DataTable({
                    pageLength: 50,
                    scrollX: true,
                    dom: 'Bfrtip',
                    buttons: [
                        {extend: 'copy', text: 'Copy', filename: '{{ $project->code."_".date("YmdHs") }}'},
                        {extend: 'csv', text: 'Csv', filename: '{{ $project->code."_".date("YmdHs") }}'},
                        {
                            extend: 'excelHtml5',
                            text: 'Excel',
                            filename: '{{ $project->code."_".date("YmdHs") }}',
                            exportOptions: {
                                stripHtml: true
                            }
                        }
                    ]
                });
            @else
                $('.dataTable').DataTable({
                    scrollX: true,
                });
            @endif
            $('.select2').select2();
        })
    </script>
@endpush