<!DOCTYPE html>
<html>
    @include('include/head')
    <body class="with-side-menu control-panel control-panel-compact chrome-browser">
	    <div class="container-fluid">
            @yield('content')
        </div>
    </body>
    @include('include/scripts')
    @stack('script')
</html>