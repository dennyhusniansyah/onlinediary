<!DOCTYPE html>
<html>
    <head lang="en">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>CLOSER DASHBOARD</title>
        
        <link rel="icon" href="{{ asset('assets/img/logo.png') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/separate/pages/login.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/lib/font-awesome/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/lib/bootstrap/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
        <style>
            body.login{
                background: #39436a;
            }
        </style>
    </head>
    <body>
        <div class="page-center" style="height:650px">
            <div class="page-center-in">
                <div class="container-fluid">
                    <form class="sign-box" method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="text-center">
                            <img src="{{ asset('assets/img/logo.png') }}" alt="" width="50%">
                        </div>
                        <br/>
                        <h4 class="text-center">CLOSER DASHBOARD</h4>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-at"></i></span></div>
                                <input type="email" class="form-control" name="email" placeholder="E-mail" required autocomplete="off"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-key"></i></span></div>
                                <input type="password" class="form-control" name="password" placeholder="Password" required autocomplete="off"/>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-closer btn-rounded">Sign in</button>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>