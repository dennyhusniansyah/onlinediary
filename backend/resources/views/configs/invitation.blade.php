@extends('master')

@section('content')
    <div class="container-fluid pl-0 pr-0">
        <h5 class="text-left">Undangan</h5>
        <form action="{{ url('configs/save-invitation') }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-12">
                    <section class="widget widget-activity" style="width:100%">
                        <header class="widget-header">Format Pesan Undangan<button class="btn btn-sm btn-closer btn-labeled pull-right mt_-5"><span class="btn-label"><i class="fa fa-save"></i></span>SIMPAN</button></header>
                        <div class="widget-activity-item">
                            <div class="form-group">
                                <textarea class="form-control" name="post" style="z-index:1" autofocus>{{ $configs['post'] ?? '' }}</textarea>
                                <small class="text-muted"><i>masukkan kode [role] untuk hak akses, [email] untuk email dan [password] untuk password!</i></small>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </form>
    </div>
@endsection