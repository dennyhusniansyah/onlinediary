@extends('master')

@push('style')
    <link rel="stylesheet" href="{{ asset('assets/css/datepicker.min.css') }}">
    <style>
        td,
        td ul li{
            white-space: nowrap;
        }
    </style>
@endpush

@section('content')
    <header class="section-header">
        <div class="tbl">
            <div class="tbl-row">
                <div class="tbl-cell">
                    <h4 class="pull-left">Project</h4>
                    <button onclick="modalExtra('Tambah Project', '{{ url('project/add-data') }}')" class="btn btn-closer btn-labeled btn-header pull-right"><span class="btn-label"><i class="fa fa-plus"></i></span>TAMBAH</button>
                </div>
            </div>
        </div>
    </header>
    <section class="card">
        <div class="card-block">
            <div class="row">
                <table class="display table table-striped table-bordered dataTable">
                    <thead>
                        <tr class="text-center">
                            <th width="15px">ID</th>
                            <th>Aksi</th>
                            <th>Kode</th>
                            <th>Nama</th>
                            <th>Produk</th>
                            <th>Jadwal</th>
                            <th>Capaian</th>
                            <th>Hari</th>
                            <th>Kuota</th>
                            <th>Task</th>
                            <th>Instruksi</th>
                            <th>Deskripsi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($projects as $p)
                            <tr>
                                <td class="text-center" width="15px">{{ $p->id }}</td>
                                <td class="text-center">
                                    <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                                        <button class="btn btn-sm btn-success" onclick="modalExtra('Edit Project', '{{ url('/project/edit-data/'.$p->id) }}')"><i class="fa fa-pencil-alt"></i></button>
                                        <button class="btn btn-sm btn-danger" onclick="confirmModal('{{ url('/project/hapus-data/'.$p->id) }}')"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                                <td>{{ $p->code }}</td>
                                <td>{{ $p->name }}</td>
                                <td class="text-center">{{ ucfirst($p->product) }}</td>
                                <td class="text-center">{{ str_replace('#', ', ', $p->timing) }}</td>
                                <td class="text-center">{{ $p->capaian }} %</td>
                                <td class="text-center">{{ $p->days }}</td>
                                <td class="text-center">{{ $p->quota }}</td>
                                <td>{{ $p->task }}</td>
                                <td>{{ $p->instruction }}</td>
                                <td>{{ $p->description }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection

@push('script')
    <script src="{{ asset('assets/js/datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/datatables-net/datatables.min.js') }}"></script>
	<script>
        $(document).ready(function() {
            $('.dataTable').DataTable({
                scrollX : true,
                columnDefs: [
                    { width: '60px', targets: 1 }
                ],
            });
        });
    </script>
@endpush