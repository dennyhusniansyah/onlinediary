<form method="POST" action="{{ url('project/update-data') }}">
    @csrf
    <input type="hidden" name="id" value="{{ $project->id }}"/>
    <div class="row">
        <div class="col-6">
            <div class="form-group row">
                <label class="col-sm-3 form-control-label">Kode</label>
                <div class="col-sm-9">
                    <div class="form-control-static">
                        <input type="text" class="form-control" maxlength="10"  value="{{ $project->code }}" readonly autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 form-control-label">Nama</label>
                <div class="col-sm-9">
                    <div class="form-control-static">
                        <input type="text" name="name" class="form-control" value="{{ $project->name }}" required autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 form-control-label">Kuota</label>
                <div class="col-sm-9">
                    <div class="form-control-static">
                        <input type="number" name="quota" class="form-control" value="{{ $project->quota }}" required autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 form-control-label">Capaian Waktu</label>
                <div class="col-sm-9">
                    <div class="form-control-wrapper form-control-icon-right">
                        <input type="number" name="capaian" class="form-control" value="{{ $project->capaian }}" required autocomplete="off">
                        <i class="fa fa-percent"></i>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 form-control-label">Timing</label>
                <div class="col-sm-9">
                    <div class="row">
                    @php($timing = explode('#', $project->timing))
                    @foreach ($timings as $t)
                        <div class="col-6">
                            <div class="checkbox-bird maroon">
                                <input type="checkbox" id="{{ strtolower($t->name) }}" name="timing[{{ $t->id }}]" {!! in_array($t->id, $timing) ? 'checked' : '' !!}>
                                <label for="{{ strtolower($t->name) }}">{{ $t->name }}</label>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="form-group row">
                <label class="col-sm-3 form-control-label">Product</label>
                <div class="col-sm-9">
                    <div class="form-control-static">
                        <select name="products_id" class="form-control select2" required>
                            @foreach ($products as $p)
                                <option value="{{ $p->id }}" {!! ($p->id == $project->products_id) ? 'selected' : '' !!}>{{ $p->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 form-control-label">Max Hari</label>
                <div class="col-sm-9">
                    <div class="form-control-static">
                        <input type="number" name="days" class="form-control" value="{{ $project->days }}" required autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 form-control-label">Task</label>
                <div class="col-sm-9">
                    <div class="form-control-static">
                        <input type="text" name="task" class="form-control" value="{{ $project->task }}" required autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 form-control-label">Instruksi</label>
                <div class="col-sm-9">
                    <div class="form-control-static">
                        <textarea name="instruction" class="form-control">{{ $project->instruction }}</textarea>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 form-control-label">Deskripsi</label>
                <div class="col-sm-9">
                    <div class="form-control-static">
                        <textarea name="description" class="form-control">{{ $project->description }}</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group text-center">
        <button type="submit" class="btn btn-success">UPDATE</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">BATAL</button>
    </div>
</form>
<script>
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });
</script>