<form method="POST" action="{{ url('project/save-data') }}">
    @csrf
    <div class="row">
        <div class="col-6">
            <div class="form-group row">
                <label class="col-sm-3 form-control-label">Kode</label>
                <div class="col-sm-9">
                    <div class="form-control-static">
                        <input type="text" name="code" class="form-control" maxlength="10"  placeholder="Kode Project" required autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 form-control-label">Nama</label>
                <div class="col-sm-9">
                    <div class="form-control-static">
                        <input type="text" name="name" class="form-control" placeholder="Nama Project" required autocomplete="off">
                    </div>
                </div>
            </div>
            {{--  <div class="form-group row">
                <label class="col-sm-3 form-control-label">Start</label>
                <div class="col-sm-9">
                    <div class="form-control-static">
                        <input type="text" name="start" class="form-control datepicker" placeholder="Start Project" required autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 form-control-label">Finish</label>
                <div class="col-sm-9">
                    <div class="form-control-static">
                        <input type="text" name="finish" class="form-control datepicker" placeholder="Finish Project" required autocomplete="off">
                    </div>
                </div>
            </div>  --}}
            <div class="form-group row">
                <label class="col-sm-3 form-control-label">Kuota</label>
                <div class="col-sm-9">
                    <div class="form-control-static">
                        <input type="number" name="quota" class="form-control" placeholder="Kuota Responder" required autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 form-control-label">Capaian Waktu</label>
                <div class="col-sm-9">
                    <div class="form-control-wrapper form-control-icon-right">
                        <input type="number" name="capaian" class="form-control" placeholder="Capaian Waktu (100)" required autocomplete="off">
                        <i class="fa fa-percent"></i>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 form-control-label">Timing</label>
                <div class="col-sm-9">
                    <div class="row">
                    @foreach ($timing as $t)
                        <div class="col-6">
                            <div class="checkbox-bird maroon">
                                <input type="checkbox" id="{{ strtolower($t->name) }}" name="timing[{{ $t->id }}]" checked>
                                <label for="{{ strtolower($t->name) }}">{{ $t->name }}</label>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-3 form-control-label">Product</label>
            <div class="col-sm-9">
                <div class="form-control-static">
                    <select name="products_id" class="form-control select2" required>
                        <option value="" disabled selected>Pilih Product</option>
                        @foreach ($products as $p)
                            <option value="{{ $p->id }}">{{ $p->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="form-group row">
                <label class="col-sm-3 form-control-label">Max Hari</label>
                <div class="col-sm-9">
                    <div class="form-control-static">
                        <input type="number" name="days" class="form-control" placeholder="Maksimal Hari" required autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 form-control-label">Title Task</label>
                <div class="col-sm-9">
                    <div class="form-control-static">
                        <input type="text" name="task" class="form-control" placeholder="Task" required autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 form-control-label">Instruksi</label>
                <div class="col-sm-9">
                    <div class="form-control-static">
                        <textarea name="instruction" class="form-control"></textarea>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 form-control-label">Deskripsi</label>
                <div class="col-sm-9">
                    <div class="form-control-static">
                        <textarea name="description" class="form-control"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group text-center">
        <button type="submit" class="btn btn-success">SIMPAN</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">BATAL</button>
    </div>
</form>
<script>
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });
</script>
