<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>CLOSER</title>

    <link rel="icon" href="{{ asset('assets/img/logo.png') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/lib/bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/separate/vendor/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/lib/datatables-net/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/lib/datatables-net/responsive-2.1.0/css/responsive.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/separate/vendor/datatables-net.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/separate/pages/widgets.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/all.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/lib/bootstrap-sweetalert/sweetalert.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/separate/vendor/sweet-alert-animations.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/separate/pages/profile.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/datepicker.min.css') }}">
    <style>
        .support{
            position: fixed;
            right: 20px;
            bottom: 20px;
            width: 50px;
            height: 50px;
        }
        .support button{
            background: rgba(0, 0, 0, 0);
            border: 2px solid rgb(44, 140, 33);
            width: 100%;
            height: 100%;
            border-radius: 50%;
        }
        .support button img{
            width: 100%;
            height: 100%;
        }
    </style>
    @stack('style')
</head>