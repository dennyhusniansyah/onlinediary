<div class="modal fade" id="modal_pict">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" id="img"></div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_ajax">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_lg">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_xl">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>
<div class="modal fade" id="confirmModal">
    <div class="modal-dialog">
        <div class="modal-content" style="margin-top:100px;">
            <div class="modal-header">
                <h4 class="modal-title" style="text-align:center;width:100%;">Anda yakin ingin menghapus data ini ?</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="form-group text-center" style="margin-top:15px;">
                <a href="#" class="btn btn-danger" id="delete_link">YA</a>
                <button type="button" class="btn btn-success" data-dismiss="modal">BATAL</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="optModal">
    <div class="modal-dialog">
        <div class="modal-content" style="margin-top:100px;">
            <div class="modal-header">
                <h4 class="modal-title" style="text-align:center;width:100%;"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="form-group text-center" style="margin-top:15px;">
                <a href="#" class="btn btn-success" id="optLink">YA</a>
                <button type="button" class="btn btn-danger" data-dismiss="modal">BATAL</button>
            </div>
        </div>
    </div>
</div>
<script>
    @if ($message = Session::get('success'))
        swal({
            title: "Good job!",
            text: '{{ $message }}',
            type: "success",
            timer: 1500,
            showConfirmButton: false
        });
    @endif
    @if ($message = Session::get('error'))
        swal({
            title: "Failed !",
            text: '{{ $message }}',
            type: "error",
            timer: 1500,
            showConfirmButton: false
        });
    @endif
    function modalLarge(title, url) {
        jQuery('#modal_lg .modal-body').html('');
        $('#modal_lg .modal-title').text(title);
        jQuery('#modal_lg').modal('show', {
            backdrop: 'true'
        });
        $.ajax({
            url: url,
            success: function (response) {
                jQuery('#modal_lg .modal-body').html(response);
            }
        });
    }
    function modalExtra(title, url) {
        jQuery('#modal_xl .modal-body').html('');
        $('#modal_xl .modal-title').text(title);
        jQuery('#modal_xl').modal('show', {
            backdrop: 'true'
        });
        $.ajax({
            url: url,
            success: function (response) {
                jQuery('#modal_xl .modal-body').html(response);
            }
        });
    }
    function showAjaxModal(title, url) {
        jQuery('#modal_ajax .modal-body').html('');
        $('#modal_ajax .modal-title').text(title);
        jQuery('#modal_ajax').modal('show', {
            backdrop: 'true'
        });
        $.ajax({
            url: url,
            success: function (response) {
                jQuery('#modal_ajax .modal-body').html(response);
            }
        });
    }
    function modalPict(title, url) {
        jQuery('#modal_pict .modal-body').html('');
        $('#modal_pict .modal-title').text(title);
        jQuery('#modal_pict').modal('show', {
            backdrop: 'true'
        });
        $.ajax({
            url: url,
            success: function (response) {
                jQuery('#modal_pict .modal-body').html('<img src="'+url+'" width="100%"/>');
            }
        });
    }
    function confirmModal(url) {
        jQuery('#confirmModal').modal('show', {
            backdrop: 'static'
        });
        document.getElementById('delete_link').setAttribute('href', url);
    }
    function optModal(title, url) {
        $('#optModal .modal-title').text(title);
        jQuery('#optModal').modal('show', {
            backdrop: 'static'
        });
        document.getElementById('optLink').setAttribute('href', url);
    }
</script>