<header class="site-header">
    <div class="container-fluid">
        <div class="site-header-collapsed text-center">
            <h3><b>CLOSER</b></h3>
        </div>
        <a href="#" class="site-logo">
            {{--  <img class="hidden-md-down" src="{{ asset('assets/img/logo.png') }}">  --}}
        </a>
        <button id="show-hide-sidebar-toggle" class="show-hide-sidebar pull-left"><span>toggle menu</span></button>
        <button class="hamburger hamburger--htla">
            <span>toggle menu</span>
        </button>
        <div class="site-header-shown">
            <div class="dropdown user-menu">
                <button class="dropdown-toggle text-left" id="dd-user-menu" type="button" data-toggle="dropdown">
                    <strong class="pull-left">{{ Auth::user()->name }}</strong>
                    <img src="{{ (Auth::user()->photo != '') ? asset('upload/profile/'.Auth::user()->photo) : asset('assets/img/avatar.png') }}"/>
                </button>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-user-menu">
                    <a class="dropdown-item" href="{{ url('akun') }}"><span class="font-icon glyphicon glyphicon-user"></span>Akun</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><span class="font-icon glyphicon glyphicon-log-out"></span>Logout</a>
                </div>
            </div>
        </div>
    </div>
</header>