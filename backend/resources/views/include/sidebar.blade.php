<nav class="side-menu">
    <ul class="side-menu-list">
        <li class="maroon {{ ($pageName == 'dashboard') ? 'opened' : '' }}">
            <a href="{{ url('dashboard') }}"><i class="font-icon font-icon-dashboard"></i><span class="lbl">Dashboard</span></a>
        </li>

        @if(Auth::user()->role == 'admin' || Auth::user()->role == 'pm')
        <li class="maroon {{ ($pageName == 'resume') ? 'opened' : '' }}">
            <a href="{{ url('resume') }}"><i class="fa fa-clipboard-list"></i><span class="lbl">Resume</span></a>
        </li>
        <li class="maroon {{ ($pageName == 'timing') ? 'opened' : '' }}">
            <a href="{{ url('timing') }}"><i class="fa fa-clock"></i><span class="lbl">Timing</span></a>
        </li>
        <li class="maroon {{ ($pageName == 'product') ? 'opened' : '' }}">
            <a href="{{ url('product') }}"><i class="fa fa-tags"></i><span class="lbl">Product</span></a>
        </li>
        <li class="maroon {{ ($pageName == 'project') ? 'opened' : '' }}">
            <a href="{{ url('project') }}"><i class="fa fa-shoe-prints"></i><span class="lbl">Project</span></a>
        </li>
        <li class="maroon {{ ($pageName == 'qna') ? 'opened' : '' }}">
            <a href="{{ url('qna') }}"><i class="fa fa-question-circle"></i><span class="lbl">Question & Answer</span></a>
        </li>
        @endif

        <li class="maroon {{ ($pageName == 'progres') ? 'opened' : '' }}">
            <a href="{{ url('progres') }}"><i class="fa fa-clipboard-check"></i><span class="lbl">Progress</span></a>
        </li>
        {{--  <li class="maroon {{ ($pageName == 'product') ? 'opened' : '' }}">
            <a href="{{ url('product') }}"><i class="fa fa-tags"></i><span class="lbl">Product</span></a>
        </li>
        <li class="maroon with-sub  {{ ($pageName == 'comments') ? 'opened' : '' }}">
            <span><i class="fa fa-comments"></i><span class="lbl">Comments</span></span>
            <ul>
                <li><a href="{{ url('/comments/all') }}"><span class="lbl">All Comments</span></a></li>
                <li><a href="{{ url('/comments/bot') }}"><span class="lbl">Bot Comments</span></a></li>
            </ul>
        </li>  --}}
        <li class="maroon {{ ($pageName == 'comments') ? 'opened' : '' }}">
            <a href="{{ url('comments/all') }}"><i class="fa fa-comments"></i><span class="lbl">Comments</span>{!! commentUnread() !!}</a>
        </li>
        <li class="maroon with-sub  {{ ($pageName == 'tasks') ? 'opened' : '' }}">
            <span><i class="fa fa-tasks"></i><span class="lbl">Assign</span></span>
            <ul>
            @if(Auth::user()->role == 'admin' || Auth::user()->role == 'pm')
                <li><a href="{{ url('/tasks/recruiter') }}"><span class="lbl">Recruiter</span></a></li>
            @endif
                <li><a href="{{ url('/tasks/responder') }}"><span class="lbl">Respondent</span></a></li>
            </ul>
        </li>
        <li class="maroon with-sub  {{ ($pageName == 'users') ? 'opened' : '' }}">
            <span><i class="fa fa-users"></i><span class="lbl">Users</span></span>
            <ul>
            @if(Auth::user()->role == 'admin')
                <li><a href="{{ url('/users/administrator') }}"><span class="lbl">Administrator</span></a></li>
            
                <li><a href="{{ url('/users/project-manager') }}"><span class="lbl">Project Manager</span></a></li>
                @endif
                @if(Auth::user()->role == 'admin' || Auth::user()->role == 'pm')
                <li><a href="{{ url('/users/recruiter') }}"><span class="lbl">Recruiter</span></a></li>
                @endif
                <li><a href="{{ url('/users/responder') }}"><span class="lbl">Respondent</span></a></li>
            </ul>
        </li>
        @if(Auth::user()->role == 'admin' || Auth::user()->role == 'pm')
        <li class="maroon with-sub  {{ ($pageName == 'configs') ? 'opened' : '' }}">
            <span><i class="fa fa-tools"></i><span class="lbl">Settings</span></span>
            <ul>
                <li><a href="{{ url('/configs/invitation') }}"><span class="lbl">Format Undangan</span></a></li>
            </ul>
        </li>
        <li class="maroon {{ ($pageName == 'akun') ? 'opened' : '' }}">
            <a href="{{ url('akun') }}"><i class="fa fa-id-card-alt"></i><span class="lbl">Akun</span></a>
        </li> 
        @endif
        <li class="maroon">
            <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out-alt"></i><span class="lbl">Logout</span></a>
        </li>
    </ul>
</nav>