@extends('../master')

@section('content')
    <div class="card">
        <div class="card-block content">
            <header class="box-typical-header-sm"><strong>Edit Profile</strong></header>
            <hr/>
            <form method="POST" action="{{ url('akun/update-akun') }}" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group row">
                            <label class="col-sm-4 form-control-label">Foto</label>
                            <div class="col-sm-5 text-center">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 100px; height: 100px;" data-trigger="fileinput">
                                        <img src="{{ (Auth::user()->photo != '') ? asset('upload/profile/'.Auth::user()->photo) : asset('assets/img/avatar.png') }}"/>
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                                    <div>
                                        <span class="btn btn-sm btn-closer btn-file">
                                            <span class="fileinput-new">Pilih image</span>
                                            <span class="fileinput-exists">Pilih</span>
                                            <input type="file" name="userfile" accept="image/*">
                                        </span>
                                        <a href="#" class="btn btn-danger btn-sm fileinput-exists" data-dismiss="fileinput">Hapus</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group row">
                            <label class="col-sm-4 form-control-label">Email</label>
                            <div class="col-sm-8">
                                <div class="form-control-static">
                                    <input type="text" class="form-control" value="{{ $akun->email }}" readonly autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 form-control-label">Nama</label>
                            <div class="col-sm-8">
                                <div class="form-control-static">
                                    <input type="text" name="name" class="form-control" value="{{ $akun->name }}" required autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 form-control-label">Password</label>
                            <div class="col-sm-8">
                                <div class="form-control-static">
                                    <input type="password" name="password" class="form-control" placeholder="Kosongi jika tidak dirubah" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-success">UPDATE</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('assets/js/fileinput.js') }}"></script>
    <script src="{{ asset('assets/js/lib/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/datepicker.min.js') }}"></script>
    <script>
        $('.select2').select2();
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });
    </script>
@endpush