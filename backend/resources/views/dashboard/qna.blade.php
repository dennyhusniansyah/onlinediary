<div class="row">
    <div class="col-lg-4 col-sm-6 col-12">
        @foreach ($qna as $k => $q)
            @if ($k <= count($qna) / 3)
                <div class="form-group">
                    <label class="form-control-label">{{ $q->question }}</label>
                    @if ($q->type == 'file')
                        <img src="{{ url('upload/activity/'.$q->photo) }}" width="100%"/>
                    @else
                        <input type="text" class="form-control" value="{{ $q->answer }}" readonly/>
                    @endif
                </div>
            @endif
        @endforeach
    </div>
    <div class="col-lg-4 col-sm-6 col-12">
        @foreach ($qna as $k => $q)
            @if ($k > count($qna) / 3 && $k < count($qna) * 2 / 3)
                <div class="form-group">
                    <label class="form-control-label">{{ $q->question }}</label>
                    @if ($q->type == 'file')
                        <img src="{{ url('upload/activity/'.$q->photo) }}" width="100%"/>
                    @else
                        <input type="text" class="form-control" value="{{ $q->answer }}" readonly/>
                    @endif
                </div>
            @endif
        @endforeach
    </div>
    <div class="col-lg-4 col-sm-6 col-12">
        @foreach ($qna as $k => $q)
            @if ($k >= count($qna) * 2 / 3)
                <div class="form-group">
                    <label class="form-control-label">{{ $q->question }}</label>
                    @if ($q->type == 'file')
                        <img src="{{ url('upload/activity/'.$q->photo) }}" width="100%"/>
                    @else
                        <input type="text" class="form-control" value="{{ $q->answer }}" readonly/>
                    @endif
                </div>
            @endif
        @endforeach
    </div>
</div>