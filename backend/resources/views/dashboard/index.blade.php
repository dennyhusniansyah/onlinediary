@extends('master')

@push('style')
    <link rel="stylesheet" href="{{ asset('assets/css/datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/separate/pages/tasks.min.css') }}">
    <style>
        .task-card-tags{
            width: 100%;
        }
        .task-card-meta-item.default,
        .task-card-tags.default i,
        .task-card-tags.default small{
            color: #9d9d9d !important;
            background: unset !important;
        }
        .task-card-meta-item.maroon,
        .task-card-meta-item.maroon a{
            color: #DC143C !important;
            background: unset !important;
        }
        .task-card-meta-item.default,
        .task-card-meta-item.default a{
            color: rgb(157 157 157) !important;
            background: unset !important;
        }
        .task-card-meta-item.pink,
        .task-card-meta-item.pink a{
            color: #FFB6C1 !important;
            background: unset !important;
        }
        .task-card-meta-item.success{
            color: #46c35f !important;
            background: unset !important;
        }
        .task-card-meta-item.warning{
            color: #fdad2a !important;
            background: unset !important;
        }
        .task-card-meta-item.danger{
            color: #fa424a !important;
            background: unset !important;
        }
    </style>
@endpush

@section('content')
    <div class="col-12 filter">
        <form method="GET" action="{{ url('dashboard/filter') }}" id="filter">
            <div class="row">
                <div class="col-lg-3 col-6">
                    <div class="form-group row">
                        <label class="col-md-4 form-control-label"><b>Periode</b></label>
                        <div class="col-md-8">
                            <input type="text" name="from_date" class="form-control datepicker" value="{{ $from_date }}"/>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <div class="form-group row">
                        <label class="col-md-4 form-control-label"><b>Sampai</b></label>
                        <div class="col-md-8">
                            <input type="text" name="to_date" class="form-control datepicker" value="{{ $to_date }}"/>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <div class="form-group row">
                        <label class="col-md-4 form-control-label"><b>Resp</b></label>
                        <div class="col-md-8">
                            <select name="resp_id" class="form-control select2" onchange="this.form.submit()">
                                <option value="0">Semua</option>
                                @foreach ($responder as $r)
                                    <option value="{{ $r->id }}" {!! ($resp_id == $r->id) ? 'selected' : '' !!}>{{ $r->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <div class="form-group row">
                        <label class="col-md-4 form-control-label"><b>Project</b></label>
                        <div class="col-md-8">
                            <select name="project_id" class="form-control select2" onchange="this.form.submit()">
                                <option value="0" selected>Semua</option>
                                @foreach ($projects as $p)
                                    <option value="{{ $p->id }}" {!! ($p->id == $project_id) ? 'selected' : '' !!}>{{ $p->code }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="tasks-grid-col red">
        <div class="row">
            @foreach ($activity as $a)
                <div class="col-lg-3 col-md-4 col-6">
                    <section class="box-typical task-card task">
                       <div class="task-card-photo">
                            <span style="background-image: url({{ asset('upload/activity/'.$a->photo) }})"/>
                        </div>
                        <div class="task-card-in">
                            <div class="task-card-title">{{ $a->name }} - {{ $a->projects_id }}</div>
	                           
                            <div class="task-card-tags default">
                                <i class="fa fa-sm fa-calendar-alt"></i><small> {{ $a->created_at->format('d M Y H:i') }}</small>
                                <span class="label label-danger pull-right">{{ strtolower($a->timing) }}</span>
                            </div>
                            <div class="btn-group task-card-menu">
                                <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="font-icon-dots-vert-square"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a onclick="optModal('Apakah anda menyetujui activity ini ?', '{{ url('dashboard/activity/approved/'.$a->id) }}')" class="dropdown-item" href="#"><i class="font-icon font-icon-ok"></i>Approve</a>
                                    <a onclick="optModal('Apakah anda menolak activity ini ?', '{{ url('dashboard/activity/rejected/'.$a->id) }}')" class="dropdown-item" href="#"><i class="font-icon font-icon-del"></i>Reject</a>
                                </div>
                            </div>
                        </div>
                        <div class="task-card-footer">
                            <div class="task-card-meta-item {!! (checkQnA($a->id) > 0) ? 'maroon' : 'default' !!}">
                                @if (checkQnA($a->id) > 0)
                                    <a href="#" onclick="modalExtra('QnA', '{{ url('dashboard/qna/'.$a->id) }}')"><i class="fa fa-lg fa-clipboard-list"></i></a>
                                @else
                                    <i class="fa fa-lg fa-clipboard-list"></i>
                                @endif
                            </div>
                            <div class="task-card-meta-item {!! ($a->status == 'approved') ? 'success' : (($a->status == 'pending') ? 'default' : 'danger') !!}">
                                <i class="fa-lg {!! ($a->status == 'approved') ? 'fa fa-check' : (($a->status == 'pending') ? 'fa fa-spinner' : 'fas fa-times') !!}"></i>
                            </div>
                            <div class="task-card-meta-item default">
                                <a href="{{ url('comments/activity/'.$a->id) }}" class="default">
                                    <i class="fa fa-lg fa-comments"></i> {{ $a->comment }}
                                </a>
                            </div>
                            <div class="avatar-preview avatar-preview-31">
                                {{ $a->username }}
                            </div>
                            <div class="avatar-preview avatar-preview-32">
                                <a href="#">
                                    <img src="{{ asset('assets/img/avatar.png') }}">
                                </a>
                            </div>
                        </div>
                    </section>
                </div>
            @endforeach
        </div>
    </div>
    <div class="text-center">
        {{ $activity->links() }}
    </div>
@endsection

@push('script')
    <script src="{{ asset('assets/js/datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/select2/select2.full.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.datepicker').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            }).change(function(){
                    $('#filter').submit();
            });
        })
        $('.select2').select2();
    </script>
@endpush
